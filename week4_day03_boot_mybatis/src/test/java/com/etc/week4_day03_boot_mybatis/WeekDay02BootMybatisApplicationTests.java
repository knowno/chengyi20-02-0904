package com.etc.week4_day03_boot_mybatis;

import com.etc.week4_day03_boot_mybatis.dao.BlogMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WeekDay02BootMybatisApplicationTests {

    //这里要使用BlogMapper对象
    @Autowired
    private BlogMapper blogMapper;

    @Test
    void contextLoads() {
    }

    @Test
    void getBlog() {
        System.out.println(blogMapper.selectBlog(1));
    }

}
