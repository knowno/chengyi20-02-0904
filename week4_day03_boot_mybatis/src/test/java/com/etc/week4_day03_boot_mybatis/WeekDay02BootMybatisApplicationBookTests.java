package com.etc.week4_day03_boot_mybatis;

import com.etc.week4_day03_boot_mybatis.dao.BookMapper;
import com.etc.week4_day03_boot_mybatis.entity.Book;
import com.github.pagehelper.PageHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class WeekDay02BootMybatisApplicationBookTests {

    //这里要使用BookMapper
    @Autowired
    private BookMapper bookMapper;


    @Test
    void getBook() {
        System.out.println(bookMapper.selectByPrimaryKey(2L));
    }

    @Test
    void getBookByPage() {
        //分页4 在测试类(或者业务层) ，调用分页工具类的静态方法设置页码和每页记录数
        //分页之前需要加入pageHelper.startPage...
        PageHelper.startPage(2,5);
        //再调用查询的方法
        List<Book> books = bookMapper.selectByKeywords("%%");
        System.out.println(books);


    }

}
