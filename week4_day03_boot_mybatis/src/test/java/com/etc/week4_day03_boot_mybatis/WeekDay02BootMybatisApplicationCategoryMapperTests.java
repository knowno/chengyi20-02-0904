package com.etc.week4_day03_boot_mybatis;

import com.etc.week4_day03_boot_mybatis.dao.ArticleMapper;
import com.etc.week4_day03_boot_mybatis.dao.CategoryMapper;
import com.etc.week4_day03_boot_mybatis.entity.Article;
import com.etc.week4_day03_boot_mybatis.entity.Category;
import org.apache.ibatis.binding.MapperProxy;
import org.apache.ibatis.executor.BaseExecutor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class WeekDay02BootMybatisApplicationCategoryMapperTests {

    //这里要使用CategoryMapper
    @Autowired
    private CategoryMapper categoryMapper;


    @Test
    void getCategory() {
        Category category = categoryMapper.selectByPrimaryKey(1L);
        System.out.println(category);
        category.getList().forEach(System.out::println);
    }


}
