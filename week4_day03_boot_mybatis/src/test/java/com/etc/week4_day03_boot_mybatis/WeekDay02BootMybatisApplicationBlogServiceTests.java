package com.etc.week4_day03_boot_mybatis;

import com.etc.week4_day03_boot_mybatis.service.BlogService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WeekDay02BootMybatisApplicationBlogServiceTests {

    //这里要注入一个BlogService对象
    @Autowired
    private BlogService blogService;


    @Test
    void getBlog() {
        System.out.println(blogService.getBlog(1));
    }

}
