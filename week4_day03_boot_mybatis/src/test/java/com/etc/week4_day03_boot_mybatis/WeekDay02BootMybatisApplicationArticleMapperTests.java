package com.etc.week4_day03_boot_mybatis;

import com.etc.week4_day03_boot_mybatis.dao.ArticleMapper;
import com.etc.week4_day03_boot_mybatis.entity.Article;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
class WeekDay02BootMybatisApplicationArticleMapperTests {

    //这里要使用ArticleMapper对象
    @Autowired
    private ArticleMapper articleMapper;


    @Test
    void getArticle() {
        System.out.println(articleMapper.selectByPrimaryKey(2L));
    }

    @Test
    void updateArticle() {
        // 修改其实现查 =>再改
        Article article = articleMapper.selectByPrimaryKey(2L);
        System.out.println(article);
        //修改
        article.setSummary("测试修改summary0917");
        article.setContent(null);
        int i = articleMapper.updateByPrimaryKeySelective(article);

    }

    @Test
    void updateArticle2() {
        //构造一个对象
        Article article = new Article();
        article.setId(2);
        article.setContent("测试修改content0917");
        article.setTitle("测试title0917");
        //修改
        int i = articleMapper.updateByPrimaryKeySelective(article);
    }

    @Test
    void insertArticle() {
        //构造一个对象
        Article article = new Article();
        article.setContent("测试插入0917");
        article.setTitle("测试插入title0917");
        //修改
        int i = articleMapper.insertSelective(article);
    }

    @Test
    void selectArticleByLike() {
        //构造一个对象
        Article article = new Article();
        article.setTitle("%测试%");
        article.setContent("%试%");
        article.setCid(1);
        //修改
        List<Article> articleList = articleMapper.selectByLike(article);
        System.out.println(articleList);

    }

    @Test
    void selectArticleByLike2() {
        //构造一个对象
        Article article = new Article();
        article.setTitle("%测试%");
        article.setContent("%试%");
        article.setCid(1);
        //
        MyPageRequest myPageRequest = new MyPageRequest(1,10);

        List<Article> articleList = articleMapper.selectByLikeExample(article,myPageRequest);
        System.out.println(articleList);

    }



    /**
     * 测试一级缓存
     */
    @Test
    @Transactional
    void getArticleByCache() {
        Article article = articleMapper.selectByPrimaryKey(2L);
        System.out.println(article);
        System.out.println("******************************");
        Article article2 = articleMapper.selectByPrimaryKey(2L);
        System.out.println(article2);

    }

    /**
     * 测试二级缓存
     */
    @Test
    void getArticleByCache2() {
        Article article = articleMapper.selectByPrimaryKey(2L);
        System.out.println(article);
        System.out.println("******************************");
        Article article2 = articleMapper.selectByPrimaryKey(2L);
        System.out.println(article2);

    }

}
