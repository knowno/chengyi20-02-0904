package com.etc.week4_day03_boot_mybatis.service.impl;

import com.etc.week4_day03_boot_mybatis.dao.BlogMapper;
import com.etc.week4_day03_boot_mybatis.entity.Blog;
import com.etc.week4_day03_boot_mybatis.service.BlogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BlogServiceImpl implements BlogService {

    //装配一个blogmapper对象
    @Autowired
    BlogMapper blogMapper;

    @Override
    public Blog getBlog(Integer blogid) {
        //打印一个日志,日志级别
        log.info("BlogServiceImpl getBlog");
        log.debug("BlogServiceImpl getBlog");
        log.warn("BlogServiceImpl getBlog");
        log.error("BlogServiceImpl getBlog");
        return blogMapper.selectBlog(blogid);
    }
}
