package com.etc.week4_day03_boot_mybatis.service;

import com.etc.week4_day03_boot_mybatis.entity.Blog;

public interface BlogService {
    public Blog getBlog(Integer blogid);
}
