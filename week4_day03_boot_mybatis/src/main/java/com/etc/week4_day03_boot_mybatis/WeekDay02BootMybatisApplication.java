package com.etc.week4_day03_boot_mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//指定mapper所在的位置
@MapperScan("com.etc.week4_day03_boot_mybatis.dao")
public class WeekDay02BootMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeekDay02BootMybatisApplication.class, args);
    }

}
