package com.etc.week4_day03_boot_mybatis;

import lombok.Data;

@Data
public class MyPageRequest {
    private Integer page;//页码
    private Integer size;//每页记录数

    public MyPageRequest() {
    }

    public MyPageRequest(Integer page, Integer size) {
        this.page = page;
        this.size = size;
    }
}
