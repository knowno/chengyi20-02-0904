package com.etc.week4_day03_boot_mybatis.dao;

import com.etc.week4_day03_boot_mybatis.entity.Book;

import java.util.List;

/**
* @author Administrator
* @description 针对表【book】的数据库操作Mapper
* @createDate 2023-09-26 16:36:23
* @Entity com.etc.week4_day02_boot_mybatis.entity.Book
*/
public interface BookMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Book record);

    int insertSelective(Book record);

    Book selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Book record);

    int updateByPrimaryKey(Book record);

    /**
     *  分页 2 模糊查询的方法
     * @param keywords
     * @return
     */
    List<Book> selectByKeywords(String keywords);

}
