package com.etc.week4_day03_boot_mybatis.dao;

import com.etc.week4_day03_boot_mybatis.MyPageRequest;
import com.etc.week4_day03_boot_mybatis.entity.Article;

import java.util.List;

/**
* @author Administrator
* @description 针对表【article】的数据库操作Mapper
* @createDate 2023-09-26 14:35:29
* @Entity com.etc.week4_day02_boot_mybatis.entity.Article
*/
public interface ArticleMapper {

    /**
     * 根据主键删除记录
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 插入记录
     * @param record
     * @return
     */
    int insert(Article record);

    int insertSelective(Article record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    Article selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKey(Article record);

    /**
     * 参数不固定
     * @param param
     * @return
     */
//    List<Article> selectByPrimaryKey(Object ... param);

    /**
     *  固定三个参数
     * @param params1
     * @param params2
     * @param params3
     * @return
     */
//    List<Article> selectByPrimaryKey(@Param("params1") String params1,@Param("params2")  String params2, @Param("params2")String params3);


    /**
     * 根据对象的属性信息来动态生成sql语句
     * @param article
     * @return
     */
    List<Article> selectByLike(Article  article);

    /**
     *多个参数组成的，我们想使用某个对象的属性的话，需要将这个对象引用到
     * 对象名.属性名
     * @param article
     * @param pageRequest
     * @return
     */
    List<Article> selectByLikeExample(Article  article,MyPageRequest pageRequest);

}
