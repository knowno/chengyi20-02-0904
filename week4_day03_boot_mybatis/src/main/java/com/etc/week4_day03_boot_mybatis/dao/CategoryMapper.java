package com.etc.week4_day03_boot_mybatis.dao;

import com.etc.week4_day03_boot_mybatis.entity.Category;

/**
* @author Administrator
* @description 针对表【category】的数据库操作Mapper
* @createDate 2023-09-27 13:50:31
* @Entity com.etc.week4_day03_boot_mybatis.entity.Category
*/
public interface CategoryMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);

}
