package com.etc.test;

import java.sql.*;

public class TestJDBC {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //1 加载驱动(高版本可以省略)
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2. 创建连接
        String url = "jdbc:mysql://localhost:3307/storedb?useSSL=false&serverTimezone=Asia/Shanghai";
        String user = "root";
        String password = "root";
        Connection connection = DriverManager.getConnection(url, user, password);

        //3创建PreparedStatement
        String sql = "select * from tbl_emp";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);

        //4. 执行sql
        ResultSet resultSet = preparedStatement.executeQuery();

        //5处理结果
        while (resultSet.next()) {
            System.out.println(resultSet.getInt(1) + " :" + resultSet.getString(2));
        }

        //6释放资源
        resultSet.close();
        preparedStatement.close();
        connection.close();

    }
}
