package com.etc.test;

import java.sql.*;
import java.util.List;

public class TestJDBC2 {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        //此时我们使用通用类来查询
        List list = DBUtil2.exQuery("select * from tbl_emp", Emp.class);
        for (Object o : list) {
            System.out.println(o);
        }

    }
}
