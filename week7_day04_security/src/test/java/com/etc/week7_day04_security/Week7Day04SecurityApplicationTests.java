package com.etc.week7_day04_security;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class Week7Day04SecurityApplicationTests {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Test
	void contextLoads() {
		String encode = passwordEncoder.encode("123456");
		System.out.println(encode);
	}

}
