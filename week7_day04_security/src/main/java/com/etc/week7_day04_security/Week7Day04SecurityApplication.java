package com.etc.week7_day04_security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;

@SpringBootApplication
// 1 开启全局的配置,支持注解的角色访问控制
@EnableGlobalMethodSecurity(securedEnabled = true)
public class Week7Day04SecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week7Day04SecurityApplication.class, args);
	}

}
