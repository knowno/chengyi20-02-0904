package com.etc.week7_day04_security.service;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @Service =>UserDetailsServiceImpl
 * UserDetails 对象
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    //认证
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //就是去数据库表 根据username查询，如果不存在，抛出一个异常
        //我们使用临时数据进行模拟
        if (!"jerry".equals(username)) {
            throw new UsernameNotFoundException(username);
        } else {
            //我们需要一个用户对象
            //构造一个用户，包含了用户的基本信息(用户名,用户密码)和角色信息(和后续后端权限管理有关)
            String encode = "$2a$10$VUYQ0H94c/hbTF4zI73gJONUNF9o/iFnx8BDD05wwNBmyOqKqhD2y";
            //角色
            //String authrity = "ROLE_admin,ROLE_doctor,ROLE_manager";
            String authrity = "ROLE_admin";
            //这个对象密码特定的 加密规则的.
           return new User(username, encode, AuthorityUtils.commaSeparatedStringToAuthorityList(authrity));
        

        }
    }
}
