package com.etc.week7_day04_security.controller;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    
    @GetMapping("index")
    public String getByPage() {
        //查看 =>UsernamePasswordAuthenticationToken 包含了用户details信息
        System.out.println(SecurityContextHolder.getContext().getAuthentication());
        return "Index getByPage";
    }
}
