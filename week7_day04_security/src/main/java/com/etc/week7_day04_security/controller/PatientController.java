package com.etc.week7_day04_security.controller;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PatientController {

    /**
     * 例如我们希望  /patient这个地址 /patient/** 地址  有管理员权限(角色)的才能访问
     * @return
     */
    @GetMapping("patient")
    public String getByPage() {
        //查看 =>UsernamePasswordAuthenticationToken 包含了用户details信息
        System.out.println(SecurityContextHolder.getContext().getAuthentication());
        return "patient getByPage";
    }
}
