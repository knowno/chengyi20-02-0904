package com.etc.week7_day04_security.controller;


import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ManagerController {

    /**
     * 希望  /manager  manager角色才能访问的地址
     * 2  : @Secured("ROLE_manager")
     * @return
     */
//    @Secured("ROLE_manager")
    @GetMapping("manager")
    public String getByPage() {
        //查看 =>UsernamePasswordAuthenticationToken 包含了用户details信息
        System.out.println(SecurityContextHolder.getContext().getAuthentication());
        return "manager getByPage";
    }
}
