package com.etc.drugs.contoller;

import com.etc.demos.nacosdiscoveryconsumer.PatientServiceFallBack;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;


/**
 * 3  PatientFallBackFactory
 */
@Component
public class DrugsFallBackFactory implements FallbackFactory<DrugsServiceFallBack> {
    @Override
    public DrugsServiceFallBack create(Throwable cause) {
        return new DrugsServiceFallBack(cause);
    }
}
