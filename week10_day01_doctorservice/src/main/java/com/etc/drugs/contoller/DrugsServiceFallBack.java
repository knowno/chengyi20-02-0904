package com.etc.drugs.contoller;

import com.etc.demos.nacosdiscoveryconsumer.PatientService;
import com.etc.dto.DrugsDto;
import com.etc.dto.PatientDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 2 服务降级之后的处理方式
 *
 */
public class DrugsServiceFallBack implements DrugsService{

    private Throwable throwable;

    public DrugsServiceFallBack(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public ResponseEntity<DrugsDto> queryById(@PathVariable("id") Integer id){
        DrugsDto drugsDto = new DrugsDto();
        drugsDto.setId(0);
        drugsDto.setDrugsname("测试数据.");
        return ResponseEntity.ok(drugsDto);
    }
}
