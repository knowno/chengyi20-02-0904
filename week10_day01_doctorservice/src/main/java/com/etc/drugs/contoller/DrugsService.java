package com.etc.drugs.contoller;

import com.etc.dto.DrugsDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name="drugs-provider",fallbackFactory = DrugsFallBackFactory.class)
public interface DrugsService {
    @GetMapping("drugs/{id}")
    public ResponseEntity<DrugsDto> queryById(@PathVariable("id") Integer id);
}
