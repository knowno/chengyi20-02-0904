/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 package com.etc.drugs.contoller;

import com.etc.dto.DrugsDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 使用工具在单位时间内持续向 /feign/patient/{id} 发送请求.
 * 同时我们使用浏览器访问   @GetMapping("hello")
 */
@RestController
@Slf4j
public class OpenFeignDrugsController {

    @Autowired
    private DrugsService drugsService;

    @GetMapping("/feign/drugs/{id}")
    public ResponseEntity<DrugsDto> getDrugs(@PathVariable Integer id) throws InterruptedException {
        log.info("getDrugs "+id);
        log.info("System.currentTimeMillis() :"+System.currentTimeMillis());
        //这里假设是一个复杂的业务，这里需要调用其他业务方法完成整体请求...
        //使用线程的方法
        Thread.sleep(1000);
        return drugsService.queryById(id);
    }
}
