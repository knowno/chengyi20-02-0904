package com.etc.demos.nacosdiscoveryconsumer;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;


/**
 * 3  PatientFallBackFactory
 */
@Component
public class PatientFallBackFactory implements FallbackFactory<PatientServiceFallBack> {
    @Override
    public PatientServiceFallBack create(Throwable cause) {
        return new PatientServiceFallBack(cause);
    }
}
