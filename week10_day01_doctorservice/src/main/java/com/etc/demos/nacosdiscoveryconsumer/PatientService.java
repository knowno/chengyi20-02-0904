package com.etc.demos.nacosdiscoveryconsumer;

import com.etc.dto.PatientDto;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


/**
 * 4 fallbackFactory = PatientFallBackFactory.class
 */
@FeignClient(name = "patient-service",fallbackFactory = PatientFallBackFactory.class) // 指向服务提供者应用
public interface PatientService {
    //这里支持springmvc接口访问方式
    @GetMapping("patient/{id}")
    public ResponseEntity<PatientDto> queryById(@PathVariable("id") Long id);
}
