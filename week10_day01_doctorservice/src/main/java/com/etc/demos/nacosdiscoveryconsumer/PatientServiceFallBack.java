package com.etc.demos.nacosdiscoveryconsumer;

import com.etc.dto.PatientDto;
import org.springframework.http.ResponseEntity;

/**
 * 2 服务降级之后的处理方式
 *
 */
public class PatientServiceFallBack implements PatientService{

    private Throwable throwable;

    public PatientServiceFallBack(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public ResponseEntity<PatientDto> queryById(Long id) {
        //创建一个假的dto(默认值dto)
        PatientDto dto = new PatientDto();
        dto.setId(id);
        dto.setName("默认值");
        dto.setAddress("默认值地址");
        return ResponseEntity.ok(dto);
    }
}
