package com.etc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients // 激活 @FeignClient
public class Week10Day01DoctorserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week10Day01DoctorserviceApplication.class, args);
    }

}
