package com.etc.his.controller;

import com.etc.dto.PatientDto;
import com.etc.his.entity.Patient;
import com.etc.his.service.PatientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 就诊患者表(Patient)表控制层
 *
 * @author makejava
 * @since 2023-11-13 09:26:57
 */
@RestController
@RequestMapping("patient")
@Slf4j
public class PatientController {
    /**
     * 服务对象
     */
    @Resource
    private PatientService patientService;

    /**
     * 分页查询
     *
     * @param patient     筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<Patient>> queryByPage(Patient patient, PageRequest pageRequest) {
        return ResponseEntity.ok(this.patientService.queryByPage(patient, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<PatientDto> queryById(@PathVariable("id") Long id)  {
        log.info(" queryById "+id);
        //这里有一个对象转换过程
        Patient patient = patientService.queryById(id);
        PatientDto  dto =  new PatientDto();
        if (patient != null) {
            BeanUtils.copyProperties(patient,dto);
        }
        return ResponseEntity.ok(dto);
    }

    /**
     * 新增数据
     *
     * @param patient 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<Patient> add(Patient patient) {
        return ResponseEntity.ok(this.patientService.insert(patient));
    }

    /**
     * 编辑数据
     *
     * @param patient 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<Patient> edit(Patient patient) {
        return ResponseEntity.ok(this.patientService.update(patient));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Long id) {
        return ResponseEntity.ok(this.patientService.deleteById(id));
    }




}

