package com.etc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.etc.his.dao")
public class Week10Day01PatientproviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week10Day01PatientproviderApplication.class, args);
    }

}
