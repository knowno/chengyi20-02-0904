package com.etc.clonedemo;

public class GirlFriend {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GirlFriend(String name) {
        this.name = name;
    }

    public GirlFriend() {
    }

    @Override
    public String toString() {
        return "GirlFriend{" +
                "name='" + name + '\'' +
                '}';
    }
}
