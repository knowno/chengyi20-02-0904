package com.etc.com.etc.boxdemo;

/**
 * 包装类
 * byte,short,char,int,long,float,double,boolean.
 * Byte,Short,Character,Integer,Long,Float,Double,Boolean
 */
public class TestInteger2 {
    public static void main(String[] args) {

        // i1 i2 =>在byte的范围内 -128 ~ 127
        //当我们直接赋值为字面量,不是new操作
        Integer i1 = 100;
        Integer i2 = 100;
        //比较 ==
        System.out.println(i1 == i2);


        Integer i3 = 129;
        Integer i4 = 129;
        //比较 ==
        System.out.println(i3 == i4);


        Integer i5 = new Integer(100);
        Integer i6 = new Integer(100);
        //比较 ==
        System.out.println(i5 == i6);


    }
}
