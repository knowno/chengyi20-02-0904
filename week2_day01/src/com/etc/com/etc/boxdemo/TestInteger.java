package com.etc.com.etc.boxdemo;

/**
 * 包装类
 * byte,short,char,int,long,float,double,boolean.
 * Byte,Short,Character,Integer,Long,Float,Double,Boolean
 */
public class TestInteger {
    public static void main(String[] args) {

        //数据 int 定义Integer
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);


        System.out.println(Byte.MIN_VALUE);
        System.out.println(Byte.MAX_VALUE);

        //将字符串转换为int/Integer
        int i = Integer.parseInt("123");
        Integer integer = Integer.valueOf("456");
        //返回值直接进行数学计算
        System.out.println(i+integer);



    }
}
