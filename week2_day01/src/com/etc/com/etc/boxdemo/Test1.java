package com.etc.com.etc.boxdemo;

/**
 * 包装类
 * byte,short,char,int,long,float,double,boolean.
 * Byte,Short,Character,Integer,Long,Float,Double,Boolean
 */
public class Test1 {
    public static void main(String[] args) {

        //数据 int 定义Integer
        // Integer = 基本类型 int =>从基本类型 自动转换为对应包装类，这种隐式的转换，我们称为装箱.
        // Double d = 12.34;
        Integer i = 12;
        //这里我们就可以用属性和方法

        Double d = new Double(12.34);
        // 基本类型double = 包装类Double
        // 依然是隐式转换，这种转换称为拆箱.
        double dd = d;

    }
}
