package com.etc.exam;

/**
 * 自定义父类Car
 *
 */
public class Car {
    private String name;
    private String color;

    public Car() {
    }

    public Car(String name, String color) {

        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void move(){
        System.out.println("运行的方法....");
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}

/**
 * 子类Bmw
 */
class Bmw extends  Car{

    public Bmw() {
    }

    public Bmw(String name, String color) {
        //调用父类的构造方法
        super(name, color);
    }

    @Override
    public void move() {
        System.out.println("以100km/h的速度飞驰，谁都别摸我");
    }
}

/**
 * 子类Benz
 */

class Benz extends  Car{

    public Benz() {
    }

    public Benz(String name, String color) {
        //调用父类的构造方法
        super(name, color);
    }

    @Override
    public void move() {
        System.out.println("以100km/h的速度飞驰，我是Benz");
    }
}

/**
 * 子类Porsche
 */
class  Porsche extends  Car{
    public Porsche() {
    }

    public Porsche(String name, String color) {
        //调用父类的构造方法
        super(name, color);
    }

    @Override
    public void move() {
        System.out.println("以100km/h的速度飞驰，Porsche");
    }
}
