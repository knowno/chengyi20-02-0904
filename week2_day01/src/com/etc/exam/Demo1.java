package com.etc.exam;

public class Demo1 {
    public static void main(String[] args) {


//        int[][] arr = new int[3][];
//        //如果定义格式如上
//
//        //第一行第一列?
//        //java.lang.NullPointerException
//        //因为第二个维度还没有指定长度
//        System.out.println(arr[0][0]);

        //
        int[][] arr1 = new int[3][];

        //访问元素之前，必须先执行第二个维度对象常见
        arr1[0] = new int[5];
        //可以去访问 arr1[0][0]
        System.out.println(arr1[0][0]);


    }

    public final   void t1(int a){

    }

    public final   void t1(int a,int b){

    }
}
