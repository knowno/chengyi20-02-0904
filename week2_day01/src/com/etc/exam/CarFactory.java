package com.etc.exam;

/**
 * 工厂类
 */
public class CarFactory {
    /**
     *
     * @return
     */
    public static Car getInstance() {
        int round = (int) Math.round(Math.random() * 2);
        if (round == 0) {
            return new Bmw("奔驰", "红色");
        } else if (round == 1) {
            return new Benz("宝马", "绿色");
        } else if (round == 2) {
            return new Porsche("保时捷", "黑色");
        }
        return null;
    }

    /**
     *
     * @return
     */
    public static Car createInstance() {
        int round = (int) Math.round(Math.random() * 2);
        Car car = null;
        if (round == 0) {
            car = new Bmw("奔驰", "红色");
        } else if (round == 1) {
            car = new Benz("宝马", "绿色");
        } else if (round == 2) {
            car = new Porsche("保时捷", "黑色");
        }
        return car;
    }
}
