package com.etc.objectdemo;

public class TestObject {
    public static void main(String[] args) {

        Object obj = new Object();

        // return getClass().getName() + "@" + Integer.toHexString(hashCode());
        //java.lang.Object@4eec7777
        System.out.println(obj.toString());

        //System.out.println(obj.toString());
        // why?这里的输出和.toString()输出结果一样的？
        //obj.toString() 底层就是这样呀
        System.out.println(obj);


        //Object => return (this == obj);
        Object o1 = new Object();
        Object o2 = new Object();

        System.out.println(o1.equals(o2)); //false
        System.out.println(o1.hashCode());
        System.out.println(o2.hashCode());
        System.out.println("*********************************");
        //我写的是字符串
        String str1 = new String("a");
        String str2 = new String("a");

        //字符串的底层代码对equals进行了重写，按照每个字符进行逐一比较，比较内容是否相同.
        boolean e = str1.equals(str2);
        System.out.println(e);//true



    }

}
