package com.etc.objectdemo;

/**
 * 如果想调用clone方法的话，我们要求要实现Cloneable的接口,重写方法clone.
 * 我们可以通过对象.clone方法copy一个对象出来. 默认情况下和原来对象属性相同.
 */
class Person implements  Cloneable{
    private String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        System.out.println("Person类中的Clond方法");
        return super.clone();
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
public class TestClone {
    public static void main(String[] args) throws CloneNotSupportedException {

        Person person1 = new Person("张三");

        //调用clone方法
        Object person2 = person1.clone();

        System.out.println(person1.hashCode());
        System.out.println(person2.hashCode());

        //输出原来对象
        System.out.println(person1);
        System.out.println(person2);
    }

}
