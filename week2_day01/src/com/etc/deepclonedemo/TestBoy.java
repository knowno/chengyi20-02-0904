package com.etc.deepclonedemo;

public class TestBoy {
    public static void main(String[] args) throws CloneNotSupportedException {

        //创建Boy对象
        Boy boy = new Boy();
        boy.setName("小白");
        //创建一个GirlFriend对象
        GirlFriend girlFriend = new GirlFriend();
        girlFriend.setName("小青");
        //设置二者关系
        boy.setGirlFriend(girlFriend);

        System.out.println("...............克隆之前对象boy的信息........................");
        System.out.println(boy);
        //调用clone的方法
        Boy clone1 = (Boy) boy.clone();
        //输出这个克隆后的对象的信息
        System.out.println("...............克隆之后对象boy的信息........................");
        System.out.println(clone1);
        System.out.println("...............克隆之后对象的girl属性进行修改........................");
        clone1.getGirlFriend().setName("小绿");

        System.out.println("...............克隆之前对象boy的信息........................");
        System.out.println(boy);
        System.out.println("...............克隆之后对象boy的信息........................");
        System.out.println(clone1);




    }
}
