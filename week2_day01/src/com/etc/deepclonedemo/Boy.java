package com.etc.deepclonedemo;

/**
 * 深克隆
 */
public class Boy implements Cloneable {
    private String name;

    //这个男生有一个女朋友
    private GirlFriend girlFriend;

    public Boy() {
    }

    public Boy(String name, GirlFriend girlFriend) {
        this.name = name;
        this.girlFriend = girlFriend;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GirlFriend getGirlFriend() {
        return girlFriend;
    }

    public void setGirlFriend(GirlFriend girlFriend) {
        this.girlFriend = girlFriend;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        //boy也clone一下
        Boy boy = (Boy) super.clone();
        //这里需要对girl也做clone的调用(深克隆的核心代码)
        GirlFriend girl = (GirlFriend) girlFriend.clone();
        //设置关系
        boy.girlFriend = girl;
        return boy;
    }

    @Override
    public String toString() {
        return "Boy{" +
                "name='" + name + '\'' +
                ", girlFriend=" + girlFriend +
                '}';
    }
}
