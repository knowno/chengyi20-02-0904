package com.etc.review;

public class TestClass {

    public void test1(){
        System.out.println("test1");

        //能否实例方法中调用静态方法,可以的
        test2();
    }


    public static void test2(){
        //能否在这里直接调用test1方法 =》静态方法中能否直接调用实例方法
       //  test1(); 类存在，这个test2就可以执行，但是此时无法保证TestClass的对象是存在的，所以实例方法不能直接被调用.

        System.out.println("test2");
    }
}
