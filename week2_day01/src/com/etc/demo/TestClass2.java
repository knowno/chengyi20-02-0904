package com.etc.demo;

public class TestClass2 {
    // n 类所有，所有对象共享
    static int n = 1;

    // 没有static属于对象所有
    int m =1;


    public static void main(String[] args) {

        TestClass2.n++; //2

        TestClass2 t1 = new TestClass2();
        //不应该通过类实例访问静态成员
        t1.n++; //3

        t1.m++;


        TestClass2 t2 = new TestClass2();
        //不应该通过类实例访问静态成员
        t2.n++; //4

        t2.m++;

        System.out.println(TestClass2.n);

        //?
        System.out.println("t2.m: "+t2.m);



    }

}
