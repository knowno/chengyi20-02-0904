package com.etc.demo;

/**
 * final 关键字
 * 1. 修饰类： 这个类不能有子类(不能被继承) =>如果一个类工具类.
 * 2. 修饰一个方法，则这个方法不能被覆写（重写）
 * 3. 修饰一个属性
 */
public final class Person {

    //1 常量必须要赋值，而且不能修改
    final  String username = "usernmae";

    public final void test1(){

//        username = "xxx";
    }

    //2 常量可以通过构造方法来赋值
    final String sex;

    public Person(String sex) {
        this.sex = sex;
    }
}
//Cannot inherit from final 'com.etc.demo.Person'
//class Student extends  Person{
//
//}

final class StringUtil{

}
final  class DButil{

}
