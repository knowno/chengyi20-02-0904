package com.etc.demo;

public class Animal {

    //私有(当前类)
    private String name1;
    //默认 default(当前类，同一个包)
    String name2;
    // 受保护(当前类，同一个包，不同包的子类) -> 继承关系
    protected String name3;
    // 公共(当前类，同包，不同包子类，不同包非子类)
    public String name4;


}
