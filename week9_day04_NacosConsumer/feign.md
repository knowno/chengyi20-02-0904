### feign(openfeign)的操作步骤:

0. 在Consumer的pom.xml文件中加入feign的坐标
   ```xml
       <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
            <version>3.1.4</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-loadbalancer</artifactId>
            <version>3.1.4</version>
        </dependency>
   ```

1. 在Provider一端创建控制器接口

    ```java
    @RestController
    @Slf4j
    public class UsersController {
    
        @GetMapping("user/{userid}")
        public String getUser(@PathVariable Integer userid){
            log.info("porovder UsersController : "+userid);
            return "user "+userid+": "+ UUID.randomUUID();
        }
    }
    
    ```


2. 在consumer一端，需要创建一个interface,其中的抽象方法和Provider一侧是一致的
   ```java
    @FeignClient("nacos-provider")
    public interface UsersService {
    /**
     * 这里的方法和在服务端Provider提供的是一致的
     * @param userid
     * @return
     */
     @GetMapping("user/{userid}")
     public String getUser(@PathVariable Integer userid);
     }

   ```
  注意 @FeignClient("nacos-provider")参数表示服务提供者的名字(去nacos控制台查看);
  
3. 在Consumer一侧的控制器中.
```java
  @RestController
    @Slf4j
    public class NacosUsersController {
    
        //这里要调用接口对象
        @Autowired
        private UsersService  usersService;
    
        @GetMapping("consumer/{userid}")
        public String getById(@PathVariable Integer userid) {
            log.info("NacosUsersController :"+usersService);
            String user = usersService.getUser(userid);
            return user;
        }
    }
  
```
↑面的操作在接口中，注入了上一步的service对象,然后在调用这个业务方法的时候就和本地调用一样.

4. 启动类上加入feign的支持.
   //开启feignclient的支持
   @EnableFeignClients