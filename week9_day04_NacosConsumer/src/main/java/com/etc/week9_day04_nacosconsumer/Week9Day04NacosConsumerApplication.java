package com.etc.week9_day04_nacosconsumer;

import com.etc.week9_day04_nacosconsumer.config.CustomLoadBalancerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
//开启服务发现的客户端
@EnableDiscoveryClient
//开启feignclient的支持
@EnableFeignClients
//开启LoadBalancerClient,指定provider的名字，还有负载均衡的策略
@LoadBalancerClient(name="nacos-provider",configuration= CustomLoadBalancerConfiguration.class)
public class Week9Day04NacosConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week9Day04NacosConsumerApplication.class, args);
    }

}
