package com.etc.week9_day04_nacosconsumer.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Slf4j
public class NacosUsersController {

    //这里要调用接口对象
    @Autowired
    private UsersService  usersService;

    @GetMapping("consumer/{userid}")
    public String getById(@PathVariable Integer userid) {
        log.info("NacosUsersController :"+usersService);
        String user = usersService.getUser(userid);
        return user;
    }
}