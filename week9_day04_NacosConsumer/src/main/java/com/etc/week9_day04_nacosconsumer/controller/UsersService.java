package com.etc.week9_day04_nacosconsumer.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @FeignClient("nacos-provider") 实现和Provider关联的参数
 *
 */
@FeignClient("nacos-provider")
public interface UsersService {
    /**
     * 这里的方法和在服务端Provider提供的是一致的
     * @param userid
     * @return
     */
    @GetMapping("user/{userid}")
    public String getUser(@PathVariable Integer userid);
}