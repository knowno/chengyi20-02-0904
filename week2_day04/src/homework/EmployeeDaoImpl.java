package homework;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 后续是要和文件以及数据库整合在一起的
 * 此时，我们就通过一个集合来模拟
 */
public class EmployeeDaoImpl implements EmployeeDao {

    List<Employee> list = new ArrayList<>();

    @Override
    public boolean addEmp(Employee employee) {
        //员工编号能否重复？
        list.add(employee);
        return true;
    }

    @Override
    public boolean updateEmp(Employee employee) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmpno().equals(employee.getEmpno())) {
                //找到了这个人
                list.set(i, employee);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean delEmp(String empno) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmpno().equals(empno)) {
                //找到了这个人
                list.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public Employee getEmp(String empno) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmpno().equals(empno)) {
                //找到了这个人
                return list.get(i);
            }
        }
        return null;
    }

    @Override
    public List<Employee> getEmps(String keywords) {
        List<Employee> returnlist = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmpname().contains(keywords)) {
                //找到了这个人
                returnlist.add(list.get(i));
            }
        }
        return returnlist;
    }

    @Override
    public List<Employee> getEmpsByDept(String deptName) {
        List<Employee> returnlist = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDept().getDeptname().equals(deptName)) {
                //找到了这个人
                returnlist.add(list.get(i));
            }
        }
        return returnlist;
    }

    /**
     * 保存员工信息到文件中
     *
     * @param path
     * @return
     * @throws IOException
     */
    @Override
    public boolean saveToFile(String path) throws IOException {
        //FileWriter/BufferedWriter/FileOutputStream
        FileWriter fileWriter = new FileWriter(path);
        //尝试将ArrayList中的数据进行存储,只能存储字符串
        for (Employee e : list) {
            String s = e.toString();
            fileWriter.write(s + "\r\n");
        }
        //关闭
        fileWriter.close();
        return true;

    }

    /**
     * 读取
     *
     * @param path
     * @return
     * @throws IOException
     */
    @Override
    public String readFromFile(String path) throws IOException {
        Reader reader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String str = null;
        StringBuffer result = new StringBuffer();

        while ((str = bufferedReader.readLine()) != null) {
            result.append(str);
        }
        bufferedReader.close();
        reader.close();
        return result.toString();
    }

    /**
     * 写入的方法
     *
     * @param path
     * @param emps
     * @return
     * @throws IOException
     */
    @Override
    public boolean saveToFile3(String path, List<Employee> emps) throws IOException {
        //1 FileOutputStream
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        //2
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        //3 writeObject
        objectOutputStream.writeObject(emps);
        //4 释放资源
        objectOutputStream.close();
        fileOutputStream.close();
        return true;
    }

    @Override
    public boolean saveToFile2(String path, List<Employee> emps) throws IOException {
        return false;
    }


    /**
     * 读取文件内容，反序列化
     *
     * @param path
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public List<Employee> readFromFile2(String path) throws IOException, ClassNotFoundException {
        //1 FileInputStream
        FileInputStream fileInputStream = new FileInputStream(path);
        //2 ObjectInputStream
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        //3读取
        Object object = objectInputStream.readObject();
        List<Employee> list = null;
        if (object instanceof List) {
            //判断并转型
            list = (List<Employee>) object;

        }
        objectInputStream.close();
        fileInputStream.close();
        return list;
    }
}
