package homework;

import java.io.IOException;
import java.util.List;

/**
 * 新建员工数据操作类：定义新增员工，查询员工，修改员工，删除员工的方法
 */
public interface EmployeeDao {

    public boolean addEmp(Employee employee);

    public boolean updateEmp(Employee employee);

    public boolean delEmp(String empno);

    public Employee getEmp(String empno);

    public List<Employee> getEmps(String keywords);

    public List<Employee> getEmpsByDept(String deptName);

    public boolean saveToFile(String path) throws IOException;

    // 目前也只能读取String,而实际的需求，我们希望能得到Employee对象
    //暂时这样处理
    public String readFromFile(String path) throws IOException;


    /**
     * 写入文件的时候，不是按照普通字符串去写,可以用json格式的字符串保存.
     * @param path
     * @param emps
     * @return
     * @throws IOException
     */
    public boolean saveToFile3(String path, List<Employee> emps) throws IOException;


    /**
     * 写入文件的时候，不是按照普通字符串去写,可以以对象的形式直接存储.
     * @param path
     * @param emps
     * @return
     * @throws IOException
     */
    public boolean saveToFile2(String path, List<Employee> emps) throws IOException;

    /**
     * 读取文件的时候，不是按照普通字符串去读,可以以对象的形式直接读取.
     * @param path
     * @return
     * @throws IOException
     */
    public List<Employee> readFromFile2(String path) throws IOException, ClassNotFoundException;

}
