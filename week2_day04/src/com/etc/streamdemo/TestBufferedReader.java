package com.etc.streamdemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * 读取文本文件中字符串到java
 * 1. FileInputStream
 * 2. FileReader
 * 3. BufferedReader(简单)
 */
public class TestBufferedReader {
    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader("e:\\0915.txt");

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        //bufferedReader
        String str = null;
        while ((str = bufferedReader.readLine()) != null) {
            System.out.println(str);
        }

        bufferedReader.close();//关闭流
        fileReader.close();//关闭流


    }
}
