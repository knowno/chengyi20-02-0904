package com.etc.streamdemo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * 将一段字符串写入文本文件中
 * 1. FileOuputStream
 * 2. FileWriter
 * 3. BufferedWriter
 */
public class TestBufferedWriter {
    public static void main(String[] args) throws IOException {

        FileWriter fileWriter1 = new FileWriter("e:\\0915.txt",true);
        //0914文件不存在 =>自动创建文件
        BufferedWriter bf = new BufferedWriter(fileWriter1);
        //1w     =>\r\n换行
        bf.write("Hello Java\r\n");
        //2f
        bf.flush();
        //3c
        bf.close();
        System.out.println("写入成功!");
    }
}
