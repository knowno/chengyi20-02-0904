package com.etc.streamdemo;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

/**
 * 将一段字符串写入文本文件中
 * 1. FileOuputStream
 * 2. FileWriter (简单)
 * 3. BufferedWriter
 */
public class TestFileWriter {
    public static void main(String[] args) throws IOException {
        //1.

        //0914文件不存在 =>自动创建文件
        FileWriter fileWriter = new FileWriter("e:\\0914.txt",true);
        //1w     =>\r\n换行
        fileWriter.write("\r\nHello FileWriter");
        //2f
        fileWriter.flush();
        //3c
        fileWriter.close();
        System.out.println("写入成功!");


    }
}
