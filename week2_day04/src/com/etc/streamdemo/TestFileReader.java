package com.etc.streamdemo;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 读取文本文件中字符串到java
 * 1. FileInputStream
 * 2. FileReader
 * 3. BufferedReader
 */
public class TestFileReader {
    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader("e:\\0915.txt");
        //使用FileReader处理流
        File file = new File("e:\\0915.txt");
        char [] ch = new char[(int) file.length()];
        int read = fileReader.read(ch);
        fileReader.close();//关闭流
        System.out.println(new String(ch));

    }
}
