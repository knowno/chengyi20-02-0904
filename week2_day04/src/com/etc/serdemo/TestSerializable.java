package com.etc.serdemo;

import homework.Dept;
import homework.Employee;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.time.LocalDate;

/**
 * 序列化:理解为把一个对象"拍扁"了，存起来.
 * 要求"相关(Employee,Dept)"的类都要实现序列化接口，否则
 * java.io.NotSerializableException: homework.Employee
 * ObjectOutputStream
 */
public class TestSerializable {
    public static void main(String[] args) throws IOException {

        //序列化
        //1 创建一个FileOutputStream
        OutputStream out = new FileOutputStream("e:\\employee.ini");
        //2 创建ObjectOutputStream对象，将FileOutputStream作为参数
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
        //3调用方法实现序列化
        //java.io.NotSerializableException: homework.Employee
        Dept dept1 = new Dept(1,"开发部","开发部门");
        Employee employee1 = new Employee("202301","张三", LocalDate.of(2000,12,12),23,LocalDate.now(),"13612345678",dept1,5234);

        objectOutputStream.writeObject(employee1);

        //4 释放资源
        objectOutputStream.close();
        out.close();

    }
}
