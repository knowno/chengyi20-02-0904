package com.etc.serdemo;

import homework.Dept;
import homework.Employee;

import java.io.*;
import java.time.LocalDate;

/**
 * 反序列化:理解为把一个"拍扁"了对象，满血复活起来.
 * <p>
 *     反过程中，会将文件中的serialVersionUID =》local class serialVersionUID 进行比较
 * ObjectInputStream
 */
public class TestDeSerializable {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        //反序列化
        //1 创建一个FileInputStream
        InputStream in = new FileInputStream("e:\\employee.ini");
        //2 创建ObjectInputStream对象，将FileInputStream作为参数
        ObjectInputStream objectInputStream = new ObjectInputStream(in);
        //3调用方法实现反序列化

        Object o = objectInputStream.readObject();
        if (o instanceof Employee) {
            //转换o为Employee
            Employee employee = (Employee) o;
            System.out.println(employee);
            System.out.println(employee.getDept());
        }

        //4 释放资源
        objectInputStream.close();
        in.close();

    }
}
