package com.etc.filedemo;

import java.io.File;
import java.util.Arrays;

/**
 * 列表展示磁盘某个目录下的文件(要求遍历到最后一级,包含所有子目录下的文件)
 */
public class TestFile3 {
    public static void main(String[] args) {

        //创建一个File对象，参数是pathname 可以是一个目录，也可以是一个文件
        File file1 = new File("E:\\2023-09-诚毅软工后端2班");

        //调用自定义函数遍历并显示目录和子目录下的文件
        showFiles(file1);

    }

    public static void showFiles(File file) {
        //File 数组
        File[] files = file.listFiles();
        if (files != null) {

            for (File f : files) {
                if (f.isFile()) {
                    System.out.println(f.getAbsolutePath());
                }
                //如果不是File,是Directory,递归调用
                else if (f.isDirectory()) {
                    showFiles(f);
                }
            }
        }
    }
}
