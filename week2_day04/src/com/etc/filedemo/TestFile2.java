package com.etc.filedemo;

import java.io.File;
import java.util.Arrays;

public class TestFile2 {
    public static void main(String[] args) {

        //创建一个File对象，参数是pathname 可以是一个目录，也可以是一个文件
        File file1 = new File("E:\\2023-09-诚毅软工后端2班");

        //public String[] list() {
        System.out.println(Arrays.toString(file1.list()));

        //File 数组
        File[] files = file1.listFiles();

        for (File file:files) {
            System.out.println(file.getName()+", "+file.getAbsolutePath());
        }

    }
}
