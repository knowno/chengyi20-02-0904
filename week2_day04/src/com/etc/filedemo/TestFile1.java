package com.etc.filedemo;

import java.io.File;

public class TestFile1 {
    public static void main(String[] args) {

        //创建一个File对象，参数是pathname 可以是一个目录，也可以是一个文件
        File file1 = new File("E:\\2023-09-诚毅软工后端2班");

        System.out.println(file1);
        //查看这个file对象的常见方法
        System.out.println(file1.isDirectory()+" "+file1.isFile());
        File file2 = new File("E:\\2023-09-诚毅软工后端2班\\2-授课期\\1-标准化教案\\第1周Java面向对象和常见API\\2-本周word讲义或电子笔记\\HelloWorld执行过程解析.drawio");

        System.out.println(file2);
        System.out.println(file2.isDirectory()+" "+file2.isFile());


    }
}
