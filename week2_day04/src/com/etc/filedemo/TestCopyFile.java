package com.etc.filedemo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestCopyFile {
    public static void main(String[] args) {

        //调用文件复制的方法来copy文件
        copyFile("f:\\soft\\devecostudio-windows-tool-2.0.12.201.zip","e:\\test0914.zip");

    }

    /**
     * 文件复制
     *
     * @param srcPath  要复制的文件位置
     * @param destPath 目标位置
     */
    public static void copyFile(String srcPath, String destPath) {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(srcPath);

            fileOutputStream = new FileOutputStream(destPath);

            //读取
            byte[] b = new byte[1024];
            int len = 0;
            while ((len = fileInputStream.read(b)) != -1) {
                //读到了数据 存到byte数组中.
                fileOutputStream.write(b);
            }

            System.out.println("文件 " + srcPath + " 复制成功");

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }


    }


}
