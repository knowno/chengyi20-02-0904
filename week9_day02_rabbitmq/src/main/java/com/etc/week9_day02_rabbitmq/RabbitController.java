package com.etc.week9_day02_rabbitmq;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RabbitController {

    //调用RabbitMQSender
    @Autowired
    private  RabbitMQSender rabbitMQSender;

    @GetMapping("rabbitmq/{username}")
    public String hellomq(@PathVariable("username") String username){
        String send = rabbitMQSender.send(username);
        return "P 发送者: "+send;
    }
}
