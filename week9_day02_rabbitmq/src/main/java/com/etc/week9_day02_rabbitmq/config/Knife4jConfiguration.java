package com.etc.week9_day02_rabbitmq.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * https://doc.xiaominfo.com/docs/quick-start
 */
@Configuration
@EnableSwagger2
public class Knife4jConfiguration {

    @Bean(value = "RabbitMQ")
    public Docket createAPI() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("接口文档列表")
                        .description("接口文档")
                        .termsOfServiceUrl("http://testapi.com.cn")
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("SpringBoot-RabbitMQ")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.etc.week9_day02_rabbitmq.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

}
