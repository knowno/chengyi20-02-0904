package com.etc.week9_day02_rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author Administrator
 */
@Configuration
public class RabbitMQConfig {
        // 队列名称
        public static final String QUEUE_NAME = "email_queue";
        // 交换器名称
        public static final String EXCHANGE = "email_exchange";
        // 路由键名称
        public static final String ROUTEKEY = "email_routekey";
        

        /**
         * @Description: 创建队列
         * @return: org.springframework.amqp.rabbit.annotation.Queue
         */
        @Bean
        Queue queue(){
            // 是否持久化
            boolean durable = true;
            // 仅创建者可以使用的私有队列，断开后自动删除
            boolean exclusive = false;
            // 当所有消费客户端连接端库后，是否自动删除队列
            boolean autoDelete = false;
            return new Queue(QUEUE_NAME, durable,exclusive,autoDelete);
        }

        /**
         * @Description: 创建Topic交换器
         * @return: org.springframework.amqp.core.TopicExchange
         */
        @Bean
        public  TopicExchange exchange(){
            return new TopicExchange(EXCHANGE,true,false);
        }

        /**
         * @Description: 绑定交换器和队列通过路由键
         * @return: org.springframework.amqp.core.Binding
         */
        @Bean
        Binding binding(){
            return BindingBuilder.bind(queue()).to(exchange()).with(ROUTEKEY);
        }

        @Bean
        public Jackson2JsonMessageConverter messageConverter(){
            return new Jackson2JsonMessageConverter();
        }


}
