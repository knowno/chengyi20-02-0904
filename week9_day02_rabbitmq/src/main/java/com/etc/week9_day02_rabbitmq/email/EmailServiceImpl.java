package com.etc.week9_day02_rabbitmq.email;

import com.etc.week9_day02_rabbitmq.config.RabbitMQConfig;
import com.etc.week9_day02_rabbitmq.dto.EmailDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void sendEmailMessage(EmailDto emailDto) {
        //转换和发送消息
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE,RabbitMQConfig.ROUTEKEY, emailDto);
        log.info("emailDto: "+emailDto);
    }
}
