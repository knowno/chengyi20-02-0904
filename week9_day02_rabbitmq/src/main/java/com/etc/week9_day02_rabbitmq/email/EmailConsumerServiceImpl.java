package com.etc.week9_day02_rabbitmq.email;

import cn.hutool.extra.mail.MailUtil;
import com.etc.week9_day02_rabbitmq.config.RabbitMQConfig;
import com.etc.week9_day02_rabbitmq.dto.EmailDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * @author knowno
 */
@Service
@Slf4j
public class EmailConsumerServiceImpl implements EmailConsumerService {
    @Override
    public void sendEmail(EmailDto emailDto) {

        log.info("发送邮箱地址： " + emailDto.getTo());
        //封装的批量发送邮件的方法
        MailUtil.send(emailDto.getTo(), "通知", emailDto.getContent(), true);
        log.info("message： " + emailDto.getContent());
    }

    @RabbitListener(queues = RabbitMQConfig.QUEUE_NAME)
    private void consumerEmail(EmailDto emailDto) {
        sendEmail(emailDto);
    }
}
