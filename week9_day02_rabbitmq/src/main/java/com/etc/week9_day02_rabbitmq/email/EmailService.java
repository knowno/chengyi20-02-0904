package com.etc.week9_day02_rabbitmq.email;

import com.etc.week9_day02_rabbitmq.dto.EmailDto;

public interface EmailService {
    void sendEmailMessage(EmailDto emailDto);
}
