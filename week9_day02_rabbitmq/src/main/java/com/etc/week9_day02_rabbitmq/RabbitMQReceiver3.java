package com.etc.week9_day02_rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "hello")
public class RabbitMQReceiver3 {

//在接收处理消息的方法上声明@RabbitHandler
    @RabbitHandler
    public void receive(String info) {
        System.out.println("C3 RabbitMQReceiver :"+info);
    }

}
