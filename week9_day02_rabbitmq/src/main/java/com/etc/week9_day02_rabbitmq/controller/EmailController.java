package com.etc.week9_day02_rabbitmq.controller;

import com.etc.week9_day02_rabbitmq.dto.EmailDto;
import com.etc.week9_day02_rabbitmq.email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Administrator
 */
@RestController
public class EmailController {
    @Autowired
    private EmailService emailService;

    @PostMapping("/sendemail")
    public String sendmail(@RequestBody EmailDto emailDto) {
        emailService.sendEmailMessage(emailDto);
        return "邮件已经发送";
    }
}
