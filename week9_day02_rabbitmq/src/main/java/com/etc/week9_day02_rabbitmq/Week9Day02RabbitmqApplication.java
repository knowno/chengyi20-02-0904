package com.etc.week9_day02_rabbitmq;

import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling //启用定时任务
public class Week9Day02RabbitmqApplication {

    //返回一个Bean
    @Bean
    public Queue helloQueue(){
        // public Queue(String name)  参数为队列名字
        return new Queue("hello");
    }

    public static void main(String[] args) {
        SpringApplication.run(Week9Day02RabbitmqApplication.class, args);
    }

}
