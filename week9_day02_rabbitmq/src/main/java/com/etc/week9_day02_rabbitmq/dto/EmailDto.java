package com.etc.week9_day02_rabbitmq.dto;

import java.util.List;

/**
 * 输出传输对象，包含
 * 收件人邮箱地址列表
 * 发件人
 * 主题
 * 内容
 *
 */
public class EmailDto {
    private List<String> to;
    private String from;
    private String subject;
    private String content;

    @Override
    public String toString() {
        return "EmailDto{" +
                "to=" + to +
                ", from='" + from + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
