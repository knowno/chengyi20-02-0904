package com.etc.week9_day02_rabbitmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class RabbitMQSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

//    @Autowired
//    private RabbitTemplate rabbitTemplate;

    public String send(String username){
        //构造一个info对象
        String info = username + " : "+ LocalDateTime.now();
        //convertAndSend =>转换一个Java对象为Amqp消息，然后再用缺省的交换机指定路由键发送消息。
        amqpTemplate.convertAndSend("hello",info);
        return info;
    }

}
