package com.etc.week9_day02_rabbitmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RabbitListener(queues = "hello")
public class RabbitMQReceiver {

//在接收处理消息的方法上声明@RabbitHandler
    @RabbitHandler
    public void receive(String info) {
        System.out.println("C RabbitMQReceiver :"+info);
    }

}
