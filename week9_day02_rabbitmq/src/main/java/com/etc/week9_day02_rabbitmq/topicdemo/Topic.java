package com.etc.week9_day02_rabbitmq.topicdemo;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类Topic
 */
@Configuration
public class Topic {

    /**
     * 队列topicA
     * @return Queue
     */
    @Bean
    public Queue topicA(){
        return new Queue("topicA");
    }

    /**
     * 队列topicB
     * @return  Queue
     */
    @Bean
    public Queue topicB(){
        return new Queue("topicB");
    }

    /**
     * 创建topicExchange
     * @return TopicExchange对象
     */
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange("topicExchange");
    }

    /**
     * Exchange和topicA绑定
     * @param topicExchange
     * @param topicA
     * @return
     */
    @Bean
    public Binding bindingA(TopicExchange topicExchange,
                            Queue topicA) {
        return BindingBuilder.bind(topicA).to(topicExchange).with("topic.A");
    }


    /**
     * * (star) can substitute for exactly one word.(1个)
     * # (hash) can substitute for zero or more words.(0个或者多个)
     * topicExchange和topicB绑定
     * @param topicExchange
     * @param topicB
     * @return
     */
    @Bean
    public Binding bindingB(TopicExchange topicExchange,
                            Queue topicB) {
        return BindingBuilder.bind(topicB).to(topicExchange).with("topic.#");
    }
}
