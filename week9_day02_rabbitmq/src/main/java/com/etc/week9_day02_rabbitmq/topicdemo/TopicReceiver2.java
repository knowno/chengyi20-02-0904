package com.etc.week9_day02_rabbitmq.topicdemo;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "topicB")
public class TopicReceiver2 {

//在接收处理消息的方法上声明@RabbitHandler
    @RabbitHandler
    public void receive(String info) {
        System.out.println("topicB Receiver2 RabbitMQReceiver :"+info);
    }

}
