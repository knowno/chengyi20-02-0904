package com.etc.week9_day02_rabbitmq.topicdemo;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 发送者类
 */
@Component
public class TopicSender {
    @Autowired
    private RabbitTemplate template;

    @Autowired
    private TopicExchange topicExchange;

    AtomicInteger dots = new AtomicInteger(0);

    AtomicInteger count = new AtomicInteger(0);

    /**
     * 定时任务
     */
    @Scheduled(fixedDelay = 1000, initialDelay = 500)
    public void send() {
        StringBuilder builder = new StringBuilder("Hello");
        if (dots.getAndIncrement() == 3) {
            dots.set(1);
        }
        for (int i = 0; i < dots.get(); i++) {
            builder.append('.');
        }
        builder.append(count.incrementAndGet());
        String message = builder.toString();
        //这里的方法发生了变化 ,第一参数是exchange对象
        template.convertAndSend(topicExchange.getName(), "topic.A", message);
        System.out.println(" [x] Sent '" + message + "'");
    }
}
