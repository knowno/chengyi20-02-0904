package com.etc.week9_day02_rabbitmq.fanoutdemo;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类Fanout
 */
@Configuration
public class Fanout {

    /**
     * 队列fa
     * @return
     */
    @Bean
    public Queue faQueue(){
        return new Queue("fanout.a");
    }

    /**
     * 队列fb
     * @return
     */
    @Bean
    public Queue fbQueue(){
        return new Queue("fanout.b");
    }

    /**
     * 创建Exchange
     * @return
     */
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("fanoutExchange");
    }

    /**
     * Exchange和fa绑定(fanout.a)
     * @param fanout
     * @param faQueue
     * @return
     */
    @Bean
    public Binding binding1(FanoutExchange fanout,
                            Queue faQueue) {
        return BindingBuilder.bind(faQueue).to(fanout);
    }


    /**
     * Exchange和fb绑定(fanout.b)
     * @param fanout
     * @param fbQueue
     * @return
     */
    @Bean
    public Binding binding2(FanoutExchange fanout,
                            Queue fbQueue) {
        return BindingBuilder.bind(fbQueue).to(fanout);
    }
}
