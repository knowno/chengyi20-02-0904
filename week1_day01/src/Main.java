public class Main {

    // public static int a  =12;
    public static void main(String[] args) {
        //String str 称为引用
        // "hello world" 字面值，常量
        String str = "hello world";
//        String str1 = new String("hello world");
        //查看源码方式 ctrl+鼠标左键
        System.out.println(str);
    }

}
