package com.etc.micro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudMicroApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudMicroApplication.class, args);
    }

}
