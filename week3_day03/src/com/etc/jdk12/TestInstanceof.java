package com.etc.jdk12;

public class TestInstanceof {
    public static void main(String[] args) {

        Object str  = "I Love Java";
        if (str instanceof String ) {
            String s =(String)str;
            System.out.println(s.charAt(0));
        }
        System.out.println("***********************");

        Object str1  = "I Love Java";
        //语法 变量  instanceof 类型  新变量名  =>String  s
        if (str1 instanceof String  s) {
            System.out.println(s.charAt(0));
        }

    }
}
