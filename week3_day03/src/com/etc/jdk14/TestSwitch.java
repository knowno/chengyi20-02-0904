package com.etc.jdk14;

public class TestSwitch {
    public static void main(String[] args) {

        int day = 7;
        String result = switch (day) {
            case 1, 2, 3, 4, 5 -> {
                yield "weekday";
            }
            case 6, 7 -> {
                yield "weekend";
            }
            default -> {
                yield "error";
            }
        };

        System.out.println(result);

    }
}
