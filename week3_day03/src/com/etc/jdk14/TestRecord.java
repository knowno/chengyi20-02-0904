package com.etc.jdk14;

public class TestRecord {
    public static void main(String[] args) {

        MyShape myShape = new MyShape(3);
        System.out.println(myShape.r());

    }
}

interface  Shape{

}
class Circle implements  Shape{
    private double r;

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public Circle(double r) {
        this.r = r;
    }
}
record MyShape(double r) implements Shape {

}
