package com.etc.jdk10;

import java.time.LocalDate;
import java.util.List;

public class TestVar {
    public static void main(String[] args) {

        var i = new Integer(10);
        var integer = Integer.valueOf(100);
        System.out.println(i instanceof Integer);
        System.out.println(integer instanceof Integer);

        var str ="java";
        System.out.println(str instanceof  String);

        var list = List.of("java", 1, LocalDate.now());


        var array1 = new int[]{1, 2, 3};

        //Array initializer is not allowed here
//        var array2 = {1,2,3};

    }
}
