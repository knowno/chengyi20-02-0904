package com.etc.jdk9;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * jdk9 集合接口提供了静态的方法便捷的创建集合对象
 */
public class TestList {
    public static void main(String[] args) {
        //以前 Arrays.asList("xxxxx")
        // new ArrayList(); list.add...

        //List集合对象创建
        List list = List.of("java", 1, LocalDate.now());

        Set set = Set.of("java","China","MySQL");

        Map map = Map.of(1,"java",2,"MySQL");

        System.out.println(list);
        System.out.println(set);
        System.out.println(map);


    }
}
