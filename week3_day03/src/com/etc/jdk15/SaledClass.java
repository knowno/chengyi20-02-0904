package com.etc.jdk15;

public class SaledClass {
    public static void main(String[] args) {

    }
}
//Sealed class must have subclasses
//编译阶段就会检查，看这个A有没有subclass=>子类
sealed  class A{

}
//sealed, non-sealed or final modifiers expected
non-sealed class SubClass extends  A{

}

final class SubClass1 extends  A{

}
class SubClass2 extends  SubClass{

}

sealed class SubClass3 extends  SubClass permits  SubClass4{

}

//这里只能使用SubClass4的名字来继承SubClass3
non-sealed  class SubClass4 extends  SubClass3{

}


