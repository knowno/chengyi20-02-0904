package com.etc.jdk11;

public class TestString {
    public static void main(String[] args) {

        String str = "java&javascript";

        //since 11 ,如果字符串"" "  " =>true
        System.out.println(str.isBlank());
        //重复 6次
        System.out.println(str.repeat(6));

        String str1 = " j a v a ";
        //删除前后空格
        System.out.println(str1.strip());
        //删除前空格
        System.out.println(str1.stripLeading());
        //删除后空格
        System.out.println(str1.stripTrailing());

    }
}
