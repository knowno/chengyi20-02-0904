package com.etc.jdk8;

import java.util.Optional;

public class TestOptional {
    public static void main(String[] args) {

        Object obj = null;
        Optional<Object> optional = Optional.ofNullable(obj);

        //  optional.isPresent() 用来判断某个对象是否为null =>如果null  =>false
        System.out.println(optional.isPresent());
//        System.out.println(optional.get());


        Object obj1 = new Object();
        Optional<Object> optional1 = Optional.ofNullable(obj1);
        //  optional.isPresent() 用来判断某个对象是否为null =>如果非null  =>true
        System.out.println(optional1.isPresent());
        System.out.println(optional1.get());
    }
}
