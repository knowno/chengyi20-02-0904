package com.etc.jdk8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class TestStream {
    public static void main(String[] args) {

        // 1 数组工具类Arrays的方法
        Object[] arr = {1,2,4,7,9,0};
        Stream<Object> stream1 = Arrays.stream(arr);

        System.out.println(stream1.count());

        //2 使用集合的方法
        List<Integer> integers = Arrays.asList(1, 3, 5, 7, 9, 2);
        Stream<Integer> stream2 = integers.stream();

        //3 直接使用Stream的方法
        Stream<String> stream3 = Stream.of("abc", "xyz", "java","abc");
        //System.out.println(stream3.count());
//        System.out.println(stream3.distinct().count());
        stream3.forEach(System.out::println);

    }
}
