package com.etc.jdk8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * lambda表达式
 * (参数)->{ // 代码段}
 */
public class TestArrayList2 {
    public static void main(String[] args) throws InterruptedException {

        //Collections.synchronizedList
        List<Object> objects = Collections.synchronizedList(new ArrayList<>());

        //循环
        for (int i = 1; i <= 50000; i++) {

            //创建线程对象 ->每循环一次，将这个线程对象name添加到list中
            //Runnable target 是 () ->{} 代替一个Runnable类型的对象,是一个接口实现语法规范.
            new Thread(()->{
                System.out.println("test");
            }).start();
        }

        //加一个延迟
        Thread.sleep(10000);
        //多次试验表明，size的值都无法达到50000
        System.out.println(objects.size());

    }

}


