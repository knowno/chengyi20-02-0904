package com.etc.jdk8;

import com.etc.jdk8.stream.Employee;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class TestMethod {
    public static void main(String[] args) {

        Person p1 = new Person(1, "小白");
        Person p2 = new Person(2, "小黑");

        List<Person> people = Arrays.asList(p1, p2);

        //遍历并输出集合元素
        //1 传统方法
        for (Person p : people) {
            System.out.println(p.getId() + "," + p.getName());
        }
        System.out.println("****************************");
        //2 使用方法引用  对象名::方法
        people.forEach(System.out::println);
        System.out.println("****************************");
        //2-1 如果我想输出对象的某个信息呢? p.getName
        people.forEach((x)->{
            System.out.println(x.getName());
        });

        //4 定义一个方法 赋值给一个接口Consumer接口
        Consumer con1 = System.out::println;
        //调用Consumer accpet方法的时候就相当于调用println
        con1.accept("hello");


        Comparator<Integer> compare = Integer::compare;
        int compare1 = compare.compare(1, 2);
        System.out.println(compare1);

    }
}

class Person {
    private int id;
    private String name;

    public Person() {
    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
