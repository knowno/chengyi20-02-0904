package com.etc.jdk8;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * lambda表达式
 * (参数)->{ // 代码段}
 */
public class TestTreeSet {
    public static void main(String[] args) throws InterruptedException {

        //没有使用lambda表达式的写法
        TreeSet<String> treeSet = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return 0;
            }
        });

        TreeSet<String> treeSet1 = new TreeSet<>((o1, o2) -> {
            {
                return Integer.valueOf(o1) - Integer.valueOf(o2);
            }
        });



    }

}


