package com.etc.jdk8.stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

public class TestEmployeeStream {
    public static void main(String[] args) {
        //创建对象
        Employee p1 = new Employee(1, "a", 12);
        Employee p2 = new Employee(2, "b", 13);
        Employee p3 = new Employee(3, "c", 11);
        Employee p4 = new Employee(4, "d", 10);
        Employee p5 = new Employee(1, "a", 12);

        //1 将对象添加到集合
        List<Employee> list = Arrays.asList(p1, p2, p3, p4, p5);

        System.out.println(list.size());
        System.out.println("*********distinct() 去除重复*****************");
        //2 使用list对象的方法得到一个流对象 =>去重复
        list.stream().distinct().forEach(System.out::println);
        System.out.println("*********sorted 进行排序  重写方法*****************");
        //3  排序=> Stream<T> sorted(Comparator<? super T> comparator)
        list.stream().sorted((o1, o2) -> {
            {
                return o1.getAge() - o2.getAge();
            }
        }).forEach(System.out::println);
        System.out.println("*********sorted 进行排序  Comparator.comparing *****************");
        list.stream().sorted(Comparator.comparing(Employee::getAge)).forEach(System.out::println);
        System.out.println("*********filter 进行过滤  *****************");
        list.stream().filter((Employee employee) -> employee.getAge() > 11).forEach(System.out::println);

        //limit(n) 截断(提取前n个)
        list.stream().filter((Employee employee) -> employee.getAge() > 11).limit(2).forEach(System.out::println);
        System.out.println("*********filter summaryStatistics *****************");
        //统计 intSummaryStatistics =>summaryStatistics
        IntSummaryStatistics intSummaryStatistics = list.stream().mapToInt(p -> p.getAge()).summaryStatistics();
        System.out.println(intSummaryStatistics);

        System.out.println("*********collect  *****************");
        List<String> collect = list.stream().map(p -> p.getName().toUpperCase()).collect(Collectors.toList());
        collect.forEach(System.out::println);
        //思考，如果没有stream的操作，我们如何实现从集合中提取name信息，并将name转换为大写字母呢?


    }
}
