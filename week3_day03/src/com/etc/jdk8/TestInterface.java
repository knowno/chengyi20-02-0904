package com.etc.jdk8;

public class TestInterface {

    public static void main(String[] args) {

        MyInterface.test2();
        MyInterface myInterface = new MyInterface(){
        };
        myInterface.test1();

    }
}

interface MyInterface {

    default void test1() {
        System.out.println("jdk 1.8 default method ..");
    }

    static void test2() {
        System.out.println("jdk 1.8 static method ...");
    }

    private void test3(){
        System.out.println("jdk private method");
    }
}
