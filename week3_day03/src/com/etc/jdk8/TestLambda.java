package com.etc.jdk8;

public class TestLambda {
    public static void main(String[] args) {

        TestIA.test1(new IA() {
            @Override
            public void print() {
                System.out.println("匿名内部类");
            }
        });


        TestIA.test1(() -> {
            System.out.println("拉姆达表达式");
        });


    }
}


class TestIA {
    public static void test1(IA ia) {
        ia.print();
    }
}

interface IA {
    //抽象方法
    void print();
}
