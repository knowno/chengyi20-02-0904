package com.etc.jdk13;

public class TestString {
    public static void main(String[] args) {

        String sql = "select emp.id,emp.name,emp.sal,emp.hiredate,dept.deptname from emp" +
                "inner join dept" +
                "on emp.deptid = dept.deptid" +
                "where dept.deptid>2";

        System.out.println(sql);

        // """ sql """
        String sql2 = """
                select emp.id,emp.name,emp.sal,emp.hiredate,dept.deptname from emp
                inner join dept
                on emp.deptid = dept.deptid
                where dept.deptid >2
                """;

        System.out.println(sql2);

    }
}
