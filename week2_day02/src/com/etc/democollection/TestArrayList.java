package com.etc.democollection;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestArrayList {
    public static void main(String[] args) {

        //创建对象
        ArrayList arrayList = new ArrayList();
        //1 增加
        arrayList.add("java");
        arrayList.add(LocalDate.now());
        arrayList.add("小白");
        arrayList.add(1);
        arrayList.add(null);
        System.out.println("list.size: " + arrayList.size());
        //2 删除
        arrayList.remove(0);
        System.out.println("list.size: " + arrayList.size());
        //3 输出
        arrayList.forEach(System.out::println);
        // 4 ->使用add方法insert 插入
        arrayList.add(1, "小黑");
        System.out.println("******************************");

        arrayList.forEach(System.out::println);

        // 5 替换操作=>set(index,object)
        for (int i = 0; i < arrayList.size(); i++) {
            //查找是否包含某某xx
            Object o = arrayList.get(i);
            if (o instanceof String) {
                if (o.equals("小白")) {
                    //将其修改为小黄
                    arrayList.set(i,"小黄");
                }
            }

        }
        System.out.println("******************************");

        arrayList.forEach(System.out::println);


    }
}
