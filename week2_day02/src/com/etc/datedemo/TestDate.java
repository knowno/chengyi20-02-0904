package com.etc.datedemo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestDate {
    public static void main(String[] args) throws ParseException {

        //Date
        Date date = new Date();
        System.out.println(date);
        //China Standard Time   =>UTC+8
        //Tue Sep 12 10:27:56 CST 2023

        //这个时间-》转换特定的格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //用它去格式化date
        String format = simpleDateFormat.format(date);
        System.out.println(format);


        //字符串->Date
        String str = "2023-09-12 10:31:16";
        Date parse = simpleDateFormat.parse(str);
        System.out.println(parse);


    }
}
