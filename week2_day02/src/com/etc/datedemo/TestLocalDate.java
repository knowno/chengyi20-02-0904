package com.etc.datedemo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 员工类: 入职日期 =>?String/Date/LocalDate
 */
public class TestLocalDate {
    public static void main(String[] args) throws ParseException {

        LocalDate now = LocalDate.now();
        System.out.println(now);

        LocalDate of = LocalDate.of(2008, 8, 8);
        System.out.println(of);

        //时间 日期对象有一个方法
        DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
        //LocalDate =>String
        String str = now.format(formater);
        System.out.println(str);

        //String=>LocalDate
       //使用LocalDate中的静态方法
        LocalDate localDate = LocalDate.parse(str, formater);

        System.out.println(localDate);

    }
}
