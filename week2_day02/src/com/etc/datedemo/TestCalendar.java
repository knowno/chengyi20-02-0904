package com.etc.datedemo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestCalendar {
    public static void main(String[] args) throws ParseException {


        //为啥不能new出来?
        Calendar instance = Calendar.getInstance();
        //输出看看
        System.out.println(instance);

        //格式转换 ->String
        // Calendar->Date ->String
        Date date = instance.getTime();
        //这个时间-》转换特定的格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //用它去格式化date
        String format = simpleDateFormat.format(date);
        System.out.println(format);

        //Date->Calendar
        instance.setTime(date);
        System.out.println(instance);



    }
}
