package com.etc;

public class Father {
    public void drive() {
        System.out.println("drive");
    }
}

class Mother{
    public void cook(){
        System.out.println("cook");
    }
}
class  Son extends  Father{

    class InnerClass extends  Mother{

    }

    public void skill(){
        //从父类继承而来的
        super.drive();
        //从母亲继承而来的
        new InnerClass().cook();
    }
}