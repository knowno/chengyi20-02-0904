package com.etc.classdemo;

import java.lang.reflect.Constructor;
import java.time.LocalDate;

/**
 * 平时我们编写代码的时候，任何对象都属于特定某个类型=>类就是对象的类型
 * Class类的类表示正在运行的Java应用程序中的类和接口
 */
public class TestClass {
    public static void main(String[] args) {

        //s就是String类型
        String s = new String("String");

        //其实 s还有一个"类型",而且我们会发现所有对象都能使用getClass方法
        //返回此xx类型 s对应的类 的运行时类
        Class cls = s.getClass();

        //得到公共的构造
        Constructor[] constructors = cls.getConstructors();
        System.out.println(constructors.length);

        LocalDate now = LocalDate.now();
        Class cls1 = now.getClass();
        Constructor[] constructors1 = cls1.getConstructors();
        System.out.println(constructors1.length);


        //Constructor对象的数组
        Constructor[] constructors2 = cls1.getDeclaredConstructors();
        System.out.println(constructors2.length);


//        Class.forName()//将某个类加载到JVM
    }
}
