package com.etc.classdemo;

import java.lang.reflect.Constructor;
import java.time.LocalDate;

/**
 * 平时我们编写代码的时候，任何对象都属于特定某个类型=>类就是对象的类型
 * Class类的类表示正在运行的Java应用程序中的类和接口
 */
public class Person {
   private int id;
   private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public Person() {
        System.out.println("Person类的无参数构造");
    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
