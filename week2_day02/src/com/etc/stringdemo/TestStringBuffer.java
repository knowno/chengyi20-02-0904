package com.etc.stringdemo;

/**
 * StringBuffer 可变字符串,内存堆.
 * 线程安全
 */
public class TestStringBuffer {
    public static void main(String[] args) {

        StringBuffer python = new StringBuffer("Python");
        python.append("&Java");
        System.out.println(python);

    }
}
