package com.etc.stringdemo;

public class TestString1 {
    public static void main(String[] args) {

        //字符串创建
        // 1直接赋字面值(字面值存储在字符串常量池)
        String str1 = "java";
        String str2 = "java";
        //比较的是地址，str1和str2的地址是一致的
        System.out.println(str1 == str2);

        //字符串String 不变性
        str1 = "Python";
        str2 = "C#";

        //2 拼接处理
       //不是在池中的
        str2 = str1 + str2;

        //如果是 "Python"+"C#" =>就是在池中

        //2  new
        String str3 = new String("java");
        String str4 = new String("java");
        System.out.println(str3 == str4);


    }
}
