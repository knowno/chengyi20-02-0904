package com.etc.stringdemo;

public class TestString3 {
    public static void main(String[] args) {
       //字面值
        String str = "ILoveJava";
        String str1 = "I";
        String str2 = "Love";
        String str3 = "Java";
        //使用 + 进行拼接 ,拼接的是引用的名称
        System.out.println(str == (str1 + str2 + str3));// 堆 false

        //使用 +进行拼接，字面量直接拼接，不是用变量名(引用)
        String str4 = "I" + "Love" + "Java";//字面值拼接 池
        System.out.println(str == str4);// true

    }
}
