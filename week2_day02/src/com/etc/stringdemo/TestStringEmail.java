package com.etc.stringdemo;

import java.util.Scanner;

/**
 * 如何提取一个字符串
 * zhangsan@sina.com
 */
public class TestStringEmail {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入您的邮箱");
        String email = scanner.next();

        //邮箱正则
        String str = "^[A-Za-z0-9\\u4e00-\\u9fa5]+(\\.[A-Za-z0-9\\u4e00-\\u9fa5]+)*@[A-Za-z0-9\\u4e00-\\u9fa5]+(\\.[A-Za-z0-9\\u4e00-\\u9fa5]+)*(\\.[A-Za-z\\u4e00-\\u9fa5]{2,})$";
        boolean matches = email.matches(str);
       // System.out.println(matches);

        if (matches){
            //找到@位置
            int index = email.indexOf("@");
            //提取@前面的部分
            String substring1 = email.substring(0, index);
            System.out.println(substring1);
        }
        else{
            System.out.println("邮箱格式不正确!");
        }




    }
}
