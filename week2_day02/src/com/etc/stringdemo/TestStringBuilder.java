package com.etc.stringdemo;

/**
 * StringBuilder 可变字符串,内存堆.
 * 线程不安全,单线程，效率高.
 */
public class TestStringBuilder {
    public static void main(String[] args) {

        StringBuilder python = new StringBuilder("Python");
        python.append("&Java");
        System.out.println(python);

    }
}
