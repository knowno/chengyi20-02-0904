package com.etc.stringdemo;

/**
 * 如何提取一个字符串
 * zhangsan@sina.com
 */
public class TestString2 {
    public static void main(String[] args) {

        //1 提取字符串的方法
        String email = "zhang@san@sina.com";
        //找到@位置
        int index = email.indexOf("@");
        //一个参数，从当前索引位置，到结尾
        String substring = email.substring(index + 1);
        System.out.println(substring);

        //提取@前面的部分
        String substring1 = email.substring(0, index);
        System.out.println(substring1);
        String str = "^[A-Za-z0-9\\u4e00-\\u9fa5]+(\\.[A-Za-z0-9\\u4e00-\\u9fa5]+)*@[A-Za-z0-9\\u4e00-\\u9fa5]+(\\.[A-Za-z0-9\\u4e00-\\u9fa5]+)*(\\.[A-Za-z\\u4e00-\\u9fa5]{2,})$";
        boolean matches = email.matches(str);
        System.out.println(matches);


    }
}
