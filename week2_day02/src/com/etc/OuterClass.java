package com.etc;

/**
 * 1。有些类需要定义，只是在当前类内部使用一次而已。不会给外部用。例如：Integer,IntegerCache
 * 2. 在类内部，要访问外部一些属性(闭包)
 * 3. 折中去处理我们Java继承的问题. ,例如一个Son->Father,Mother =>出现多继承解决方案.
 */
public class OuterClass {
    private int n = 100;

    /**
     * 1 成员内部类(可以直接调用外部类的成员)
     */
    class InnerClass {

        public void test1() {

            n++;
        }

    }

    /**
     * 在一个方法中定义了一个类
     * 2 . 局部内部类
     */
    public void testMethod() {
        class InnerClass2 {

            public void test1() {

                n++;
            }

        }
    }


    /**
     * 3 静态的内部类
     */
    static class InnerClass3 {

        public void test1() {
        }

    }

    //4 匿名内部类  和接口结合使用的(直接重写接口的方法，从而构造接口类型的对象.)
    IA ia = new IA(){
        @Override
        public void t1() {

        }
    };


    public void tt(){
        ia.t1();
    }
}

interface  IA{
    void t1();
}
