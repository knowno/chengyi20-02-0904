package com.etc.week4_day02_boot_mybatis.dao;


import com.etc.week4_day02_boot_mybatis.entity.Blog;

public interface BlogMapper {
    public Blog selectBlog(Integer id);
}
