package com.etc.week4_day02_boot_mybatis.entity;

public class Blog {
    private Integer id;
    private String blogtitle;
    private String content;

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", blogtitle='" + blogtitle + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBlogtitle() {
        return blogtitle;
    }

    public void setBlogtitle(String blogtitle) {
        this.blogtitle = blogtitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
