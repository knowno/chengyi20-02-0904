package com.etc.week4_day02_boot_mybatis;

import com.etc.week4_day02_boot_mybatis.dao.ArticleMapper;
import com.etc.week4_day02_boot_mybatis.entity.Article;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WeekDay02BootMybatisApplicationArticleMapperTests {

    //这里要使用ArticleMapper对象
    @Autowired
    private ArticleMapper articleMapper;


    @Test
    void getArticle() {
        System.out.println(articleMapper.selectByPrimaryKey(2L));
    }

    @Test
    void updateArticle() {
        // 修改其实现查 =>再改
        Article article = articleMapper.selectByPrimaryKey(2L);
        System.out.println(article);
        //修改
        article.setSummary("测试修改summary0917");
        article.setContent(null);
        int i = articleMapper.updateByPrimaryKeySelective(article);

    }

    @Test
    void updateArticle2() {
       //构造一个对象
        Article article = new Article();
        article.setId(2);
        article.setContent("测试修改content0917");
        article.setTitle("测试title0917");
        //修改
        int i = articleMapper.updateByPrimaryKeySelective(article);
    }

}
