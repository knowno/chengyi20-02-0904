import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        Person p = new Person();


        TreeSet set = new TreeSet();
        set.add(p);

        //外部比较器Comparator
//        Collections.sort(list, new Comparator<Object>() {
//            @Override
//            public int compare(Object o1, Object o2) {
//                return 0;
//            }
//        });

    }

}

/**
 * 内部比较器
 */
class Person implements  Comparable<Person>{
    int id;
    String name;

    @Override
    public int compareTo(Person o) {
        return this.id - o.id;
    }
}