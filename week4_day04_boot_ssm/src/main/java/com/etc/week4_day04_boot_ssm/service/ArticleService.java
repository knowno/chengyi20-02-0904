package com.etc.week4_day04_boot_ssm.service;

import com.etc.week4_day04_boot_ssm.entity.Article;

import java.util.List;

public interface ArticleService {
    /**
     * 根据id查询Article
     *
     * @param id 编号
     * @return
     */
    Article selectById(Long id);

    /**
     * 增加
     * @param article
     * @return
     */
    int add(Article article);

    /**
     * 修改
     * @param article
     * @return
     */
    int updateById(Article article);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleteById(Long id);

    /**
     * 分页和模糊查询
     * @param article
     * @return
     */
    List<Article> selectByLike(Article article,int page,int pagesize);
}

