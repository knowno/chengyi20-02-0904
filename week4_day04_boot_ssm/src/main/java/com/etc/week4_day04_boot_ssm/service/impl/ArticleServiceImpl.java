package com.etc.week4_day04_boot_ssm.service.impl;

import com.etc.week4_day04_boot_ssm.dao.ArticleMapper;
import com.etc.week4_day04_boot_ssm.entity.Article;
import com.etc.week4_day04_boot_ssm.service.ArticleService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务层的实现类
 */
@Service
public class ArticleServiceImpl implements ArticleService {
    /**
     * spring的特征： IOC/DI
     */
    @Autowired
    private ArticleMapper mapper;
    @Override
    public Article selectById(Long id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int add(Article article) {
        return mapper.insertSelective(article);
    }

    @Override
    public int updateById(Article article) {
        return mapper.updateByPrimaryKey(article);
    }

    @Override
    public int deleteById(Long id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<Article> selectByLike(Article article,int page,int pagesize) {
        //先调用PageHelper.startPage
        PageHelper.startPage(page,pagesize);
        //需要注意的是我们没有在mapper.xml文件中处理%%拼接，所以我们这里传递参数要保证
        //模糊查询的关键字是已经拼接好的
        return mapper.selectByLike(article);
    }
}
