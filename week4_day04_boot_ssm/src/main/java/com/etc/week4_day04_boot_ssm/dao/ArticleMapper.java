package com.etc.week4_day04_boot_ssm.dao;

import com.etc.week4_day04_boot_ssm.entity.Article;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author Administrator
* @description 针对表【article】的数据库操作Mapper
* @createDate 2023-09-28 13:53:18
* @Entity com.etc.week4_day04_boot_ssm.entity.Article
*/

@Repository
public interface ArticleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Article record);

    int insertSelective(Article record);

    Article selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKey(Article record);

    List<Article> selectByLike(Article article);

}
