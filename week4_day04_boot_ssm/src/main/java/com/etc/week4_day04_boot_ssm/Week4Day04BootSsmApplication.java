package com.etc.week4_day04_boot_ssm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.etc.week4_day04_boot_ssm.dao")
public class Week4Day04BootSsmApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week4Day04BootSsmApplication.class, args);
	}

}
