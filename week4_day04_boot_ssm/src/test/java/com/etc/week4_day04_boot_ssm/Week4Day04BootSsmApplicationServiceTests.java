package com.etc.week4_day04_boot_ssm;

import com.etc.week4_day04_boot_ssm.dao.ArticleMapper;
import com.etc.week4_day04_boot_ssm.service.ArticleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Week4Day04BootSsmApplicationServiceTests {

	@Autowired
	private ArticleService service;
	@Test
	void testArticle() {
		System.out.println(service.selectById(1L));
	}

}
