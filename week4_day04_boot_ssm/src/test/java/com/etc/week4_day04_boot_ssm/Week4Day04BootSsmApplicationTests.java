package com.etc.week4_day04_boot_ssm;

import com.etc.week4_day04_boot_ssm.dao.ArticleMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Week4Day04BootSsmApplicationTests {

	@Autowired
	private ArticleMapper mapper;
	@Test
	void testArticle() {
		System.out.println(mapper.selectByPrimaryKey(1L));
	}

}
