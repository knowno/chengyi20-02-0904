## 数据访问(Dao)操作步骤  =>Spring
1. 新建数据库，表，插入测试数据
2. 新建一个项目(moudule),添加关于springbootweb/mysqljdbc/mybatis支持
3. idea中添加数据库连接
![img.png](img.png)
4. 修改配置文件
![img_1.png](img_1.png)
5. 使用mybatisx插件来生成entity,dao,mapper.xml
![img_2.png](img_2.png)
6. 单元测试
![img_3.png](img_3.png)
7. 上面操作是通用代码，如果在dao访问层，涉及到其他查询操作，我们就手动去修改dao或者mapper.xml文件

## 业务层(service)的操作步骤  =>Spring
1. 业务层的接口
![img_4.png](img_4.png)
2. 业务层的实现类(需要使用spring的注入)
![img_5.png](img_5.png)
3. 业务层也要单元测试
![img_6.png](img_6.png)

## 控制器 =>表示层一部分(基于http访问的后端接口)
1. 创建控制器类，其中调用service
![img_7.png](img_7.png)
2. 接口测试
如果是get请求，可以直接使用浏览器: http://localhost:9876/article/1
![img_8.png](img_8.png)
如果是其他请求，则不太好使用浏览器
可以借助： PostMan（接口测试软件）/可以使用后端接口组件 Swagger2(测试+后端接口文档)


## rest风格的接口设计和测试
1. 设计规范
![img_9.png](img_9.png)
2. 设计的控制器代码:
```java
package com.etc.week4_day04_boot_ssm.controller;

import com.etc.week4_day04_boot_ssm.entity.Article;
import com.etc.week4_day04_boot_ssm.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Article控制器
 */
@RestController
public class ArticleController {

    @Autowired
    private ArticleService service;

    /**
     * rest风格演示： 查询操作 GetMapping
     *
     * @param id
     * @return
     */
    @GetMapping("article/{id}")
    public Article getById(@PathVariable("id") Long id) {
        Article article = service.selectById(id);
        return article;
    }

    /**
     * rest风格演示： 查询操作 GetMapping
     *
     * @param article
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping("article/{page}/{pagesize}")
    public List<Article> getByLike(Article article, @PathVariable(name = "page", required = false) int page, @PathVariable(name = "pagesize", required = false) int pagesize) {

        if (article == null) {
            article = new Article();
        } else {
            if (article.getTitle() != null) {
                article.setTitle("%" + article.getTitle() + "%");
            }
            // ... 省略其他属性
        }
        List<Article> articles = service.selectByLike(article, page, pagesize);
        return articles;
    }


    /**
     * rest风格演示： 增加操作 PostMapping
     *
     * @param article
     * @return
     */
    @PostMapping("article")
    public int add(@RequestBody() Article article) {

        //校验省略....
        int result = service.add(article);
        return result;
    }

    /**
     * rest风格演示： 修改操作 PutMapping
     *
     * @param article
     * @return
     */

    @PutMapping("article")
    public int update(@RequestBody() Article article) {

        //校验省略....
        int result = service.updateById(article);
        return result;
    }

    /**
     * rest风格演示： 删除 DeleteMapping
     *
     * @param id
     * @return
     */
    @DeleteMapping("article/{id}")
    public int deleteById(@PathVariable("id") Long id) {

        //校验省略....
        int result = service.deleteById(id);
        return result;
    }

}


```
3. 使用swagger2步骤(pom.xml)
```xml
<!--swagger2 start-->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>2.9.2</version>
		</dependency>

		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>2.9.2</version>
		</dependency>

		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>bootstrap</artifactId>
			<version>3.3.5</version>
		</dependency>
		<!--swagger2 end-->
```
4. 使用swagger2步骤(config/Swagger2.java)
```java
package com.etc.week4_day04_boot_ssm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger2的配置文件，这里可以配置swagger2的一些基本的内容，比如扫描的包等等
 */
@Configuration
@EnableSwagger2
public class Swagger2 {


    /**
     * 返回的是一个Docket对象
     *
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //为当前包路径
                .apis(RequestHandlerSelectors.basePackage("com.etc.week4_day04_boot_ssm"))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * 构建 api文档的详细信息函数,注意这里的注解引用的是哪个
     *
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //页面标题
                .title("Spring MVC 测试使用 Swagger2 构建RESTful API")
                //创建人
                .contact(new Contact("小白", "http://zretc.net", ""))
                //版本号
                .version("1.0")
                //描述
                .description("API 描述")
                .build();
    }


}


```
5. 使用swagger2步骤(application.properties/yml)
```properties

# 配合Swagger2使用
spring.mvc.pathmatch.matching-strategy=ant_path_matcher
```
6. 打开浏览器窗口完成测试(文档+测试):
http://localhost:9876/swagger-ui.html#/
![img_10.png](img_10.png)