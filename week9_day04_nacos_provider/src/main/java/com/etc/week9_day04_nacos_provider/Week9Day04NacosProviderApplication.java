package com.etc.week9_day04_nacos_provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Week9Day04NacosProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week9Day04NacosProviderApplication.class, args);
    }

}
