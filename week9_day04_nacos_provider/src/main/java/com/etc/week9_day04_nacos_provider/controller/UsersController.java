package com.etc.week9_day04_nacos_provider.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Slf4j
public class UsersController {

    @GetMapping("user/{userid}")
    public String getUser(@PathVariable Integer userid){
        log.info("porovder UsersController : "+userid);
        return "user "+userid+": "+ UUID.randomUUID();
    }
}
