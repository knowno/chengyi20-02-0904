package com.etc.week9_day04_nacos_provider.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@org.springframework.context.annotation.Configuration
public class BalanceConfig {
    @org.springframework.context.annotation.Bean
    public org.springframework.boot.web.server.WebServerFactoryCustomizer<org.springframework.boot.web.server.ConfigurableWebServerFactory> webServerFactoryCustomizer(){
        return new org.springframework.boot.web.server.WebServerFactoryCustomizer<org.springframework.boot.web.server.ConfigurableWebServerFactory>() {
            @Override
            public void customize(org.springframework.boot.web.server.ConfigurableWebServerFactory factory) {
                int port = org.springframework.util.SocketUtils.findAvailableTcpPort(8081, 8999);
                factory.setPort(port);
                System.getProperties().put("server.port", port);

            }
        };
    }

}
