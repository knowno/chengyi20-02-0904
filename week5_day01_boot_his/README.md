### 后端使用hisdb完成数据访问
1. 将通用application.properties文件复制过来，并修改为适合当前项目.
2. 将Pom.xml中的部分依赖复制过来.
3. 使用Database工具连接上数据库
4. 使用插件完成通用代码(MyBatisX/EasyCode)
![img.png](img.png)
![img_1.png](img_1.png)
如果提示有分页类找不到错误:
```xml
<dependency>
      <groupId>org.springframework.data</groupId>
      <artifactId>spring-data-commons</artifactId>
      <version>2.7.5</version>
      </dependency>
```

5. 添加swagger支持的配置类 : config/Swagger2.java
6. 启动项上添加@MapperScan
7. 启动项目，测试后端接口: swagger-ui.html
![img_2.png](img_2.png)
8. 使用插件生成的接口和实际的需求一定有差异，大家后续还要根据实际情况进行调整