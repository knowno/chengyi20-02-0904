package com.etc.week5_day01_boot_his;

import com.etc.week5_day01_boot_his.dao.UserinfoDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;

@SpringBootTest
class Week5Day01BootHisApplicationTests {

	@Autowired
	private UserinfoDao userinfoDao;
	@Test
	void contextLoads() {
		System.out.println(userinfoDao.queryById(8L));
	}

}
