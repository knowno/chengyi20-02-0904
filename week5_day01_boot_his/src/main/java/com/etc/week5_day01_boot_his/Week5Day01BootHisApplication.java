package com.etc.week5_day01_boot_his;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@MapperScan("com.etc.week5_day01_boot_his.dao")
@EnableAspectJAutoProxy
public class Week5Day01BootHisApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week5Day01BootHisApplication.class, args);
	}

}
