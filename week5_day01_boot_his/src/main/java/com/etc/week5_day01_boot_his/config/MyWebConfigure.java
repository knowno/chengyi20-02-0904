package com.etc.week5_day01_boot_his.config;

import com.etc.week5_day01_boot_his.intercept.MyInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyWebConfigure implements WebMvcConfigurer {

    //注入一个我们自定义的拦截器
    @Autowired
    private MyInterceptor myInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //所有/patinet/** 的路径都被拦截
        //registry.addInterceptor(myInterceptor).addPathPatterns("/patient/**");
    }
}
