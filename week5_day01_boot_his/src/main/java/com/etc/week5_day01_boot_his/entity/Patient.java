package com.etc.week5_day01_boot_his.entity;

import lombok.ToString;

import java.util.Date;
import java.io.Serializable;

/**
 * 就诊患者表(Patient)实体类
 *
 * @author makejava
 * @since 2023-10-07 09:41:03
 */
@ToString
public class Patient implements Serializable {
    private static final long serialVersionUID = -28175991664667509L;
    /**
     * 编号
     */
    private Long id;
    /**
     * 用户id
     */
    private Long userid;
    /**
     * 姓名
     */
    private String name;
    /**
     * 证件类型
     */
    private String certificatesType;
    /**
     * 证件编号
     */
    private String certificatesNo;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 出生年月
     */
    private Date birthdate;
    /**
     * 手机
     */
    private String phone;
    /**
     * 是否结婚
     */
    private Integer isMarry;
    /**
     * 省code
     */
    private String provinceCode;
    /**
     * 市code
     */
    private String cityCode;
    /**
     * 区code
     */
    private String districtCode;
    /**
     * 详情地址
     */
    private String address;
    /**
     * 联系人姓名
     */
    private String contactsName;
    /**
     * 联系人证件类型
     */
    private String contactsCertificatesType;
    /**
     * 联系人证件号
     */
    private String contactsCertificatesNo;
    /**
     * 联系人手机
     */
    private String contactsPhone;
    /**
     * 就诊卡号
     */
    private String cardNo;
    /**
     * 是否有医保
     */
    private Integer isInsure;
    /**
     * 状态（0：默认 1：已认证）
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 逻辑删除(1:已删除，0:未删除)
     */
    private Integer isDeleted;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCertificatesType() {
        return certificatesType;
    }

    public void setCertificatesType(String certificatesType) {
        this.certificatesType = certificatesType;
    }

    public String getCertificatesNo() {
        return certificatesNo;
    }

    public void setCertificatesNo(String certificatesNo) {
        this.certificatesNo = certificatesNo;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getIsMarry() {
        return isMarry;
    }

    public void setIsMarry(Integer isMarry) {
        this.isMarry = isMarry;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactsName() {
        return contactsName;
    }

    public void setContactsName(String contactsName) {
        this.contactsName = contactsName;
    }

    public String getContactsCertificatesType() {
        return contactsCertificatesType;
    }

    public void setContactsCertificatesType(String contactsCertificatesType) {
        this.contactsCertificatesType = contactsCertificatesType;
    }

    public String getContactsCertificatesNo() {
        return contactsCertificatesNo;
    }

    public void setContactsCertificatesNo(String contactsCertificatesNo) {
        this.contactsCertificatesNo = contactsCertificatesNo;
    }

    public String getContactsPhone() {
        return contactsPhone;
    }

    public void setContactsPhone(String contactsPhone) {
        this.contactsPhone = contactsPhone;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public Integer getIsInsure() {
        return isInsure;
    }

    public void setIsInsure(Integer isInsure) {
        this.isInsure = isInsure;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

}

