package com.etc.week5_day01_boot_his.controller;

import com.etc.week5_day01_boot_his.entity.Patient;
import com.etc.week5_day01_boot_his.service.PatientService;
import com.etc.week5_day01_boot_his.tools.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * 就诊患者表(Patient)表控制层
 *
 * 后端设计分页+模糊查询有一个问题:
 * 1. 如果遵循rest风格，后端取数据的时候，我们使用是get请求， 但是在现实中，我们如果想传递一个对象的话，例如partient,则不能使用@RequestBody,参数不好传递
 * 2. 不再遵循rest风格，直接使用post请求，这个时候可以直接@RequstBody ;此时折中处理。我们用post
 *
 * @author makejava
 * @since 2023-10-07 09:38:49
 */
@RestController
@RequestMapping("patient")
@CrossOrigin() //支持任何地址的访问
@Slf4j
public class PatientController {
    /**
     * 服务对象
     */
    @Resource
    private PatientService patientService;

    /**
     * 分页查询
     *
     * @param patient     筛选条件
     * @param page  页码
     * @param  pageSize 每页记录数
     * @return 查询结果
     */
//    public ResponseEntity<Page<Patient>> queryByPage(@RequestParam(required = false)Long id,String name, @RequestParam(required = false,defaultValue = "1")  Integer page, @RequestParam(required = false,defaultValue = "10") Integer pageSize) {
//    }
//    public ResponseEntity<Page<Patient>> queryByPage(@RequestBody(required = false) Patient patient, @RequestParam(required = false,defaultValue = "1")  Integer page, @RequestParam(required = false,defaultValue = "10") Integer pageSize) {
//
//    }
        @GetMapping
    public ResponseEntity<Page<Patient>> queryByPage(@RequestParam(required = false) Patient patient, @RequestParam(required = false,defaultValue = "1")  Integer page, @RequestParam(required = false,defaultValue = "10") Integer pageSize) {

        PageRequest pageRequest = null;

        if (patient == null) {
            patient = new Patient();
        }

        log.info("patinet: "+patient);
        //PageRequest参数中的page索引位置从0开始
        page--;
        pageRequest = PageRequest.of(page,pageSize);
        pageRequest = PageRequest.of(page,pageSize);
        return ResponseEntity.ok(this.patientService.queryByPage(patient, pageRequest));
    }

    /**
     * 新的分页的方法
     * @PostMapping + @RequestBody
     * @param patient
     * @param page
     * @param pageSize
     * @return
     */
    @PostMapping("page")
    public ResponseEntity<Page<Patient>> queryObjectByPage(@RequestBody(required = false) Patient patient, @RequestParam(required = false,defaultValue = "1")  Integer page, @RequestParam(required = false,defaultValue = "10") Integer pageSize) {

        log.info("patinet: "+patient);
        PageRequest pageRequest = null;
        if (patient == null) {
            patient = new Patient();
        }

        log.info("patinet: "+patient);
        //PageRequest参数中的page索引位置从0开始
        page--;
        pageRequest = PageRequest.of(page,pageSize);
        return ResponseEntity.ok(this.patientService.queryByPage(patient, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<Patient> queryById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.patientService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param patient 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<Patient> add(@RequestBody(required = false) Patient patient) {
        return ResponseEntity.ok(this.patientService.insertSelective(patient));
    }

    /**
     * 编辑数据
     *
     * @param patient 实体
     * @return 编辑结果
     */
    @PutMapping
    public CommonResponse edit(@RequestBody Patient patient) {
        log.info("edit patient :"+patient);
        int n = this.patientService.update(patient);
        if (n == 0) {
            //更新失败
            return CommonResponse.fail();
        }
        return CommonResponse.success();
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public CommonResponse deleteById(Long id) {
        boolean b = this.patientService.deleteById(id);
        if (b) {
            return CommonResponse.success();
        }
        return CommonResponse.fail();
    }

}

