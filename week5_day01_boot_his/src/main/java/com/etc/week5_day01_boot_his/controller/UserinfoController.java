package com.etc.week5_day01_boot_his.controller;

import com.etc.week5_day01_boot_his.entity.Userinfo;
import com.etc.week5_day01_boot_his.service.UserinfoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 用户表(Userinfo)表控制层
 *
 * @author makejava
 * @since 2023-10-07 11:09:04
 */
@RestController
@RequestMapping("userinfo")
public class UserinfoController {
    /**
     * 服务对象
     */
    @Resource
    private UserinfoService userinfoService;

    /**
     * 分页查询
     *
     * @param userinfo    筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<Userinfo>> queryByPage(Userinfo userinfo, PageRequest pageRequest) {
        return ResponseEntity.ok(this.userinfoService.queryByPage(userinfo, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<Userinfo> queryById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.userinfoService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param userinfo 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<Userinfo> add(Userinfo userinfo) {
        return ResponseEntity.ok(this.userinfoService.insert(userinfo));
    }

    /**
     * 编辑数据
     *
     * @param userinfo 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<Userinfo> edit(Userinfo userinfo) {
        return ResponseEntity.ok(this.userinfoService.update(userinfo));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Long id) {
        return ResponseEntity.ok(this.userinfoService.deleteById(id));
    }

}

