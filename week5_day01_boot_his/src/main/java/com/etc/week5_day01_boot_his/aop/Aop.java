package com.etc.week5_day01_boot_his.aop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * 自定义一个切面类
 */
@Aspect
@Component
public class Aop {

    /**
     * 前置通知
     * 切入位置 com.etc.week5_day01_boot_his.service 所有类的所有方法
     */
    @Before(value = "execution (* com.etc.week5_day01_boot_his.service.*.*(..))")
    public void before(){
        System.out.println("before :"+System.currentTimeMillis());
    }

    /**
     * 后置通知
     */
    @After(value = "execution (* com.etc.week5_day01_boot_his.service.*.*(..))")
    public void after(){
        System.out.println("after :"+System.currentTimeMillis());
    }
}
