package com.etc.week5_day01_boot_his.tools;


import lombok.Data;

@Data
public class CommonResponse<T> {
    private Integer code = 0;
    private String status = "success";
    private T data;

    public static CommonResponse  success(){
          return new CommonResponse();
    }

    public static CommonResponse  fail(){
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.code = -1;
        commonResponse.status = "fail";
        return commonResponse;
    }

}
