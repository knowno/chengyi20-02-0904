package com.etc.week5_day01_boot_his.service.impl;

import com.etc.week5_day01_boot_his.entity.Patient;
import com.etc.week5_day01_boot_his.dao.PatientDao;
import com.etc.week5_day01_boot_his.service.PatientService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;

/**
 * 就诊患者表(Patient)表服务实现类
 *
 * @author makejava
 * @since 2023-10-07 09:41:13
 */
@Service("patientService")
public class PatientServiceImpl implements PatientService {
    @Resource
    private PatientDao patientDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Patient queryById(Long id) {
        return this.patientDao.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param patient     筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    @Override
    public Page<Patient> queryByPage(Patient patient, PageRequest pageRequest) {
        //判断patient对象的name cardNo address如果三者不为空，需要拼接 %%
        if (patient.getName() != null) {
            patient.setName("%"+patient.getName()+"%");
        }
        if (patient.getAddress() != null) {
            patient.setAddress("%"+patient.getAddress()+"%");
        }
        if (patient.getCardNo() != null) {
            patient.setCardNo("%"+patient.getCardNo()+"%");
        }
        long total = this.patientDao.count(patient);
        return new PageImpl<>(this.patientDao.queryAllByLimit(patient, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param patient 实例对象
     * @return 实例对象
     */
    @Override
    public Patient insert(Patient patient) {
        this.patientDao.insert(patient);
        return patient;
    }

    /**
     * 新增数据
     * @param record
     * @return
     */
    @Override
    public Patient insertSelective(Patient record) {
        this.patientDao.insertSelective(record);
        return record;
    }

    /**
     * 修改数据
     *
     * @param patient 实例对象
     * @return 实例对象
     */
    @Override
    public int update(Patient patient) {
        return this.patientDao.update(patient);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.patientDao.deleteById(id) > 0;
    }
}
