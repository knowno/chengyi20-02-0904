package com.etc.week5_day01_boot_his.service;

import com.etc.week5_day01_boot_his.entity.Userinfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * 用户表(Userinfo)表服务接口
 *
 * @author makejava
 * @since 2023-10-07 11:09:07
 */
public interface UserinfoService {

    /**
     * 通过ID查询单条数据
     *
     * @param userid 主键
     * @return 实例对象
     */
    Userinfo queryById(Long userid);

    /**
     * 分页查询
     *
     * @param userinfo    筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    Page<Userinfo> queryByPage(Userinfo userinfo, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param userinfo 实例对象
     * @return 实例对象
     */
    Userinfo insert(Userinfo userinfo);

    /**
     * 修改数据
     *
     * @param userinfo 实例对象
     * @return 实例对象
     */
    Userinfo update(Userinfo userinfo);

    /**
     * 通过主键删除数据
     *
     * @param userid 主键
     * @return 是否成功
     */
    boolean deleteById(Long userid);

}
