package com.etc.week5_day01_boot_his.service;

import com.etc.week5_day01_boot_his.entity.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * 就诊患者表(Patient)表服务接口
 *
 * @author makejava
 * @since 2023-10-07 09:41:11
 */
public interface PatientService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Patient queryById(Long id);

    /**
     * 分页查询
     *
     * @param patient     筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    Page<Patient> queryByPage(Patient patient, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param patient 实例对象
     * @return 实例对象
     */
    Patient insert(Patient patient);

    /**
     * 新增数据
     * @param record
     * @return
     */
    Patient insertSelective(Patient record);

    /**
     * 修改数据
     *
     * @param patient 实例对象
     * @return 实例对象
     */
    int update(Patient patient);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
