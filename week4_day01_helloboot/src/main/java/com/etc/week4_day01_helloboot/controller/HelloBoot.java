package com.etc.week4_day01_helloboot.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloBoot {

    @GetMapping("hello")
    public String sayHello(){
        return "Hello Spring Boot!";
    }
}
