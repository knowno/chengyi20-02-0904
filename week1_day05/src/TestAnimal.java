public class TestAnimal {
    public static void main(String[] args) throws ClassNotFoundException {

        //1 看看静态代码段的执行
        Class.forName("Animal");
        //2 构造执行
        new Animal();
        Animal animal = new Animal();
        //3 静态方法
        Animal.test1();
        //4 实例方法
        animal.eat();


    }
}
