package demo;

public interface TestInterface {

    //可以写
    void t1();
    abstract  void t2();
    public abstract  void t3();

    //方法前 表示这个方法不能被重写
    //有一个方法是抽象的，然后这个方法又不能被重写
//    public final double methoda();
//
////    static void methoda(double d1) {
////
//    for(int i =1;)
////    }
//
//    protected void methoda(double d1);


}
