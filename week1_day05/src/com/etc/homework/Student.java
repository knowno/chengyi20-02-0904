package com.etc.homework;

import java.io.Serializable;

/**
 * 定义学生类，学号，姓名，年龄，身高四个属性。
 */
public class Student implements Serializable {
    private int no;
    private String name;
    private int age;
    private int height;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * 无参数
     */
    public Student() {
    }

    /**
     * 带参数的构造
     * @param no 学号
     * @param name 姓名
     * @param age 年龄
     * @param height 身高
     */
    public Student(int no, String name, int age, int height) {
        this.no = no;
        this.name = name;
        this.age = age;
        this.height = height;
    }

    @Override
    public String toString() {
        return "Student{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                '}';
    }
}
