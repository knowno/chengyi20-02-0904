package com.etc.homework;

import java.util.Arrays;

public class TestSort {
    public static void main(String[] args) {
        //创建数组 存储5个学生信息
        Student student1 = new Student(1, "小a", 20, 180);
        Student student2 = new Student(2, "小b", 22, 185);
        Student student3 = new Student(3, "小c", 23, 189);
        Student student4 = new Student(4, "小d", 28, 168);
        Student student5 = new Student(5, "小e", 18, 174);
        Student student6 = new Student(6, "小f", 19, 168);

        //存储到数组中
//        Student[] students = new Student[6];
//        students[0] = student1;
//        students[1] = student2;
//        students[2] = student3;
//        students[3] = student3;
//        students[4] = student4;
//        students[5] = student6;

        //
        Student[] students = {student1, student2, student3, student4, student5, student6};

        sortByAge(students);
        System.out.println(Arrays.toString(students));

        System.out.println("*********************************");
        sortByHeight(students);
        System.out.println(Arrays.toString(students));

    }

    /**
     *  按照年龄排序
     * @param students
     */

    public static void sortByAge(Student[] students) {
        // 冒泡排序
        for (int i = 0; i < students.length - 1; i++) {
            for (int j = 0; j < students.length - 1 - i; j++) {
                // 按照年龄
                if (students[j].getAge() > students[j + 1].getAge()) {
                    Student temp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = temp;
                }
            }
        }

    }

    /**
     * 按照身高排序
     * @param students
     */
    public static void sortByHeight(Student[] students) {
        // 冒泡排序
        for (int i = 0; i < students.length - 1; i++) {
            for (int j = 0; j < students.length - 1 - i; j++) {
                // 按照身高排序
                if (students[j].getHeight() > students[j + 1].getHeight()) {
                    Student temp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = temp;
                }
            }
        }

    }

}
