public class Test2DArray {

    public static void main(String[] args) {

        //二维数组
//        int [][] arr = new int[行长度][列长度];

        //如果是引用
//        arr[行索引][列索引]

        int[][] arr = new int[6][6];


        //使用for循环来赋值
        //i表示的是行
        for (int i = 0; i < 6; i++) {

            //j表示的的是列
            for (int j = 0; j <= i; j++) {

                //判断 j = =0 || i == j
                if (j == 0 || i == j) {
                    arr[i][j] = 1;
                } else {
                    arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
                }

                //打印输出一下
                System.out.print(arr[i][j] + " ");
            }
            //只是换行
            System.out.println();


        }
    }

}
