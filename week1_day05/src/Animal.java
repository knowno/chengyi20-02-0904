/**
 * 动物类
 *   肉食性动物/草食性动物/...
 *    老虎/狮子   小白兔/牛 ...
 *
 *    类名:Animal 要求每个单词的首字母要大写
 */
public class Animal {

    //1 类中属性(成员变量)
    String type;

    //静态属性，类所有，所有对象共享
    static int age;

    //2 方法(行为) 函数 =》实例(对象)方法
    public void eat(){
        System.out.println("开吃...");
    }

    //3 方法 =>构造方法(构造器)
    //方法名和类名相同
    //无返回类型
    public  Animal() {
        System.out.println("无参的构造方法");
    }

    //4 静态方法: 属于类所有，所有对象共享
    static  void test1(){

        System.out.println("test1是一个静态方法");
    }

//    private  Animal(){}

    //5 类内部可以定义类,内部类
    class InnerClass{

    }

    //6 实例代码段
    {
        System.out.println("实例代码段,在构造对象的时候执行，构造一次执行一次");
    }

    //7 静态代码段
    static {
        System.out.println("静态代码段，只执行一次，在类加载的好时候执行");
    }
}

