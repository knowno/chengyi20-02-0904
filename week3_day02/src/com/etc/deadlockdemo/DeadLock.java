package com.etc.deadlockdemo;

public class DeadLock {

    public static Object object1 = new Object();
    public static Object object2 = new Object();

    public static void main(String[] args) {

        //尝试去运行Thread1 和Thread2

        new Thead1().start();
        new Thead2().start();

    }

}

class Thead1 extends Thread {

    @Override
    public void run() {
        //获取资源 DeadLock.object1,不放手(线程同步)
        synchronized (DeadLock.object1) {
            System.out.println(Thread.currentThread().getName()+" .....DeadLock.object1");

            try {
                Thread.sleep((long) (Math.random() * 2000));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            //没释放object1,我还想去获取 DeadLock.object2
            synchronized (DeadLock.object2) {
                System.out.println(Thread.currentThread().getName() +".......DeadLock.object2");
            }

        }
    }
}


class Thead2 extends Thread {

    @Override
    public void run() {
        //获取资源 DeadLock.object2,不放手(线程同步)
        synchronized (DeadLock.object2) {
            System.out.println(Thread.currentThread().getName()+" ......DeadLock.object2");
            try {
                Thread.sleep((long) (Math.random() * 2000));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            //没释放object2,我还想去获取 DeadLock.object1
            synchronized (DeadLock.object1) {
                System.out.println(Thread.currentThread().getName()+" .......DeadLock.object1");

            }

        }
    }
}