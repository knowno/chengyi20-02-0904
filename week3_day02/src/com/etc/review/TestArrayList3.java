package com.etc.review;

import org.apache.commons.beanutils.BeanUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ArrayList是否线程安全？ 不安全
 * 我们发现，实际操作过程中，对应的集合长度无法达到50000
 * 所以我们说ArrayList存在线程安全问题(非线程安全)
 * <p>
 * synchronized
 */
public class TestArrayList3 {

    ReentrantLock lock = new ReentrantLock();

    public void locktest() throws InterruptedException {
        List<Object> objects = new ArrayList<>();
        //循环
        for (int i = 1; i <= 50000; i++) {
            //创建线程对象 ->每循环一次，将这个线程对象name添加到list中
            new Thread(() -> {
                lock.lock();//锁==开始
                objects.add(Thread.currentThread().getName());
                lock.unlock();//释放 --结束
            }).start();

        }
        //加一个延迟
        Thread.sleep(10000);

        System.out.println(objects.size());
    }


    public static void main(String[] args) throws InterruptedException {
        new TestArrayList3().locktest();


    }

}
