package com.etc.reflectdemo;

/**
 * 获取Class类型对象的方法
 */
public class TestClasss {
    public static void main(String[] args) throws ClassNotFoundException {

        //1 对象名.getClass
        String str  = "string";
        Class<? extends String> aClass = str.getClass();

        //2 这个是Class对象
        Class<?> superclass = aClass.getSuperclass();

        //3 Class.forName
        Class<?> aClass1 = Class.forName("java.lang.String");

        //3. 类型名.class
        Class<String> stringClass = String.class;

    }
}
