package com.etc.reflectdemo;

/**
 * 自定类
 */
public class Person {
    //私有属性
    private int id;
    //默认属性
    String name;
    //公共属性
    public int age;
    //受保护
    protected String tel;


    private void test1() {
        System.out.println("test1...");
    }

    public static void test2() {
        System.out.println("test2...");
    }

    void setAge(int age) {
        this.age = age;
        System.out.println("setAge...");
    }



    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", tel='" + tel + '\'' +
                '}';
    }

    public Person(int id, String name, int age, String tel) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    public Person() {
    }

    private Person(Integer id) {
        this.id = id;
    }
}
