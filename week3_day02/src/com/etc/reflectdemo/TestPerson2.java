package com.etc.reflectdemo;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestPerson2 {
    public static void main(String[] args) throws NoSuchFieldException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

        //得到Person.class
        Class<Person> personClass = Person.class;

        // 1 getDeclaredMethods =>Method[] declaredMethods
        //实例方法 ，静态方法，私有，公共，默认等,都是Person类内部的方法
        System.out.println("*************getDeclaredFields*************");
        Method[] declaredMethods = personClass.getDeclaredMethods();

        for (Method m : declaredMethods) {
            //我们Method类
            System.out.println(m.getName() + " " + m.getModifiers());
        }

        System.out.println("*************getMethods*************");
        //Person类中的公共方法，还有从父类继承而来的方法.
        Method[] methods = personClass.getMethods();

        for (Method m : methods) {
            //Method
            System.out.println(m.getName() + ": " + m.getModifiers());
        }

        System.out.println("*************getDeclaredMethod,调用invoke方法*************");
        //可以尝试给某个属性赋值
        Method test1 = personClass.getDeclaredMethod("test1");
        //需要创建Person类型的对象
        Person person1 = personClass.getConstructor().newInstance();
        test1.setAccessible(true);
        //调用invoke方法
        Object invoke = test1.invoke(person1);

    }
}
