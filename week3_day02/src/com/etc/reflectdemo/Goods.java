package com.etc.reflectdemo;

import java.math.BigDecimal;

public class Goods {
    private Integer goodsid;
    private String goodsname;
    private BigDecimal goodsprice;
    private String goodsinfo;
    private Integer goodscount;
    private String cover;
    private Integer typeid;
    private Integer shopid;
    private Integer goodsstate;

    @Override
    public String toString() {
        return "Goods{" +
                "goodsid=" + goodsid +
                ", goodsname='" + goodsname + '\'' +
                ", goodsprice=" + goodsprice +
                ", goodsinfo='" + goodsinfo + '\'' +
                ", goodscount=" + goodscount +
                ", cover='" + cover + '\'' +
                ", typeid=" + typeid +
                ", shopid=" + shopid +
                ", goodsstate=" + goodsstate +
                '}';
    }

    public Integer getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(Integer goodsid) {
        this.goodsid = goodsid;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public BigDecimal getGoodsprice() {
        return goodsprice;
    }

    public void setGoodsprice(BigDecimal goodsprice) {
        this.goodsprice = goodsprice;
    }

    public String getGoodsinfo() {
        return goodsinfo;
    }

    public void setGoodsinfo(String goodsinfo) {
        this.goodsinfo = goodsinfo;
    }

    public Integer getGoodscount() {
        return goodscount;
    }

    public void setGoodscount(Integer goodscount) {
        this.goodscount = goodscount;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getShopid() {
        return shopid;
    }

    public void setShopid(Integer shopid) {
        this.shopid = shopid;
    }

    public Integer getGoodsstate() {
        return goodsstate;
    }

    public void setGoodsstate(Integer goodsstate) {
        this.goodsstate = goodsstate;
    }
}
