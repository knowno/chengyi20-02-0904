package com.etc.threadpooldemo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestPool {
    public static void main(String[] args) {

        //使用Executors类的静态方法来创建对象
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        //executorService 调用方法
        for (int i = 1; i <= 5; i++) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName());
                }
            };
            //提交r对象给executorService=>就会自动调用start方法启动线程
            //这里省略了 new Thread(r) 以及线程的start方法调用
            executorService.submit(r);

        }

        executorService.shutdown();


    }
}
