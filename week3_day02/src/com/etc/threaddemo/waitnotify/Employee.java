package com.etc.threaddemo.waitnotify;

/**
 * 工人(员工)
 */
public class Employee {

    // 假设目前商品的库存为-1,就是没有库存
    private int productCount = -1;


    /**
     * 生产商品信息(进货)
     *
     * @param productCount
     */
    public synchronized void setProduct(int productCount) {
        //已经生产完了
        if (this.productCount != -1) {
            //不再生产，而是等待
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }
        // 不是直接做生产操作
        this.productCount = productCount;
        //
        System.out.println("生产者,生产setProduct完成 " + this.productCount);
        //到这里，我们生产完了,唤醒那个消费者线程
        notify();
    }


    /**
     * 获取商品信息(买)
     *
     * @return
     */
    public synchronized int getProduct() {
        //线程通信,我们这里购买业务，购买的时候我们先判断库存.
        if (this.productCount == -1) {
            //该方法后面的代码不执行了，而是等待生产者生产,生产者生产完了，必须要等唤醒 。
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        // 消费完了
        int n = this.productCount;
        System.out.println("消费者,购买 getProduct:" + n);
        this.productCount = -1;
        //消费完成了,去唤醒生产者
        notify();

        return n;
    }

}
