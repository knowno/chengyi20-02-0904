package com.etc.threaddemo.waitnotify;

public class TestMain {

    public static void main(String[] args) {

        //创建一个Employee对象
        Employee employee = new Employee();
        //开启两个线程分别启动消费者和生产者的线程对象
        Consumer consumer = new Consumer(employee);
        //启动消费者线程
        new Thread(consumer).start();

        Producer producer = new Producer(employee);
        //启动生产者线程
        new Thread(producer).start();

    }

}
