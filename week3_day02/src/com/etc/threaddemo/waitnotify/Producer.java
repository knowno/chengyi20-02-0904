package com.etc.threaddemo.waitnotify;

import java.util.Random;

/**
 * 生产者
 */
public class Producer implements Runnable {

	private Employee emp;

	public Producer(Employee emp) {
		super();
		this.emp = emp;
	}

	@Override
	public void run() {
		System.out.println("producer..........");
		for (int i = 1; i <= 10; i++) {
			// 随机休眠一段时间
			try {
				Thread.sleep(new Random().nextInt(2000));

				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//调用员工的生成商品的方法
			emp.setProduct(i);

		}

	}

}
