
## 回顾
###  线程创建方式
1. 类继承Thread类，重写run方法，运行调用start方法
2. 实现Runnable接口,重写run方法，需要将该对象作为Thread的参数，运行调用start方法.
3. 实现Callable接口，重写call方法，这个实现方式需要有返回值.

### 线程生命周期

![img.png](img.png)

### 线程同步
以ArrayList为例，对于ArrayList来说，对象是非线程同步。
我们尝试在for循环50000次情况下，对ArrayList进行填充(add).每循环一次
开启一个线程来执行add方法;
![img_1.png](img_1.png)
解决方案如下：
1. 加synchronized关键字来同步代码段
![img_2.png](img_2.png)
2. Collections来讲，有一个静态方法组 synchronizedxx
![img_3.png](img_3.png)
3. 将ArrayList替换掉(CopyOnWriteArrayList<E>)
4. 可以使用Lock对象.

![img_4.png](img_4.png)


