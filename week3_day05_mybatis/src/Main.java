import com.etc.dao.BlogMapper;
import com.etc.entity.Blog;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class Main {
    public static void main(String[] args) throws IOException {
        String resource = "mybatis-config.xml";

        InputStream inputStream = Resources.getResourceAsStream(resource);
        //SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // SqlSession
        try (SqlSession session = sqlSessionFactory.openSession()) {
            BlogMapper mapper = session.getMapper(BlogMapper.class);
            //这个查询条件和下面查询条件完全一致
            Blog blog = mapper.selectBlog(3);
            System.out.println(blog);
            System.out.println("***********************************");
            //上面查询有sql语句，后面么有出现，其实就是第二个查询没有向数据库发送请求
            Blog blog2 = mapper.selectBlog(3);
            System.out.println(blog2);
        }

    }
}