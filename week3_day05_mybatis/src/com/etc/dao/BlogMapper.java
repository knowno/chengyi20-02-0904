package com.etc.dao;

import com.etc.entity.Blog;

public interface BlogMapper {

    public Blog selectBlog(Integer id);
}
