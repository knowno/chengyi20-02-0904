package com.etc;

import java.util.ArrayList;

/**
 * Exception:
 * java.lang.ArithmeticException
 */
public class TestTryCatchFinally {
    public static void main(String[] args) {


        int a = 1;
        String str = null;
        try {
            System.out.println(1 / 1);
            System.out.println("这里还有逻辑代码段1..................");
            System.out.println(str.length());
            System.out.println("这里还有逻辑代码段2..................");
        } catch (ArithmeticException | NullPointerException ex) {
            //处理异常的代码
            //1. 打印异常信息(日志记录 文件/数据库)
            System.out.println(ex.getMessage());
            //2. 直接给出文字提示
//            System.out.println("发生了数学异常，请检查！");
            //3 打印异常发生 栈轨迹信息
//            ex.printStackTrace();
        } catch (Exception e) {

        } finally {
            //无论是否发生异常都会去执行的代码段
            //最后都要释放资源(关闭流)/数据库连接关闭
            System.out.println("finally");
        }


        //ArithmeticException =>RuntimeException =>Exception =>Throwable
        System.out.println("****后续代码***");


    }
}
