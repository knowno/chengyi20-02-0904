package com.etc;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Exception:
 * java: 未报告的异常错误java.io.FileNotFoundException; 必须对其进行捕获或声明以便抛出
 *
 * dao: throws=>service(要根据异常信息做业务处理的 try)
 */
public class TestThrows {
    // JUM在调用main,再向上就到JUM
    public static void main(String[] args) throws FileNotFoundException {

        //检查异常
        //1 使用try-catch主动处理
        //2 使用throws来处理
        FileReader f = new FileReader("d:\\abc.txt");

        System.out.println("打印输出");
    }

}
