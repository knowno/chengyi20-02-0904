package com.etc;

/**
 * 如果一个对象为null
 * 调用这个对象的属性或者方法就会异常
 */
public class TestNullPointerException {
    public static void main(String[] args) {
        Object obj = null;

        System.out.println(obj.hashCode());
    }
}
