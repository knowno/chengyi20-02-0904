package com.etc;

import java.sql.Array;
import java.util.ArrayList;

/**
 * Exception:
 * java.lang.ArithmeticException
 */
public class TestThrow {
    public static void main(String[] args) {

        Student student = new Student();
        student.setId(1);
    }

}


class Student {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        //别赋值给负数
        if (id < 0) {
            throw new RuntimeException("id不能小于0");
        }
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}