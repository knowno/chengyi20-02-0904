package com.etc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

/**
 * FileNotFoundException 文件未找到
 */
public class TestFileNotFoundException {
    public static void main(String[] args) throws FileNotFoundException {

        //检查时异常
        Reader reader = new FileReader("d:\\fewfew.txt");
        BufferedReader bufferedReader = new BufferedReader(reader);

    }
}
