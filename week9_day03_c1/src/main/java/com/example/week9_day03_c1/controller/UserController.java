package com.example.week9_day03_c1.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Slf4j
public class UserController {

    @GetMapping("user/{userid}")
    public String getUser(@PathVariable Integer userid){
        log.info("c1  getUser");
        return "user:"+userid+"@"+UUID.randomUUID();
    }
}
