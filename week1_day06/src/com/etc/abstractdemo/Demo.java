package com.etc.abstractdemo;

/**
 * 使用abstract修饰的类称为抽象类
 * 没有显式的声明其父类，默认继承了Object
 */
public abstract class Demo {

    //成员？
    // 私有属性
    private int a;
    //实例方法
    public void test1(){
        //普通方法
    }
    //有构造方法
    public Demo() {
    }
    //静态代码段
    static {

    }
    //实例代码段
    {

    }

    //抽象类里 还可以包含抽象方法 使用abstract修饰的方法
    //没有方法体
    public abstract void test3();

    abstract void test4();
}

/**
 * 如果是普通类，就需要抽象类的所有抽象方法
 */
class Demo3 extends  Demo{
    @Override
    public void test3() {
        System.out.println("test3");
    }

    @Override
    void test4() {
        System.out.println("test4");

    }
}
/**
 * 抽象类可以继承抽象类
 */
abstract  class Demo2 extends  Demo{

}
