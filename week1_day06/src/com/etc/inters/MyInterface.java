package com.etc.inters;

/**
 * 关键字interface
 */
public interface MyInterface {

    // 1 这里都是公共，静态常量
    public final static int a = 10;

    //公共 静态 常量
    int b = 12;

    //2 可以有默认方法 使用default来修饰
    public default void t1() {
        System.out.println("default方法");
    }

    //3 抽象方法
    void t2();

    abstract void t3();

    //4 静态方法
    static void t4() {
        System.out.println("静态方法");
    }

    //5 私有的方法
    private void t5() {
        System.out.println("default方法");
    }

}

interface  IA {

}
interface  IB{

}
//接口继承多个接口
interface  IC extends  IA,IB,MyInterface{

}
//一个类可以继承一个父类同时  实现多个接口
class  TestClass extends  Object  implements IA,IB,MyInterface{
    @Override
    public void t2() {
        System.out.println("t2");
    }

    @Override
    public void t3() {
        System.out.println("t2");

    }
}
