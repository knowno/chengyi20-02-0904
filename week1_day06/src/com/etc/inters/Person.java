package com.etc.inters;

public interface Person {
    void say();
}

class  American implements Person{
    @Override
    public void say() {
        System.out.println("哈喽呀");
    }
}

class  Chinese implements  Person{
    @Override
    public void say() {
        System.out.println("吃了没");
    }
}

class Japan implements  Person{
    @Override
    public void say() {
        System.out.println("扣你夕哇");
    }
}
class  TestPerson{
    //父接口作为参数，实际调用传递的是子类对象，多态
    public static void intro(Person p){
        p.say();
    }

    public static void main(String[] args) {
        American american = new American();
        Chinese chinese = new Chinese();
        Japan japan = new Japan();
        intro(american);
        intro(japan);
        intro(chinese);
    }
}
