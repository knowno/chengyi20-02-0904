package com.etc.exer;

/**
 * (1)定义父类 MyShape
 * (2)定义方法public double getArea(){};
 * (3)定义方法public double getLen(){};
 */
public class MyShape {
    //面积
    public double getArea() {
        return 0;
    }

    //周长
    public double getLen() {
        return 0;
    }
}

/**
 * 定义Rect类继承MyShape
 * (1)定义长和宽成员变量，double width height;
 * (2)无参构造，有参构造。
 * (3)实现父类方法。
 */
class Rect extends MyShape {

    public double width;
    public double height;

    @Override
    public double getArea() {
        return width * height;
    }

    @Override
    public double getLen() {
        return 2 * (width + height);
    }
}

/**
 * 定义Cricle类继承MyShape
 * (1)定义半径成员变量，和PI常量
 * (2)无参构造，有参构造
 * (3)实现父类方法。
 */
class Circle extends MyShape {
    public double r;

    @Override
    public double getArea() {
        return Math.PI * r * r;
    }

    @Override
    public double getLen() {
        return Math.PI * r * 2;
    }
}

class Calc {
    /**
     * 参数是父类引用
     * @param myShape
     */
    public static void calc(MyShape myShape) {
        double area = myShape.getArea();
        double len = myShape.getLen();
        System.out.println("面积是: " + area);
        System.out.println("周长是: " + len);
    }

    public static void main(String[] args) {

        //圆形面积计算并输出
        Circle myShape = new Circle();
        myShape.r = 1.23;
        //子类对象为 Circl对象
        calc(myShape);
        System.out.println("********************");

        Rect rect = new Rect();
        rect.width = 5;
        rect.height = 10;
        //子类对象为Rect对象
        calc(rect);

    }
}

