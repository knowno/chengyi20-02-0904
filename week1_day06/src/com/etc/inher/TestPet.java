package com.etc.inher;

/**
 * 创建子类对象的时候，是否会调用父类构造？ 一定会调用；先调用父类构造.
 */
public class TestPet {
    public static void main(String[] args) {

        Dog dog = new Dog();
        dog.setName("旺财");
        dog.setHeath(100);
        System.out.println(dog);

        System.out.println("******************************");

        Penguin penguin = new Penguin("腾讯", 100, "男");
        System.out.println(penguin);

        System.out.println("******************************");

        //调用方法 传递的是子类对象
        Zoo.show(dog);
        Zoo.show(penguin);


    }
}
