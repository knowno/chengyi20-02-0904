package com.etc.inher;

public class TestBird {
    public static void main(String[] args) {

        Ostrich o = new Ostrich();
        //如果子类重写父类的方法，那么调用的就是子类的方法
        o.fly();

    }
}
