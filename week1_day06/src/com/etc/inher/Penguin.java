package com.etc.inher;

/**
 *  Penguin=>Pet=>Object
 */
public class Penguin extends  Pet{
    private String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 这里 带参数构造，同时调用了父类的构造方法
     * @param name
     * @param heath
     * @param sex
     */
    public Penguin(String name, int heath, String sex) {
        // super() 调用父类构造方法,只能放在子类构造第一行代码
        super(name, heath);
        this.sex = sex;
        System.out.println("Penguin 子类的带参数的构造");
    }

    @Override
    public String toString() {
        return "Penguin{" +"name='" + super.getName() +
                "' health='" + super.getHeath() +
                "' sex='" + sex + '\'' +
                '}';
    }

    @Override
    public void perform() {
        System.out.println(getName()+" 表演水上漂...");
    }
}
