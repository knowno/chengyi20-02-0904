package com.etc.inher;

/**
 * 父类
 */
public class Pet {
    private String name;
    private int heath;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeath() {
        return heath;
    }

    public void setHeath(int heath) {
        this.heath = heath;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "name='" + name + '\'' +
                ", heath=" + heath +
                '}';
    }

    public Pet(String name, int heath) {
        this.name = name;
        this.heath = heath;
        System.out.println("父类带参数的构造");
    }

    public Pet() {
        System.out.println("父类无参数构造");
    }

    //1 增加一个表演的方法
    public void perform(){
        System.out.println("开始你的表演...");
    }
}

