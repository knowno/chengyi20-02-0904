package com.etc.inher;

public class TestSubClass {
    public static void main(String[] args) {

        //bird
        // 父类 引用  = 子类对象
        // 子类对象 赋值给父类 类型
        // 向上转型
        Bird bird = new Ostrich();


        //反之
        // 强制转换，大家要记得，右侧如果原始类型不是ostrich那么一定会异常
        //java.lang.ClassCastException 类型转换
        Ostrich os = (Ostrich) new Bird();

    }
}
