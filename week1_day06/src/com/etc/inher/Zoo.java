package com.etc.inher;

public class Zoo {


    /**
     * 多态思想
     * 参数为父类引用
     * @param pet
     */
    public static  void show(Pet pet){
        pet.perform();
    }
}
