package com.etc.inher;

/**
 * 同名
 * 同参
 * 同返回值
 * 访问权限不能缩小
 */
public class Bird {

    public void fly(){
        System.out.println("bird的fly方法");
    }
}

/**
 * 鸵鸟
 */
class Ostrich extends Bird{

    @Override
    public void fly() {
        System.out.println("Ostrich的fly方法,我不会飞...");
    }
}
