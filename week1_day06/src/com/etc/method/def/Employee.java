package com.etc.method.def;

/**
 * 实体类 要封装
 */
public class Employee {

    //1 属性私有化
    private int empid;
    private String empname;

    //2 提供公共的方法来访问私有的属性

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }
}

