package com.etc.method.def;

/**
 * 如果不做声明，在一个类中有一个无参数的构造方法，访问修饰符和类访问修饰符是一致。
 * 如果类中加入了一个带参数的构造，显式声明，默认无参数构造将会被覆盖.
 */
 class Person {
    int id;
    String name;

    //带参数构造方法
    public Person(int id, String name) {
        //this => 1 代指当前类对象 2 调用构造方法 this()
        this();
        this.id = id;
        this.name = name;
        System.out.println("带参数的构造");
    }

    //无参数构造
    public Person() {
        System.out.println("无参数的构造");
    }
}
