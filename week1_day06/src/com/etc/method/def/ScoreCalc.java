package com.etc.method.def;

public class ScoreCalc {

    /**
     * 总成绩
     *
     * @param s1
     * @param s2
     * @param s3
     * @return
     */
    public double getSum(double s1, double s2, double s3) {
        return s1 + s2 + s3;
    }

    /**
     * @param s1
     * @return
     */
    public double getSum2(double[] s1) {
        double sum = 0;
        for (int i = 0; i < s1.length; i++) {
            sum += s1[i];
        }
        return sum;
    }

    /**
     * 可变参数,这里参数为可变；那就意味着我们调用的时候可以是数组，也可以是值列表
     * @param s1
     * @return
     */
    public double getSum3(double... s1) {
        double sum = 0;
        for (int i = 0; i < s1.length; i++) {
            sum += s1[i];
        }
        return sum;
    }

    /**
     * 平均成绩
     *
     * @param s1
     * @param s2
     * @param s3
     * @return
     */
    public double getAvg(double s1, double s2, double s3) {
        return (s1 + s2 + s3) / 3;
    }

    /**
     * 求平均成绩
     * @param s1
     * @return
     */
    public double getAvg2(double[] s1) {
        //调用求和的方法
        return getSum2(s1) / 3;
    }

    public double getAvg3(double... s1) {
        //调用求和的方法
        return getSum2(s1) / 3;
    }
}
