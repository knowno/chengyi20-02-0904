package com.etc.method.def;

public class TestMethod2 {

    public static void main(String[] args) {

        int sum = getSum(100);
        System.out.println(sum);

        int sum2 = getSum2(7);
        System.out.println(sum2);

    }

    /**
     * getSum函数中调用自己
     * 这种调用称为递归调用
     * @param n
     * @return
     */
    public static int getSum(int n) { //1 2  3
        if (n == 1) {
            return 1;
        }
        return getSum(n - 1) + n; // 1+2   getSum(2)+3 =>getSum(1)+2 +3 => 1+2+3

    }

    /**
     * 阶乘
     * @param n
     * @return
     */
    public static int getSum2(int n) { //1 2  3
        if (n == 1) {
            return 1;
        }
        return getSum2(n - 1) * n; //

    }
    //IO 遍历文件夹下子文件夹和文件 =>等讲到了io的时候大家注意下

}
