package com.etc.method.def;

/**
 * 在一个类中:
 * 两个方法之间方法名相同，参数项不同()
 * 方法名的复用,提高程序的可读性
 * 1. 两个方法参数个数不同
 *  2. 参数个数相同，类型不同
 *  3. 参数个数相同，顺序不同
 *
 *
 *  我们见过的重载
 *
 *  构造方法可以重载吗？ 可以重载
 *
 */
public class TestOverload {

    //无参数
    public void  test1(){

    }

    //有一个参数
    public void  test1(int n){

    }

    //有一个参数，类型不同
    public void  test1(String n){

    }

    //2 个参数，顺序String,int
    public void  test1(String n,int b){

    }

    //2 个参数，顺序int,String
    public void  test1(int b,String n){

    }
}
