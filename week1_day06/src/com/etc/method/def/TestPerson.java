package com.etc.method.def;

/**
 * 如果不做声明，在一个类中有一个无参数的构造方法，访问修饰符和类访问修饰符是一致。
 * 如果类中加入了一个带参数的构造，显式声明，默认无参数构造将会被覆盖.
 */
 class TestPerson {
    public static void main(String[] args) {

       // Person person = new Person();

        Person person1 = new Person(1,"A");
        System.out.println(person1);

    }


}
