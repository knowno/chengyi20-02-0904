package com.etc.week9_day01_exam.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Data
@ToString
@ConfigurationProperties(prefix = "boy")
//@PropertySource(name="文件名")
@Component
public class Boy {
    //给属性赋值: new .. setXX
    //通过从配置文件中读取属性的值进来
    private int id;
    private String name;

    @Value("20")
    private int age;

    @Autowired
    private Girl girl;
}


@Component
class  Girl{

    public void test(){
        //1
        //2
        //3
        //4
    }
}