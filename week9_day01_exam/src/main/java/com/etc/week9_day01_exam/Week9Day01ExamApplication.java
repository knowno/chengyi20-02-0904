package com.etc.week9_day01_exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week9Day01ExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week9Day01ExamApplication.class, args);
    }

}
