package com.etc.week9_day01_exam;

import com.etc.week9_day01_exam.entity.Boy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Week9Day01ExamApplicationTests {

    @Autowired
    private Boy boy;
    @Test
    void contextLoads() {
        System.out.println(boy);
    }

}
