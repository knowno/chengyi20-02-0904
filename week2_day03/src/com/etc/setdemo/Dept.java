package com.etc.setdemo;

/**
 * 部门类
 */
public class Dept implements Comparable<Dept> {

    @Override
    public int compareTo(Dept dept) {
        //当前对象 no-传递过来参数.no  升序排列
        return this.getDeptno() - dept.getDeptno();
    }

    private int deptno;
    private String deptname;
    private String deptremark;


    @Override
    public String toString() {
        return "Dept{" +
                "deptno=" + deptno +
                ", deptname='" + deptname + '\'' +
                ", deptremark='" + deptremark + '\'' +
                '}';
    }

    public Dept() {
    }

    public Dept(int deptno, String deptname, String deptremark) {
        this.deptno = deptno;
        this.deptname = deptname;
        this.deptremark = deptremark;
    }

    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getDeptremark() {
        return deptremark;
    }

    public void setDeptremark(String deptremark) {
        this.deptremark = deptremark;
    }
}
