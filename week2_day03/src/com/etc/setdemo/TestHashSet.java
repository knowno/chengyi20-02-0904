package com.etc.setdemo;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;

public class TestHashSet {
    public static void main(String[] args) {

        //创建对象
        HashSet set = new HashSet();
        //1 增加
        set.add("java");
        set.add(LocalDate.now());
        set.add("小白");
        set.add(1);
        set.add(null);
        System.out.println("list.size: " + set.size());
        //2 删除
        set.remove(0);
        System.out.println("list.size: " + set.size());
        //3 输出
        set.forEach(System.out::println);
        System.out.println("******************************");

        set.forEach(System.out::println);

        // 5 替换操作=>set(index,object)
        //迭代器
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof String) {
                if (o.equals("小白")) {
                    //删除小白
                    set.remove("小白");
                    //加入小青
                    set.add("小青");
                }
            }
        }

        System.out.println("******************************");

        set.forEach(System.out::println);


    }
}
