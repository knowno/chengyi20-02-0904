package com.etc.setdemo;

import java.util.TreeSet;

public class TestTreeSet {
    public static void main(String[] args) {

        TreeSet<Object> treeSet = new TreeSet<>();

        Dept dept1 = new Dept(1,"开发部","开发部门");
        Dept dept2 = new Dept(2,"行政部","行政部门");
        treeSet.add(dept1);
        treeSet.add(dept2);
        //class com.etc.setdemo.Dept cannot be cast to class java.lang.Comparable
        //我们去实现这个接口

        System.out.println(treeSet);



    }
}
