package com.etc.setdemo;


import java.time.LocalDate;
import java.util.HashSet;

public class TestEmployee3 {
    public static void main(String[] args) {


        Dept dept1 = new Dept(1,"开发部","开发部门");
        Dept dept2 = new Dept(2,"行政部","行政部门");
        Employee employee1 = new Employee("202301","张三", LocalDate.of(2000,12,12),LocalDate.now(),"13612345678",dept1,5234);
        Employee employee2 = new Employee("202302","张4", LocalDate.of(2001,12,12),LocalDate.now(),"13612345678",dept2,5235);
        Employee employee3 = new Employee("202303","张5", LocalDate.of(2002,12,12),LocalDate.now(),"13612345678",dept1,5236);
        Employee employee4 = new Employee("202304","张6", LocalDate.of(2003,12,12),LocalDate.now(),"13612345678",dept2,5237);

        Employee employee5 = new Employee("202304","张6", LocalDate.of(2003,12,12),LocalDate.now(),"13612345678",dept2,5237);

        //将这些员工添加到HashSet中来
        HashSet<Employee> set = new HashSet<>();

        set.add(employee1);
        set.add(employee2);
        set.add(employee3);
        set.add(employee4);
        set.add(employee5);

        //我的需求：如果两个员工所有信息都一样了，我就认为员工记录重复的. ==>我的目标...
        System.out.println(employee4.hashCode());
        System.out.println(employee5.hashCode());

        System.out.println(set.size());

    }
}
