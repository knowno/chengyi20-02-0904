package com.etc.setdemo;

import java.util.HashSet;

public class TestHashSetCapi {
    public static void main(String[] args) {

        // map = new HashMap<>(); 底层就是一个Hashmap
        // capacity (16) and load factor (0.75).
        HashSet<Object> objects = new HashSet<>();

        for (int i = 1; i < 20; i++) {
            // map.put(e, PRESENT)
            objects.add(i);
        }

    }
}
