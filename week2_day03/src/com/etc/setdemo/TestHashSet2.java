package com.etc.setdemo;

import java.util.HashSet;

public class TestHashSet2 {
    public static void main(String[] args) {

        //创建对象
        HashSet set = new HashSet();
        //1 增加
        String str1 = "java";
        String str2 = "java";
        System.out.println(str1.hashCode());
        System.out.println(str2.hashCode());
        System.out.println(str1.equals(str2));
        //这里添加操作，先判断hashcode,hashcode相同的情况下，再判断equals。
        set.add(str1);
        set.add(str2);
//
//        set.add(LocalDate.now());
//        set.add(LocalDate.now());

        System.out.println("list.size: " + set.size());


        set.forEach(System.out::println);


    }
}
