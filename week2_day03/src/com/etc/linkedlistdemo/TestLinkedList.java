package com.etc.linkedlistdemo;

import java.time.LocalDate;
import java.util.LinkedList;

public class TestLinkedList {
    public static void main(String[] args) {

        //创建对象
        LinkedList linkedList = new LinkedList();
        //1 增加
        linkedList.add("java");
        linkedList.add(LocalDate.now());
        linkedList.add("小白");
        linkedList.add(1);
        linkedList.add(null);
        System.out.println("list.size: " + linkedList.size());
        //2 删除
        linkedList.remove(0);
        System.out.println("list.size: " + linkedList.size());
        //3 输出
        linkedList.forEach(System.out::println);
        // 4 ->使用add方法insert 插入
        linkedList.add(1, "小黑");
        System.out.println("******************************");

        linkedList.forEach(System.out::println);

        // 5 替换操作=>set(index,object)
        for (int i = 0; i < linkedList.size(); i++) {
            //查找是否包含某某xx
            Object o = linkedList.get(i);
            if (o instanceof String) {
                if (o.equals("小白")) {
                    //将其修改为小黄
                    linkedList.set(i,"小黄");
                }
            }

        }
        System.out.println("******************************");

        linkedList.forEach(System.out::println);

        //除了能直接用ArrayList中一些方法方法，相对来说，提供了首尾操作的方法
        System.out.println(linkedList.getFirst());
        System.out.println(linkedList.getLast());
        linkedList.removeFirst();
        linkedList.removeLast();
    }
}
