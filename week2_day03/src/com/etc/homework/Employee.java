package com.etc.homework;


import java.time.LocalDate;
import java.time.Period;

/**
 * 员工类
 */
public class Employee {
    private String empno;
    private String empname;
    private LocalDate birthdate;
    private int age; //通过出生日期来自动计算
    private  LocalDate hiredate;
    private String  tel;//手机号码
    private Dept dept;//表示这个与昂工所属的部门(id,deptname)
    private double salary;//后面在优化Bigdecimal


    @Override
    public String toString() {
        return "Employee{" +
                "empno='" + empno + '\'' +
                ", empname='" + empname + '\'' +
                ", birthdate=" + birthdate +
                ", age=" + age +
                ", hiredate=" + hiredate +
                ", tel='" + tel + '\'' +
                ", dept=" + dept +
                ", salary=" + salary +
                '}';
    }

    public Employee() {
    }

    /**
     * 带参数的构造
     * @param empno
     * @param empname
     * @param birthdate
     * @param age
     * @param hiredate
     * @param tel
     * @param dept
     * @param salary
     */
    public Employee(String empno, String empname, LocalDate birthdate, int age, LocalDate hiredate, String tel, Dept dept, double salary) {
        this.empno = empno;
        this.empname = empname;
        this.birthdate = birthdate;
        this.age = age;
        this.hiredate = hiredate;
        this.tel = tel;
        this.dept = dept;
        this.salary = salary;
    }

    public Employee(String empno, String empname, LocalDate birthdate, LocalDate hiredate, String tel, Dept dept, double salary) {
        this.empno = empno;
        this.empname = empname;
        this.birthdate = birthdate;
        this.hiredate = hiredate;
        this.tel = tel;
        this.dept = dept;
        this.salary = salary;
        this.age =Period.between(getBirthdate(), LocalDate.now()).getYears();

    }

    public String getEmpno() {
        return empno;
    }

    public void setEmpno(String empno) {
        this.empno = empno;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public int getAge() {
        //可以通过出生日期得到年龄
        int years = Period.between(getBirthdate(), LocalDate.now()).getYears();
        return years;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDate getHiredate() {
        return hiredate;
    }

    public void setHiredate(LocalDate hiredate) {
        this.hiredate = hiredate;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
