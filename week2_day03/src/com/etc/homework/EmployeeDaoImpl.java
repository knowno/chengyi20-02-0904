package com.etc.homework;

import java.util.ArrayList;
import java.util.List;

/**
 * 后续是要和文件以及数据库整合在一起的
 * 此时，我们就通过一个集合来模拟
 */
public class EmployeeDaoImpl implements EmployeeDao {

    List<Employee> list = new ArrayList<>();

    @Override
    public boolean addEmp(Employee employee) {
        //员工编号能否重复？
        list.add(employee);
        return true;
    }

    @Override
    public boolean updateEmp(Employee employee) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmpno().equals(employee.getEmpno())) {
                //找到了这个人
                list.set(i, employee);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean delEmp(String empno) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmpno().equals(empno)) {
                //找到了这个人
                list.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public Employee getEmp(String empno) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmpno().equals(empno)) {
                //找到了这个人
                return list.get(i);
            }
        }
        return null;
    }

    @Override
    public List<Employee> getEmps(String keywords) {
        List<Employee> returnlist = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmpname().contains(keywords)) {
                //找到了这个人
                returnlist.add(list.get(i));
            }
        }
        return returnlist;
    }

    @Override
    public List<Employee> getEmpsByDept(String deptName) {
        List<Employee> returnlist = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDept().getDeptname().equals(deptName)) {
                //找到了这个人
                returnlist.add(list.get(i));
            }
        }
        return returnlist;
    }
}
