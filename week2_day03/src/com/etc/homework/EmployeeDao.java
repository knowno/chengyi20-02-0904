package com.etc.homework;

import java.util.List;

/**
 * 新建员工数据操作类：定义新增员工，查询员工，修改员工，删除员工的方法
 */
public interface EmployeeDao {

    public boolean addEmp(Employee employee);

    public boolean updateEmp(Employee employee);

    public boolean delEmp(String empno);

    public Employee getEmp(String empno);

    public List<Employee> getEmps(String keywords);

    public List<Employee> getEmpsByDept(String deptName);

}
