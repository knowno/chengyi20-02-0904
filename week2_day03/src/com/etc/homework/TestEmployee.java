package com.etc.homework;

import java.time.LocalDate;
import java.util.List;

public class TestEmployee {
    public static void main(String[] args) {

        //1 增加操作
        EmployeeDao dao = new EmployeeDaoImpl();
        Dept dept1 = new Dept(1,"开发部","开发部门");
        Dept dept2 = new Dept(2,"行政部","行政部门");
        Employee employee1 = new Employee("202301","张三", LocalDate.of(2000,12,12),23,LocalDate.now(),"13612345678",dept1,5234);
        Employee employee2 = new Employee("202302","张4", LocalDate.of(2001,12,12),22,LocalDate.now(),"13612345678",dept2,5235);
        Employee employee3 = new Employee("202303","张5", LocalDate.of(2002,12,12),21,LocalDate.now(),"13612345678",dept1,5236);
        Employee employee4 = new Employee("202304","张6", LocalDate.of(2003,12,12),20,LocalDate.now(),"13612345678",dept2,5237);

        //调用addEmp方法
        dao.addEmp(employee1);
        dao.addEmp(employee2);
        dao.addEmp(employee3);
        dao.addEmp(employee4);

        //2 遍历输出看看 根据关键字查询
        List<Employee> emps = dao.getEmps("");
        emps.forEach(System.out::println);

        //3 修改
        //先查 再改
        Employee emp = dao.getEmp("202304");
        emp.setSalary(10000);

        dao.updateEmp(emp);
        Employee emp2 = dao.getEmp("202304");
        System.out.println(emp2);

        //4 删除
        dao.delEmp("202304");

        List<Employee> emps2 = dao.getEmps("");
        emps2.forEach(System.out::println);



    }
}
