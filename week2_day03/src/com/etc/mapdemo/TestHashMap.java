package com.etc.mapdemo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class TestHashMap {
    public static void main(String[] args) {

        //测试HashMap的常见方法
        HashMap map = new HashMap<>();
        //1 增加元素
        map.put(1,"java");
        //key不能重复
        map.put(1,"Python");

        map.put(2,"JavaScript");
        map.put(3,"Spring");
        //2 看看size
        System.out.println(map.size());
        //打印输出看看
        System.out.println(map);

        //3 获取key集合 ,value集合 ,通过key得到value ,获取key-value
        Set set = map.keySet();
        System.out.println(set);

        Collection values = map.values();
        System.out.println(values);

        //通过key ->value

        for (Object k:set) {
            System.out.println("key :"+k+", value: "+map.get(k));
        }
        //获取entry-set
        Set set1 = map.entrySet();
        for(Object s :set1){
            System.out.println(s);
        }



        //3 修改

        // 将Spring替换掉
        for (Object k:set) {
            System.out.println("key :"+k+", value: "+map.get(k));
            if (map.get(k).equals("Spring")){
                map.put(k,"Spring Cloud Alibaba");
            }
        }

        System.out.println(map);

        //4 删除

        map.remove(3);

        System.out.println(map);


    }
}
