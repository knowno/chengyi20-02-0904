package com.etc.mapdemo;

import java.util.TreeMap;

public class TestTreeMap {
    public static void main(String[] args) {

        TreeMap<Integer, String> treeMap = new TreeMap<>();

        //将元素添加到集合中来
        treeMap.put(1,"abc");
        treeMap.put(89,"abcd");
        treeMap.put(5,"abcde");
        treeMap.put(100,"xyz");
        treeMap.put(3,"qq");

        System.out.println(treeMap);

    }
}
