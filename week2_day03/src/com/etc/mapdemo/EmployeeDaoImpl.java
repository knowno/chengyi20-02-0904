package com.etc.mapdemo;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 后续是要和文件以及数据库整合在一起的
 * 此时，我们就通过一个集合HashMap来模拟
 */
public class EmployeeDaoImpl implements EmployeeDao {

    //我们HashMap存储员工的时候，key=>员工编号(String)  Value=>员工对象(Employee)
    Map<String, Employee> map = new HashMap<>();

    @Override
    public boolean addEmp(Employee employee) {
        //员工编号能否重复？
        if (map.containsKey(employee.getEmpno())) {
            return false;
        }
        map.put(employee.getEmpno(), employee);
        return true;
    }

    @Override
    public boolean updateEmp(Employee employee) {
        //通过编号来定位元素
        if (map.containsKey(employee.getEmpno())) {
            map.put(employee.getEmpno(), employee);
            return true;
        }
        return false;
    }

    @Override
    public boolean delEmp(String empno) {
        if (map.containsKey(empno)) {
            map.remove(empno);
            return true;
        }
        return false;
    }

    @Override
    public Employee getEmp(String empno) {
        if (map.containsKey(empno)) {
            return map.get(empno);
        }
        return null;
    }

    @Override
    public Map<String, Employee> getEmps(String keywords) {
        Map<String, Employee> returnmap = new HashMap<>();
        Iterator<String> iterator = returnmap.keySet().iterator();
        if (keywords.equals("")) {
            return map;
        }
        for (; iterator.hasNext(); ) {
            String key = iterator.next();
            if (map.get(key).getEmpname().contains(keywords)) {
                //找到了这个人
                returnmap.put(map.get(key).getEmpno(), map.get(key));
            }
        }
        return returnmap;
    }

    @Override
    public Map<String, Employee> getEmpsByDept(String deptName) {
        Map<String, Employee> returnmap = new HashMap<>();
        Iterator<String> iterator = returnmap.keySet().iterator();
        for (; iterator.hasNext(); ) {
            String key = iterator.next();
            if (map.get(key).getDept().getDeptname().equals(deptName)) {
                //找到了这个人
                returnmap.put(map.get(key).getEmpno(), map.get(key));
            }
        }
        return returnmap;
    }
}
