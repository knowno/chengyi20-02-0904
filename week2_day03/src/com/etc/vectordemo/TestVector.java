package com.etc.vectordemo;

import java.time.LocalDate;
import java.util.Vector;

public class TestVector {
    public static void main(String[] args) {

        //创建对象
        Vector vector = new Vector();
        //1 增加
        vector.add("java");
        vector.add(LocalDate.now());
        vector.add("小白");
        vector.add(1);
        vector.add(null);
        System.out.println("list.size: " + vector.size());
        //2 删除
        vector.remove(0);
        System.out.println("list.size: " + vector.size());
        //3 输出
        vector.forEach(System.out::println);
        // 4 ->使用add方法insert 插入
        vector.add(1, "小黑");
        System.out.println("******************************");

        vector.forEach(System.out::println);

        // 5 替换操作=>set(index,object)
        for (int i = 0; i < vector.size(); i++) {
            //查找是否包含某某xx
            Object o = vector.get(i);
            if (o instanceof String) {
                if (o.equals("小白")) {
                    //将其修改为小黄
                    vector.set(i,"小黄");
                }
            }

        }
        System.out.println("******************************");

        vector.forEach(System.out::println);


    }
}
