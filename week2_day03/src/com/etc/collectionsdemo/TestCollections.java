package com.etc.collectionsdemo;

import com.etc.homework.Dept;
import com.etc.homework.Employee;

import java.time.LocalDate;
import java.util.*;

public class TestCollections {
    public static void main(String[] args) {

        List<String> list1 = Arrays.asList("az", "java", "axyz", "qq", "c");
        System.out.println(list1);
        //工具类Collections  =>自然顺序
        Collections.sort(list1);
        System.out.println(list1);


        //可以指定排序规则
        Dept dept1 = new Dept(1, "开发部", "开发部门");
        Dept dept2 = new Dept(2, "行政部", "行政部门");
        Employee employee1 = new Employee("202301", "张三", LocalDate.of(2000, 12, 12), LocalDate.now(), "13612345678", dept1, 5234);
        Employee employee2 = new Employee("202302", "张4", LocalDate.of(2001, 12, 12), LocalDate.now(), "13612345678", dept2, 5235);
        Employee employee3 = new Employee("202303", "张5", LocalDate.of(2002, 12, 12), LocalDate.now(), "13612345678", dept1, 5236);
        Employee employee4 = new Employee("202304", "张6", LocalDate.of(2003, 12, 12), LocalDate.now(), "13612345678", dept2, 5237);

        List<Employee> list2 = new ArrayList<>();
        //调用addEmp方法
        list2.add(employee1);
        list2.add(employee2);
        list2.add(employee3);
        list2.add(employee4);
        System.out.println(list2);
        //可以排序的
        Collections.sort(list2,new MyCompare());
        System.out.println(list2);


        List<Employee> employees = Collections.synchronizedList(list2);

    }
}

/**
 * 自定义排序方法
 */
class MyCompare implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getAge() - o2.getAge();
    }
}
