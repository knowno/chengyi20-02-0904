package com.etc.threaddemo;

public class TestRunnable {
    public static void main(String[] args) {

        //启动线程
        Runnable1 runnable1 = new Runnable1();

        new Thread(runnable1).start();

        for (int i = 1; i <= 100; i++) {
            //输出当前线程信息 再加上i
            System.out.println(Thread.currentThread().getName() + " i: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }





    }
}

/**
 * 实现Runnable 重写run方法
 */
class Runnable1 implements Runnable {

    @Override
    public void run() {
        for (int i = 1; i <= 100; i++) {
            //输出当前线程信息 再加上i
            System.out.println(Thread.currentThread().getName() + " i: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
