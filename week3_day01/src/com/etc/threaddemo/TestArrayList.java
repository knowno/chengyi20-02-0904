package com.etc.threaddemo;

import java.util.ArrayList;
import java.util.Vector;

/**
 * ArrayList是否线程安全？ 不安全
 * 我们发现，实际操作过程中，对应的集合长度无法达到50000
 * 所以我们说ArrayList存在线程安全问题(非线程安全)
 *
 * synchronized
 */
public class TestArrayList {
    public static void main(String[] args) throws InterruptedException {

        ArrayList<Object> objects = new ArrayList<>();

        //循环
        for (int i = 1; i <=50000 ; i++) {

            //创建线程对象 ->每循环一次，将这个线程对象name添加到list中
            new Thread(()->objects.add(Thread.currentThread().getName())).start();
        }

        //加一个延迟
        Thread.sleep(20000);
        //多次试验表明，size的值都无法达到50000
        System.out.println(objects.size());

    }

}
