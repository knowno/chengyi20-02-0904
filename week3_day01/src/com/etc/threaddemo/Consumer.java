package com.etc.threaddemo;

import java.util.Random;
public class Consumer implements Runnable {

	private Employee emp;

	public Consumer(Employee emp) {
		super();
		this.emp = emp;
	}

	@Override
	public void run() {
		System.out.println("consumer..........");
		for (int i = 1; i <= 10; i++) {
			// 随机休眠一段时间
			try {
				Thread.sleep(new Random().nextInt(300));

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			emp.getProduct();

		}

	}

}
