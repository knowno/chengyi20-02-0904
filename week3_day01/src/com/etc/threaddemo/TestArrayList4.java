package com.etc.threaddemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ArrayList是否线程安全？ 不安全
 * 我们发现，实际操作过程中，对应的集合长度无法达到50000
 * 所以我们说ArrayList存在线程安全问题(非线程安全)
 * <p>
 * synchronized
 */
public class TestArrayList4 {
    ReentrantLock lock = new ReentrantLock();

    public void test() throws InterruptedException {
        List<Object> objects = new ArrayList<>();

        //循环
        for (int i = 1; i <= 50000; i++) {

            //创建线程对象 ->每循环一次，将这个线程对象name添加到list中
            new Thread(() ->
            {
                lock.lock();
                //多线程情况下，如果有某一个线程进入了这段的时候，其他线程进不来.
                objects.add(Thread.currentThread().getName());
                lock.unlock();
            }
            ).start();
        }

        //加一个延迟
        Thread.sleep(20000);


        System.out.println(objects.size());
    }

    public static void main(String[] args) throws InterruptedException {

        new TestArrayList4().test();

    }

}
