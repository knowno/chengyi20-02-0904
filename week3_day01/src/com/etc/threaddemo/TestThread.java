package com.etc.threaddemo;

public class TestThread {
    public static void main(String[] args) {

        //启动线程
        Thread1 thread1 = new Thread1();
        thread1.start();


        for (int i = 1; i <= 100; i++) {
            //输出当前线程信息 再加上i
            System.out.println(Thread.currentThread().getName() + " i: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }





    }
}

//继承Thread重写run方法
class Thread1 extends Thread {

    @Override
    public void run() {
        for (int i = 1; i <= 100; i++) {
            //输出当前线程信息 再加上i
            System.out.println(Thread.currentThread().getName() + " i: " + i);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
