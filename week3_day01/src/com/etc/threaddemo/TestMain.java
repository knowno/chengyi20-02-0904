package com.etc.threaddemo;

public class TestMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employee emp = new Employee();

		Thread th01 = new Thread(new Producer(emp));
		th01.start();

		Thread th02 = new Thread(new Consumer(emp));
		th02.start();

	}

}
