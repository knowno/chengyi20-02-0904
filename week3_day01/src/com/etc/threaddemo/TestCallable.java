package com.etc.threaddemo;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class TestCallable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        //创建Callable1对象
        Callable1 callable1 = new Callable1();
        //FutureTask对象
        FutureTask futureTask = new FutureTask<>(callable1);

        //将futureTask对象传递给Thread
        Thread thread = new Thread(futureTask);

        //启动线程
        thread.start();
        //获取返回值的方法
        Object o = futureTask.get();
        System.out.println("返回值: " + o);

    }
}

/**
 * 实现implements Callable
 * 注意call（）方法
 * 有返回值
 */
class Callable1 implements Callable {
    @Override
    public Object call() throws Exception {

        for (int i = 1; i < 10; i++) {
            System.out.println(Thread.currentThread().getName());
        }
        return "success";
    }
}
