package com.etc.threaddemo;

public class Employee {

    // 假设目前商品的库存为-1
    private int productCount = -1;

    // 生产商品信息(进货)
    public synchronized void setProduct(int productCount) {
        if (this.productCount != -1) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        // 不是直接做生产操作
        this.productCount = productCount;
        //
        System.out.println("生产者,生产setProduct完成 " + this.productCount);

        notifyAll();

    }

    // 获取商品信息(买)

    public synchronized int getProduct() {

        if (this.productCount == -1) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }


        // 消费完了
        int n = this.productCount;

        System.out.println("消费者,购买 getProduct:" + n);

        this.productCount = -1;

        notifyAll();

        return n;
    }

}
