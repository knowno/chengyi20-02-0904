package com.etc.entity;

import com.etc.myex.DataValueException;

public class Employee {
    private int id;
    private String name;
    private double salary;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        //需要对员工工资进行判断，不能低于2500
        if (salary <= 2500) {
            //这里抛出一个异常 ,如果抛出的异常是RuntimeException的子类，那么不用显式处理，编译可以通过
            //但是，如果抛出异常是Exception的子类，那么需要显式处理(检查时异常)
            throw  new DataValueException("工资最低标准应该大于2500");
        }
        this.salary = salary;
    }
}
