package com.etc.myex;

/**
 * 特定自定义的类型
 */
public class DataValueException extends RuntimeException {
    @java.io.Serial
    static final long serialVersionUID = -703489719074576695L;

    public DataValueException() {
        super();
    }

    public DataValueException(String s) {
        super(s);
    }

}
