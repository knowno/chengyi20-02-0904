package review;

import java.util.Scanner;

public class TestHomeWork2 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("输入一个整数：");
        int x = sc.nextInt();
        int y = 0,z;
        int i=0,a=0;
        boolean flag = false;
        //判断是否为负数
        if(x<0){
            x=Math.abs(x);
            flag = true;
        }
        while(x>0) {
            z = x % 10;
            y = y * 10+z;
            x /= 10;
            i++;
        }
        if(flag) {
            System.out.println("这个整数是"+i+"位数。");
            System.out.println("逆序输出：-"+y);
        }else {
            System.out.println("这个整数是"+i+"位数。");
            System.out.println("逆序输出："+y);
        }
    }
}
