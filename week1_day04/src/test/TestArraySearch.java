package test;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * 数组有关的算法,查找:
 * 在已知的一组数据中，查询是否包含特定的某个元素(查到了一个匹配元素就结束？从头查到尾？)
 *
 */
public class TestArraySearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Random random = new Random();
        //定义一个数组,给赋一些随机值
        int arr1[] = new int[10];
        //赋值，我们要通过索引
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = random.nextInt(100);

        }
        //打印输出原始数组
        System.out.println(Arrays.toString(arr1));

        //要输入查找的元素的值
        System.out.println("请输入要查找的元素的值");
        int find = scanner.nextInt();

        //标记一下
        boolean flag = false;

        for (int i = 0; i < arr1.length; i++) {

            //将数组元素依次和要查找的数据进行比较
            if (arr1[i] == find) {
                System.out.println("在索引为"+i+"的位置找到了"+find);
                flag = true;
                break;
            }

        }
        if (!flag){
            System.out.println("没有找到 "+find);
        }


    }
}
