package test;

import java.util.Arrays;
import java.util.Random;

/**
 * 数组定义
 */
public class TestArray2 {
    public static void main(String[] args) {

        Random random = new Random();
        //定义一个数组,给赋一些随机值
        int arr1 [] = new int[10];
        //赋值，我们要通过索引
        for (int i = 0; i <arr1.length ; i++) {
            arr1[i] = random.nextInt(100);
        }

        //访问并输出
        for (int i = 0; i <arr1.length ; i++) {
            System.out.println(arr1[i]);
        }
        //增强的for循环
        for (int x:arr1) {
            System.out.println(x);
        }
        //直接输出数组
        System.out.println(arr1);
        //只是想打印数组元素的值看看
        System.out.println(Arrays.toString(arr1));


    }
}
