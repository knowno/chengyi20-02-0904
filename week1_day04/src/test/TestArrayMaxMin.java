package test;

import java.util.Arrays;
import java.util.Random;

/**
 * 数组有关的算法,最值:
 * 最大值(我们认为数组第一个元素为最大值，用剩余每个元素依次和这个元素比较，如果发现某个元素大于当前元素，则将大这个元素赋值给max)
 *
 */
public class TestArrayMaxMin {
    public static void main(String[] args) {

        Random random = new Random();
        //定义一个数组,给赋一些随机值
        int arr1[] = new int[10];
        //赋值，我们要通过索引
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = random.nextInt(100);

        }
        //打印输出原始数组
        System.out.println(Arrays.toString(arr1));
        //假设数组第一个元素为最大值(最小值)
        int max = arr1[0];

        //最小值
        int min = arr1[0];
        //因为上面已经将arr1[0]赋值给max,所以我们下面的循环从1开始
        for (int i = 1; i < arr1.length; i++) {

            //如果发现数组某个元素 >max，就将这个元素赋值给max
            if (arr1[i] > max) {
                max = arr1[i];
            }

            if (arr1[i] < min) {
                min = arr1[i];
            }
        }

        System.out.println("最大值 max :"+max);
        System.out.println("最小值 min :"+min);


    }
}
