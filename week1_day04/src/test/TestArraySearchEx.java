package test;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * 数组有关的算法,查找:
 * 1. 练习一下求和，平均值，最大值，最小值的算法
 * 2. 查找特定字符串数组中是否包含某个子字符串,要求查出所有满足条件的元素.
 */
public class TestArraySearchEx {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //定义一个数组,给赋一些值
        String arr1[] = {"Java", "Python", "Go", "JavaScript", "Springcloud", "SpringBoot"};

        //打印输出原始数组
        System.out.println(Arrays.toString(arr1));

        //要输入查找的元素的值
        System.out.println("请输入要查找的字符串");
        String find = scanner.next();

        //标记一下
        boolean flag = false;

        for (int i = 0; i < arr1.length; i++) {

            //将数组元素依次和要查找的数据进行比较
            //equals比较两个字符串内容是否相等 =>精确比较
            //contains 判断一个字符串包含特定的字符串 =>模糊查询
            if (arr1[i].contains(find)) {
                System.out.println("在索引为" + i + "的位置找到了" + find+" 数组元素的值为: "+arr1[i]);
                flag = true;

            }

        }
        if (!flag) {
            System.out.println("没有找到 " + find);
        }


    }
}
