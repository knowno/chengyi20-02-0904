package test;

import java.util.Arrays;
import java.util.Random;

/**
 * 数组有关的算法 求和
 */
public class TestArraySum {
    public static void main(String[] args) {

        Random random = new Random();
        //定义一个数组,给赋一些随机值
        int arr1[] = new int[10];
        //赋值，我们要通过索引

        double sum = 0;
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = random.nextInt(100);
            //数组元素累加
            sum += arr1[i];
        }

        System.out.println("sum :"+sum);
        System.out.println("avg :"+sum/arr1.length);


    }
}
