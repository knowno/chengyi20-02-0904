package test.testsort;

import java.util.Arrays;

/**
 * 快速排序是一种比较高效的排序算法，采用“分而治之”的思想，通过多次比较和交换来实现排序，
 * 在一趟排序中把将要排序的数据分成两个独立的部分，对这两部分进行排序使得其中一部分所有数据比另一部分都要小，然后继续递归排序这两部分，最终实现所有数据有序。
 */
public class TestQuickSort {
    public static void main(String[] args) {

        //初始数组
        int[] arr = {50,100, 3, 5, 7, 90, 6, 3, 70};
        //调用方法
        quickSort(arr,0,arr.length-1);
        //输出排序后的结果
        System.out.println(Arrays.toString(arr));
    }


    /**
     * 快速排序(嵌套循环+递归调用)
     *
     * @param arr
     */
    public static void quickSort(int[] arr, int low, int high) {
        if (low >= high) {
            return;
        }
        //基准位
        int pivot = arr[low];
        //左侧移动的变量left
        int left = low;
        //右侧移动变量right
        int right = high;
        //通过循环来处理我们的指针(left ,right)的移动 （定义两个指针: 移动 left ++   right --    left<right）
        while (left < right) {
            //右侧开始
            while (left < right && arr[right] >= pivot) {
                right--;
            }
            //左侧开始
            while (left < right && arr[left] <= pivot) {
                left++;
            }
            //此时left依然小于right
            if (left < right) {
                //交换处理
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
            }
        }
        //相遇
        arr[low] = arr[left];
        arr[left] = pivot;
        //递归调用(先左侧)
        if (low<left){
            quickSort(arr,low,left-1);
        }
        //递归调用(后右侧)
        if (right<high){
            quickSort(arr,right+1,high);
        }

    }

}
