package test.testsort;

import java.util.Arrays;

/**
 * 选择排序的主要思想是寻找未排序中最小的元素加入到已有序列,直到未排序序列为空。有一无序数组,如下,现按照选择排序的规则进行升序排序:
 * 现将该数组分为两个部分,第一部分为有序部分,第二部分为无序部分,在初始时,有序部分的元素数量为0,全部为无序部分:
 * 在无序部分中寻找最小元素所在的位置,然后将其与第一位的元素交换:
 * 直到进行到所有无序部分的元素都被排序:
 * 选择排序的特点是查询次数较多,元素位置变换较少,比较适合易于查询而移动较复杂的数据
 */
public class TestSelectionSort {
    public static void main(String[] args) {

        //初始数组
        int[] arr = {1, 3, 5, 7, 9, 6, 3, 1};
        //调用方法
        selectionSort(arr);
        //输出排序后的结果
        System.out.println(Arrays.toString(arr));

    }


    /**
     * 选择排序(嵌套循环)
     * 外层控制轮(每一轮都找最小值的索引，将最小值的索引的位置元素和i个元素进行交换)
     *
     * @param arr
     */
    public static void selectionSort(int[] arr) {
        //定义一个变量用来存储最小元素的那个索引(下标位置)
        int smallindex;
        for (int i = 0; i < arr.length-1; i++) {
            //给smallindex赋值->每次循环进来，i就是smallindex
            smallindex = i;
            //比较，找无序部分中最小的元素，得到其索引值
            for (int j = i + 1; j < arr.length; j++) {
                //判断比较
                if (arr[j] < arr[smallindex]) {
                    //谁才是smallindex => j
                    smallindex = j;
                }
            }
            //找到这一轮比较中最小的那个元素索引 smallindex
            if (smallindex != i) {
                //交换
                int temp = arr[smallindex];
                arr[smallindex] = arr[i];
                arr[i] = temp;
            }
        }


    }

}
