package test.testsort;

import java.util.Arrays;

/**
 * 插入排序与选择排序类似，需要将数组分为有序与无序两部分。但插入不会去到无序部分选择，
 * 而是随意选取一个无序部分元素到有序部分中寻找它所在的位置进行插入保持有序部分仍然有序，如果要对数据进行排序：
 * 首先认为第一个元素部分为有序部分,
 * 选取无序部分的第一个元素,到有序部分中寻找位置并插入：
 * 主要集中在查找位置及元素的移动上,如果在链表中进行插入排序,会比数组中排序效率高
 */
public class TestInsertionSort {
    public static void main(String[] args) {

        //初始数组
        int[] arr = {1, 3, 5, 7, 9, 6, 3, 0};
        //调用方法
        insertionSort(arr);
        //输出排序后的结果
        System.out.println(Arrays.toString(arr));

    }


    /**
     * 插入排序(嵌套循环)
     *
     *
     * @param arr
     */
    public static void insertionSort(int[] arr) {
        //定义一个变量"当前"元素
        int curr;
        //认为第一个元素是已经排序好的部分，所以这里的i=1
        for (int i = 1; i < arr.length; i++) {
            //将i所在的位置元素赋值给curr
            curr = arr[i];
            //比较，将curr元素和已经排序好的部分元素依次比较
            for (int j = i - 1; j >= 0; j--) {
                //判断比较
                if (curr < arr[j]) {
                    //将索引为j的元素向后移动一位
                    arr[j + 1] = arr[j];
                } else {
                    //curr放在j+1的位置就可以了
                    arr[j + 1] = curr;
                    break;
                }

                //如果j==0
                if (j == 0) {
                    arr[j] = curr;
                }
            }


        }


    }

}
