package test.testsort;

import java.util.Arrays;
import java.util.Collections;

/**
 * 冒泡排序主要的思想是进行相邻的两个元素之间比较并且交换，有利于利用原有元素在集合中的位置优势，冒泡排序的原则是大的下沉小的上浮（跟最终的排序要求保持一致）\
 * 第一轮排序完毕,它能够使最大数沉到最底端，然后进行第二轮的排序。每一轮排序能够确保其最大数沉到相应位置，经过元素个数减一次的排序会生成有序数组
 */
public class TestBubbleSort {
    public static void main(String[] args) {

        //初始数组
        int[] arr = {1, 3, 5, 7, 9, 6, 3, 0};
        //调用方法
        bubbleSort(arr);
        //输出排序后的结果
        System.out.println(Arrays.toString(arr));


    }


    /**
     * 冒泡排序(嵌套循环)
     *
     *
     * @param arr
     */
    public static void bubbleSort(int[] arr) {

        //嵌套循环
        for (int i = 0; i <arr.length-1 ; i++) {

            for (int j = 0; j <arr.length-i-1; j++) {

                //相邻元素依次比较
                if (arr[j]>arr[j+1]){
                    //交换
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] =temp;
                }
            }
        }

    }

}
