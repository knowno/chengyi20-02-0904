package test;

import java.util.ArrayList;

/**
 * 数组定义
 */
public class TestArray {
    public static void main(String[] args) {
        //语法 数据类型 [] 数组名

        //声明
        int [] array1 ;
        int array2[];

        //先声明再初始化,默认值为0
        array1 = new int[10];

        //不允许的语法
//        array2={1,2,3};

        //声明 直接初始化
        int array3[] = {1,2,3};

        System.out.println(array1[0]);
        //数组 有序，通过下标(索引)进行访问.从0开始，到数组长度-1结束.
        //xception in thread "main" java.lang.ArrayIndexOutOfBoundsException

        System.out.println(array3[3]);

    }
}
