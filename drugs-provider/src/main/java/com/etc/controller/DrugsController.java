package com.etc.controller;

import com.etc.dto.DrugsDto;
import com.etc.entity.Drugs;
import com.etc.service.DrugsService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Drugs)表控制层
 *
 * @author makejava
 * @since 2023-11-14 09:18:17
 */
@RestController
@RequestMapping("drugs")
public class DrugsController {
    /**
     * 服务对象
     */
    @Resource
    private DrugsService drugsService;

    /**
     * 分页查询
     *
     * @param drugs       筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<Drugs>> queryByPage(Drugs drugs, PageRequest pageRequest) {
        return ResponseEntity.ok(this.drugsService.queryByPage(drugs, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<DrugsDto> queryById(@PathVariable("id") Integer id) {
        Drugs drugs = this.drugsService.queryById(id);
        DrugsDto drugsDto = new DrugsDto();
        if (drugs != null) {
            BeanUtils.copyProperties(drugs,drugsDto);
        }
        return ResponseEntity.ok(drugsDto);
    }

    /**
     * 新增数据
     *
     * @param drugs 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<Drugs> add(Drugs drugs) {
        return ResponseEntity.ok(this.drugsService.insert(drugs));
    }

    /**
     * 编辑数据
     *
     * @param drugs 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<Drugs> edit(Drugs drugs) {
        return ResponseEntity.ok(this.drugsService.update(drugs));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Integer id) {
        return ResponseEntity.ok(this.drugsService.deleteById(id));
    }

}

