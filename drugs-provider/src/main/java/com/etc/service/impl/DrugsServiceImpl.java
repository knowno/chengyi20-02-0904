package com.etc.service.impl;

import com.etc.entity.Drugs;
import com.etc.dao.DrugsDao;
import com.etc.service.DrugsService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;

/**
 * (Drugs)表服务实现类
 *
 * @author makejava
 * @since 2023-11-14 09:18:19
 */
@Service("drugsService")
public class DrugsServiceImpl implements DrugsService {
    @Resource
    private DrugsDao drugsDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Drugs queryById(Integer id) {
        return this.drugsDao.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param drugs       筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    @Override
    public Page<Drugs> queryByPage(Drugs drugs, PageRequest pageRequest) {
        long total = this.drugsDao.count(drugs);
        return new PageImpl<>(this.drugsDao.queryAllByLimit(drugs, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param drugs 实例对象
     * @return 实例对象
     */
    @Override
    public Drugs insert(Drugs drugs) {
        this.drugsDao.insert(drugs);
        return drugs;
    }

    /**
     * 修改数据
     *
     * @param drugs 实例对象
     * @return 实例对象
     */
    @Override
    public Drugs update(Drugs drugs) {
        this.drugsDao.update(drugs);
        return this.queryById(drugs.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.drugsDao.deleteById(id) > 0;
    }
}
