package com.etc.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * (Drugs)实体类
 *
 * @author makejava
 * @since 2023-11-14 09:18:18
 */
public class DrugsDto implements Serializable {
    private static final long serialVersionUID = -64619452011401915L;
    /**
     * ID主键
     */
    private Integer id;
    /**
     * 药品编码
     */
    private String drugscode;
    /**
     * 药品名称
     */
    private String drugsname;
    /**
     * 药品规格
     */
    private String drugsformat;
    /**
     * 包装单位
     */
    private String drugsunit;
    /**
     * 生产厂家
     */
    private String manufacturer;
    /**
     * 药品剂型
     */
    private Integer drugsdosageid;
    /**
     * 药品类型
     */
    private Integer drugstypeid;
    /**
     * 药品单价
     */
    private Double drugsprice;
    /**
     * 拼音助记码
     */
    private String mnemoniccode;
    /**
     * 创建时间
     */
    private Date creationdate;
    /**
     * 最后修改时间
     */
    private Date lastupdatedate;
    /**
     * 删除标记
     */
    private Integer delmark;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDrugscode() {
        return drugscode;
    }

    public void setDrugscode(String drugscode) {
        this.drugscode = drugscode;
    }

    public String getDrugsname() {
        return drugsname;
    }

    public void setDrugsname(String drugsname) {
        this.drugsname = drugsname;
    }

    public String getDrugsformat() {
        return drugsformat;
    }

    public void setDrugsformat(String drugsformat) {
        this.drugsformat = drugsformat;
    }

    public String getDrugsunit() {
        return drugsunit;
    }

    public void setDrugsunit(String drugsunit) {
        this.drugsunit = drugsunit;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Integer getDrugsdosageid() {
        return drugsdosageid;
    }

    public void setDrugsdosageid(Integer drugsdosageid) {
        this.drugsdosageid = drugsdosageid;
    }

    public Integer getDrugstypeid() {
        return drugstypeid;
    }

    public void setDrugstypeid(Integer drugstypeid) {
        this.drugstypeid = drugstypeid;
    }

    public Double getDrugsprice() {
        return drugsprice;
    }

    public void setDrugsprice(Double drugsprice) {
        this.drugsprice = drugsprice;
    }

    public String getMnemoniccode() {
        return mnemoniccode;
    }

    public void setMnemoniccode(String mnemoniccode) {
        this.mnemoniccode = mnemoniccode;
    }

    public Date getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Date creationdate) {
        this.creationdate = creationdate;
    }

    public Date getLastupdatedate() {
        return lastupdatedate;
    }

    public void setLastupdatedate(Date lastupdatedate) {
        this.lastupdatedate = lastupdatedate;
    }

    public Integer getDelmark() {
        return delmark;
    }

    public void setDelmark(Integer delmark) {
        this.delmark = delmark;
    }

}

