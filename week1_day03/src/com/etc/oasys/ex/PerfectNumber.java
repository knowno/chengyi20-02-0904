package com.etc.oasys.ex;

/**
 * 求1-100乊间，有哪些数是完数。【完全数（Perfect number），又称完美数或完备数，是一些特殊的
 * 自然数。它所有的真因子（即除了自身以外的约数）的和（即因子函数），恰好等于它本身。例如：6=1+2+3】
 */
public class PerfectNumber {
    public static void main(String[] args) {
        //外循环 从1到100
        for (int i = 1; i <= 100; i++) {
           //内循环
            int sum=0;
            for (int j = 1; j <=i/2; j++) {
                if(i%j==0){
                    //保留并累加
                    sum +=j;
                }
            }
            if (sum == i) {
                System.out.println(i);
            }
        }


    }
}
