package com.etc.oasys.ex;

/**
 * 题目：有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生
 * 一对兔子。假如兔子都丌死，要求输出一年内兔子的数量是多少。
 */
public class RabbitEx {
    public static void main(String[] args) {

        //兔子的规律 1 1 2 3 5 8 13 ...
        //1 使用循环结构
        int a = 1;
        int b = 1;
        for (int i = 3; i <= 10; i++) {
            int c = a + b;
            System.out.println(i +" c: "+c);

            a = b;
            b = c;
        }

        //2 使用数组+循环
        int [] arr = new int[10];
        arr[0] =arr[1] = 1;
        for (int i = 2; i <10 ; i++) {
            arr[i] = arr[i-1] + arr[i-2];
        }

        System.out.println(arr[9]);

    }
}
