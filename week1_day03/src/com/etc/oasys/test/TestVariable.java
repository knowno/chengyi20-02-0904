package com.etc.oasys.test;

public class TestVariable {

    public static void main(String[] args) {
        //语法格式
        // 数据类型 变量名 = 值

        //变种  数据类型 变量名 ; 变量名 = 赋值
        // 数据类型 变量名1,变量名2 ....

        //变量名要求有一定含义
        int age = 20; //明确 定义一个int变量，内存开辟4个字节的空间。存储整数20

        //能不能中文?
        int 年龄 = 23; //可以，但是尽量不要
        //英文，或者缩写。 骆驼命名方法   学生年龄 studentAge/stuAge
        //尽量避免    xueshengAge

        //String str ;

        //Variable 'str' might not have been initialized
       // System.out.println(str);


        //使用final修饰就是常量
        final String str1 = "hello";

        //str1 ="world"; //常量一旦赋值，就不能修改
        System.out.println(str1);


    }
}
