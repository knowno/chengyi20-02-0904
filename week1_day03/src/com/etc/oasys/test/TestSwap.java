package com.etc.oasys.test;

/**
 * 交换两个变量的值
 */
public class TestSwap {

    public static void main(String[] args) {

        //定义两个变量
        int a = 1;
        int b = 2;
        System.out.println("a :"+a+", b: "+b);
        //定义临时变量
        // 将a赋值给t
        int t = a;
        //将b赋值给a
        a  =  b;
        //将t赋值给b
        b = t;

        System.out.println("a :"+a+", b: "+b);


    }
}
