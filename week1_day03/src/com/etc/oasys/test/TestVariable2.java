package com.etc.oasys.test;

/**
 * 交换两个变量的值
 */
public class TestVariable2 {

    //类成员属性(有默认值的)
    static boolean flag1 ;

    public static void main(String[] args) {

       boolean flag ;//没赋值,没有默认值，不能直接调用
//        System.out.println(flag);//没有默认值

        System.out.println(flag1);

        Object o = new Object();

        //隐藏技能 (65-90 A->Z  97-122  a-z  48-57 '0'->'9')
        char ch = 90; // 90=>相当于哪个字符？


    }
}
