package com.etc.oasys.test;

public class TestLoop2 {
    public static void main(String[] args) {

        //循环结构 while ->先判断
        int i = 1;
        while (i >1) { //false
            System.out.println("i :" + i);
            i++;
        }

        System.out.println("*****************************");
        //循环结构do-while  ->先执行(循环体一定会执行)
        short j = 1;
        do {
            System.out.println("j :" + j);
            j++; //2

        }while (j>1);

        System.out.println(j);

        //循环结构for
    }

}
