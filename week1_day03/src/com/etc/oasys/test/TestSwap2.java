package com.etc.oasys.test;

/**
 * 交换两个变量的值,要求不能使用第三个变量
 */
public class TestSwap2 {

    public static void main(String[] args) {

        //定义两个变量
        int a = 1;
        int b = 2;
        System.out.println("a :"+a+", b: "+b);

        //利用运算符进行一些处理
        a = a+b;//将a+b的和存储在a中
        b = a-b; //在两个数字的和基础上，减去b，得到的结果为a,相当于把a赋值给b
        a = a -b; //两个数字的和，减去a，赋值给b


        System.out.println("a :"+a+", b: "+b);


    }
}
