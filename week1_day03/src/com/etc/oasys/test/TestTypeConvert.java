package com.etc.oasys.test;

/**
 * 类型转换
 */
public class TestTypeConvert {



    public static void main(String[] args) {

      // 1 基本类型之间转换  int ->double
        double d = 123; //  doube = int 自动转换/隐式转换

        //反之
        int x = (int)3.14 ;// int = double 编译出错 强制转换/显式转换
        System.out.println(x);

        //2 常见转换 字符串和基本类型之间的转换
        String str1 = "123";
        String str2 = "456";
        //将两个字符串转换为数字格式，数学运算
        System.out.println(str1+str2);

        //如果不要拼接，要转型再计算？
        // Integer
        Integer n1 = Integer.valueOf(str1);

        int n2 = Integer.parseInt(str2);
        System.out.println(n1+n2);

    }
}
