package com.etc.oasys.test;

/**
 * == 用来比较基本类型，就是比较二者的值
 * 用来比较引用类型，则比较的是地址.
 */
public class TestOperator {

    public static void main(String[] args) {

        int n1 = 1;
        int n2 = 1;
        // == 值的比较
        System.out.println(n1==n2);

        String str1 = "hello";
        String str2 = "hello";
        // ==  字面值在常量池，共享，str1和str2的引用是相同的地址
        // == 比较的的确是str1和str2的地址
        System.out.println(str1 == str2);

        Object o1 = new Object();
        Object o2 = new Object();

        // ==两个object都在堆中，内存地址一定不同
        System.out.println(o1 == o2);

    }
}
