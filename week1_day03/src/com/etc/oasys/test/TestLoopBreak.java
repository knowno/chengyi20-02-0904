package com.etc.oasys.test;

import java.util.Arrays;

public class TestLoopBreak {
    public static void main(String[] args) {

        //循环结构for
        for (int k = 1; k <=10 ; k++) {
            System.out.println("break之前 k :"+k);
            if (k==5){
                break;
            }
            System.out.println("break之后 k"+k);
        }

        System.out.println("跳出循环后位置");


    }

}
