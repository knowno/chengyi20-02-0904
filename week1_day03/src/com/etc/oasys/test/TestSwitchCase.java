package com.etc.oasys.test;

import java.util.Scanner;

public class TestSwitchCase {
    public static void main(String[] args) {

        //用户从控制台输入年和月份，输出这个月有多少天
        Scanner input = new Scanner(System.in);
        System.out.println("请输入年");
        int year = input.nextInt();
        System.out.println("请输入月份");
        int month = input.nextInt();
        input.close();

        //对月份进行判断
        int day=30;

        // jdk 8
        switch (month){
            //1 3 5 7 8 10 12 =>31
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day=31;
                break;
            //4 6 9 11 =>30
            case 4:
            case 6:
            case 9:
            case 11:
                day=30;
                break;
            //2 判断是否为闰年
            case 2:
                if((year%4==0 && year%100!=0) || (year%400==0))
                {
                    day=29;
                }
                else{
                    day=28;
                }
                break;
            default:
                System.out.println("输入错误");
                break;
        }


        System.out.println(year+" 年 "+month+" 月有"+day+"天");




    }
}
