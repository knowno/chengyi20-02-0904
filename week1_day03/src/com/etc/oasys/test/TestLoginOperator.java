package com.etc.oasys.test;

/**
 * 逻辑运算符   (boolean &&  boolean)=>boolean
 */
public class TestLoginOperator {
    public static void main(String[] args) {

        // & &&
        int a = 1;
        int b = 2;
        int c = 3 ;
        //双与 && 左侧如果为false 则右侧不执行
        //if(a > b && c++>4){
        //单与 & 左侧如果为false 则右侧依然执行
        if(a > b & c++>4){
            System.out.println("条件成立"); //不会输出
        }
        System.out.println("c:"+c); //


        //| ||

    }
}
