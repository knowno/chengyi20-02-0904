package com.etc.oasys.test;

public class TestLoopContinue {
    public static void main(String[] args) {

        //循环结构for
        for (int k = 1; k <=10 ; k++) {
            System.out.println("continue 之前 k :"+k); // 几次
            if (k==5){
              continue;
            }
            System.out.println("continue之后 k"+k); //几次
        }

        System.out.println("结束循环后位置");


    }

}
