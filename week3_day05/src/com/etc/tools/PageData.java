package com.etc.tools;

import java.util.List;

/**
 *自定义类,分页相关
 * @param <T>
 */
public class PageData<T> {
   // 当前页
   private int pageNo;
   // 每页记录数
   private int pageSize;
   // 总记录数
   private long total;
   // 总页数
   private int totalPage;
   // 当前页数据集
   private List<T> data;

   public int getPageNo() {
      return pageNo;
   }

   public void setPageNo(int pageNo) {
      this.pageNo = pageNo;
   }

   public int getPageSize() {
      return pageSize;
   }

   public void setPageSize(int pageSize) {
      this.pageSize = pageSize;
   }

   public long getTotal() {
      return total;
   }

   public void setTotal(long total) {
      this.total = total;
   }

   public int getTotalPage() {
      // 自己来计算一下 10条记录 -》2 11-》3 15-》3
      totalPage = (int) (total / pageSize);
      if (total % pageSize != 0) {
         totalPage++;
      }
      return totalPage;
   }

   public void setTotalPage(int totalPage) {
      this.totalPage = totalPage;
   }

   public List<T> getData() {
      return data;
   }

   public void setData(List<T> data) {
      this.data = data;
   }

   public PageData(int pageNo, int pageSize, long total, List<T> data) {
      super();
      this.pageNo = pageNo;
      this.pageSize = pageSize;
      this.total = total;
      this.data = data;
   }

   public PageData(int pageNo, int pageSize, long total, int totalPage, List<T> data) {
      super();
      this.pageNo = pageNo;
      this.pageSize = pageSize;
      this.total = total;
      this.totalPage = totalPage;
      this.data = data;
   }

   public PageData() {
      // TODO Auto-generated constructor stub
   }

   @Override
   public String toString() {
      return "PageData [pageNo=" + pageNo + ", pageSize=" + pageSize + ", total=" + total + ", totalPage=" + getTotalPage()
            + ", data=" + data + "]";
   }

}
