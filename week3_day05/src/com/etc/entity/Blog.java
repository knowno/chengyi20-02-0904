package com.etc.entity;

public class Blog {
  private Integer id;
  private String blogtitle;

    public Blog() {
    }

    public Blog(Integer id, String blogtitle) {
        this.id = id;
        this.blogtitle = blogtitle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBlogtitle() {
        return blogtitle;
    }

    public void setBlogtitle(String blogtitle) {
        this.blogtitle = blogtitle;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", blogtitle='" + blogtitle + '\'' +
                '}';
    }
}
