package com.etc.entity;

public class Emp {
    private Integer empid;
    private String empname;
    private String empsex;
    private String empjob;
    private String empbirthdate;
    private int warehouseid;
    private int leader;

    public Integer getEmpid() {
        return empid;
    }

    public void setEmpid(Integer empid) {
        this.empid = empid;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public String getEmpsex() {
        return empsex;
    }

    public void setEmpsex(String empsex) {
        this.empsex = empsex;
    }

    public String getEmpjob() {
        return empjob;
    }

    public void setEmpjob(String empjob) {
        this.empjob = empjob;
    }

    public String getEmpbirthdate() {
        return empbirthdate;
    }

    public void setEmpbirthdate(String empbirthdate) {
        this.empbirthdate = empbirthdate;
    }

    public int getWarehouseid() {
        return warehouseid;
    }

    public void setWarehouseid(int warehouseid) {
        this.warehouseid = warehouseid;
    }

    public int getLeader() {
        return leader;
    }

    public void setLeader(int leader) {
        this.leader = leader;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "empid=" + empid +
                ", empname='" + empname + '\'' +
                ", empsex='" + empsex + '\'' +
                ", empjob='" + empjob + '\'' +
                ", empbirthdate='" + empbirthdate + '\'' +
                ", warehouseid=" + warehouseid +
                ", leader=" + leader +
                '}';
    }
}
