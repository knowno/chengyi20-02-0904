package com.etc.test;

import com.etc.dao.EmpDao;
import com.etc.dao.impl.EmpDaoImpl;
import com.etc.entity.Emp;
import com.etc.tools.DBUtil2;
import com.etc.tools.PageData;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestEmp {
    //针对刚刚写的dao的方法的测试
    EmpDao dao = null;
    @Before
    public void before(){
        dao = new EmpDaoImpl();
    }
    @Test
    public void testgetEmp(){
        List<Emp> emp = dao.getEmp();
        emp.forEach(System.out::println);
    }

    @Test
    public void testgetEmpBypage(){
        //查询所有记录
//        PageData<Emp> empByPage = dao.getEmpByPage(1, 2, "");

        //根据关键字
        PageData<Emp> empByPage = dao.getEmpByPage(1, 2, "白");
        System.out.println(empByPage);
    }


}
