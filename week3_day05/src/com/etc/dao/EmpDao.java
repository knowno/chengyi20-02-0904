package com.etc.dao;

import com.etc.entity.Emp;
import com.etc.tools.PageData;

import java.util.List;

public interface EmpDao {
    /**
     * 增加
     *
     * @param emp
     * @return
     */
    public boolean add(Emp emp);

    /**
     * 修改
     *
     * @param emp
     * @return
     */
    public boolean update(Emp emp);

    /**
     * 删除
     *
     * @param empid
     * @return
     */
    public boolean delById(Integer empid);

    /**
     * 查询所有
     *
     * @return
     */

    public List<Emp> getEmp();

    /**
     * 根据关键字查询
     *
     * @param keywords
     * @return
     */
    public List<Emp> getEmpByKeywords(String keywords);

    /**
     * 查询单个记录
     *
     * @param empid
     * @return
     */
    public Emp getById(Integer empid);

    /**
     * 模糊查询+分页
     * @param pageNo
     * @param pageSize
     * @param keywords
     * @return
     */
    public PageData<Emp> getEmpByPage(Integer pageNo, Integer pageSize, String keywords);
}
