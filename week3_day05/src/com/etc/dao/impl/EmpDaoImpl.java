package com.etc.dao.impl;

import com.etc.dao.EmpDao;
import com.etc.entity.Emp;
import com.etc.tools.DBUtil2;
import com.etc.tools.PageData;

import java.util.List;

public class EmpDaoImpl implements EmpDao {
    @Override
    public boolean add(Emp emp) {
        return false;
    }

    @Override
    public boolean update(Emp emp) {
        return false;
    }

    @Override
    public boolean delById(Integer empid) {
        return false;
    }

    @Override
    public List<Emp> getEmp() {
        return DBUtil2.exQuery("select * from tbl_emp",Emp.class);
    }

    @Override
    public List<Emp> getEmpByKeywords(String keywords) {
        return null;
    }

    @Override
    public Emp getById(Integer empid) {
        return null;
    }

    @Override
    public PageData<Emp> getEmpByPage(Integer pageNo, Integer pageSize, String keywords) {
        return DBUtil2.exQueryByPage("select * from tbl_emp where empname like ?",Emp.class,pageNo,pageSize,"%"+keywords+"%");
    }
}
