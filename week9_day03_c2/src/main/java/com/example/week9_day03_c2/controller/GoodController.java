package com.example.week9_day03_c2.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@RestController
@Slf4j
public class GoodController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("goods/{userid}")
    public String getUserById(@PathVariable Integer userid){
        //使用RestTemplate调用week9_day03_c1下的服务
        String forObject = restTemplate.getForObject("http://localhost:9801/user/{userid}", String.class,userid);
        log.info("c2 "+forObject);
        return forObject;
    }
}

