/*
Navicat MySQL Data Transfer

Source Server         : mysqlserver
Source Server Version : 80022
Source Host           : 127.0.0.1:3307
Source Database       : hisdb

Target Server Type    : MYSQL
Target Server Version : 80022
File Encoding         : 65001

Date: 2023-11-24 14:00:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `total_price` decimal(10,2) DEFAULT NULL COMMENT '总价',
  `pay_price` decimal(10,2) DEFAULT NULL COMMENT '实付款',
  `discount` decimal(10,2) DEFAULT '0.00' COMMENT '优惠金额',
  `transport_price` decimal(10,2) DEFAULT '0.00' COMMENT '运费',
  `order_no` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编号',
  `user_id` int DEFAULT NULL COMMENT '用户id',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户账户',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `pay_time` timestamp NULL DEFAULT NULL COMMENT '支付时间',
  `state` int NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('25', 'iphone15', '6000.00', '60.00', '0.00', '0.00', '202311011234567', '1', 'admin', '2023-11-05 21:05:47', '2023-11-05 21:05:43', '0');
INSERT INTO `t_order` VALUES ('26', '华为P60', '6000.00', '60.00', '0.00', '0.00', '202311011234568', '1', 'admin', '2023-11-05 21:04:49', null, '0');
INSERT INTO `t_order` VALUES ('68', '华为mate60', '7000.00', '7000.00', '0.00', '0.00', '20231123336677', null, null, '2023-11-23 10:48:31', '2023-11-23 10:48:32', '1');
INSERT INTO `t_order` VALUES ('69', '华为mate70', '6889.00', '6889.00', '0.00', '0.00', '2023112333667776', null, null, '2023-11-23 14:27:06', '2023-11-23 14:27:08', '1');
INSERT INTO `t_order` VALUES ('70', '华为mate70', '6889.00', '6889.00', '0.00', '0.00', '2023112333667779', null, null, '2023-11-23 14:29:43', '2023-11-23 14:29:44', '1');
