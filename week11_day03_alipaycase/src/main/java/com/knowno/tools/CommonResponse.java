package com.knowno.tools;

/**
 * 自定义响应结果
 * @param <T>
 */
public class CommonResponse<T> {
    private String code;
    private String msg;
    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public CommonResponse() {
    }

    public CommonResponse(T data) {
        this.data = data;
    }

    public static CommonResponse success() {
        CommonResponse result = new CommonResponse<>();
        result.setCode("0");
        result.setMsg("成功");
        return result;
    }

    public static <T> CommonResponse<T> success(T data) {
        CommonResponse<T> result = new CommonResponse<>(data);
        result.setCode("0");
        result.setMsg("成功");
        return result;
    }

    public static CommonResponse error(String code, String msg) {
        CommonResponse result = new CommonResponse();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
