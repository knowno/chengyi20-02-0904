package com.knowno.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "alipay")
public class AliPayConfig {
    //appid
    private String appId;
    //应用私钥:
    private String appPrivateKey;
    //支付宝公钥:
    private String alipayPublicKey;
    //回调的地址
    private String notifyUrl;
    //返回地址
    private String returnUrl;
}
