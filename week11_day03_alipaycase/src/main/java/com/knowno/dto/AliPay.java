package com.knowno.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AliPay {
    private String subject;
    private String traceNo;
    private String totalAmount;

}
