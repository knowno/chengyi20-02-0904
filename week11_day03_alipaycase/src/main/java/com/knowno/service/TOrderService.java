package com.knowno.service;

import com.alipay.api.AlipayApiException;
import com.knowno.dto.AliPay;
import com.knowno.entity.TOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * (TOrder)表服务接口
 *
 * @author makejava
 * @since 2023-11-05 21:01:30
 */
public interface TOrderService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TOrder queryById(Integer id);

    /**
     * 分页查询
     *
     * @param tOrder 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    Page<TOrder> queryByPage(TOrder tOrder, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param tOrder 实例对象
     * @return 实例对象
     */
    TOrder insert(TOrder tOrder);

    /**
     * 修改数据
     *
     * @param tOrder 实例对象
     * @return 实例对象
     */
    TOrder update(TOrder tOrder);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    int updateState(@Param("tradeNo") String tradeNo, @Param("state") int state, @Param("payTime") String payTime);

    void pay(AliPay aliPay, HttpServletResponse httpResponse) throws IOException;

    int payNotify(HttpServletRequest request) throws AlipayApiException;
}
