package com.knowno;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.knowno.dao")
public class Week11Day03AlipaycaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week11Day03AlipaycaseApplication.class, args);
    }

}
